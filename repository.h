#ifndef __REPOSITORY_H__ 
#define __REPOSITORY_H__ 

#define NUMBER_OF_APPS 1
#define prod_cons 0
static unsigned int appstype[] = {prod_cons};
static unsigned int apps_addresses[] = { 0x00000000 };
static unsigned int repository[] = { 
	0x00000002,	//application prod_cons id 0
	0x00000092,	//application size
	0x00000002,	//initial tasks
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0x00000000,	//cons
	0x00000024,	//task size
	0x00000081,	//bss size
	0x0000003F,	//task initial address
	0x00000002,	// dependences
	0x000007D0,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0x00000001,	//prod
	0x0000002F,	//task size
	0x00000081,	//bss size
	0x00000063,	//task initial address
	0x00000001,	// dependences
	0x000007D0,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xb083b480,	// cons
	0x4b19af00,
	0x21002200,
	0x46002000,
	0x46124609,
	0xdf05461b,
	0x607b2300,
	0x4b14e00b,
	0x21002200,
	0x46002002,
	0x46124609,
	0xdf04461b,
	0x3301687b,
	0x687b607b,
	0xddf02bc7,
	0x22004b0d,
	0x20002100,
	0x46094600,
	0x461b4612,
	0x2300df05,
	0x21002200,
	0x46002000,
	0x46124609,
	0xdf02461b,
	0x370c4618,
	0xf85d46bd,
	0x47707b04,
	0xcccc0078,
	0xcccc0090,
	0xcccc0084,
	0x534e4f43,
	0x61745320,
	0x00007472,
	0x534e4f43,
	0x646e4520,
	0x00000000,
	0xb083b480,	// prod
	0x4b24af00,
	0x21002200,
	0x46002000,
	0x46124609,
	0xdf05461b,
	0x607b2300,
	0x4a1fe008,
	0x009b687b,
	0x687a4413,
	0x687b605a,
	0x607b3301,
	0x2b7f687b,
	0x4b19ddf3,
	0x601a220a,
	0xf6404b17,
	0x629a320a,
	0x607b2300,
	0x4b14e00b,
	0x21002200,
	0x46002001,
	0x46124609,
	0xdf03461b,
	0x3301687b,
	0x687b607b,
	0xddf02bc7,
	0x22004b0d,
	0x20002100,
	0x46094600,
	0x461b4612,
	0x2300df05,
	0x21002200,
	0x46002000,
	0x46124609,
	0xdf02461b,
	0x370c4618,
	0xf85d46bd,
	0x47707b04,
	0xcccc00a4,
	0xcccc00bc,
	0xcccc00b0,
	0x444f5250,
	0x61745320,
	0x00007472,
	0x444f5250,
	0x646e4520,
	0x00000000,
	};

#endif  /*__REPOSITORY_H__*/
