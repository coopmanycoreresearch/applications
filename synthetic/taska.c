#include "synthetic.h"

static char start_print[] = "SYNTHETIC A Start";
static char end_print[] = "SYNTHETIC A End";
Message msg;

int main()
{
    SYSCALL_PRINTF(0,0,0,start_print);

    int i, j;

    msg.length = 30;

    for (i=0;i<SYNTHETIC_ITERATIONS;i++){
	
	for (j=0;j<30;j++) msg.msg[j] =i;
        /* SYSCALL_PRINTF(1,0,0,i+10000); */
        SYSCALL_SEND(taskB,0,0,&msg);

    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}
