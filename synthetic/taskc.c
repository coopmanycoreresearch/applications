#include "synthetic.h"

static char start_print[] = "SYNTHETIC C Start";
static char end_print[] = "SYNTHETIC C End";
Message msg;

int main()
{

    SYSCALL_PRINTF(0,0,0,start_print);

    int i;

    for (i=0;i<SYNTHETIC_ITERATIONS;i++){

        /* SYSCALL_PRINTF(1,0,0,-i-20000); */
        SYSCALL_RCV(taskB,0,0,&msg);

    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
