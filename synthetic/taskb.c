#include "synthetic.h"

static char start_print[] = "SYNTHETIC B Start";
static char end_print[] = "SYNTHETIC B End";
Message msg;

int main()
{

    SYSCALL_PRINTF(0,0,0,start_print);

    int i;

    for (i=0;i<SYNTHETIC_ITERATIONS;i++){

        SYSCALL_RCV(taskA,0,0,&msg);

        /* SYSCALL_PRINTF(1,0,0,i+20000); */
        SYSCALL_SEND(taskC,0,0,&msg);

    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}
