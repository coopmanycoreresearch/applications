#include "dtw.h"

static char start_print[] = "DTW F Start";
static char test_print[] =  "DTW F Sent";
static char end_print[] =   "DTW F End";

int test[SIZE][SIZE] ;
int SendSelect,RcvSelect;
Message msg1;

int main()
{
    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    for(j=0; j<SIZE; j++) {
        for(i=0; i<SIZE; i++) {
            if(i<10) {
                test[j][i]=i;
            } else {
                test[j][i]=0;
            }
        }
    }

    msg1.length = PATTERN_SIZE; //SIZE*SIZE nao pode ser maior que 128, senao usar o SendData

    memcpy(msg1.msg, test, msg1.length);

    rec_send();

    SYSCALL_PRINTF(0,0,0,test_print);

    rec_rcv();

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}

void rec_send()
{
    int i;
    for (i=0; i<TOTAL_TASKS; i++) {
        SendSelect=i+2;

        switch (SendSelect) {
            case p1:
                {
                    SYSCALL_SEND(p1,0,0,&msg1);
                    break;
                }
            case p2:
                {
                    SYSCALL_SEND(p2,0,0,&msg1);
                    break;
                }
            case p3:
                {
                    SYSCALL_SEND(p3,0,0,&msg1);
                    break;
                }
            case p4:
                {
                    SYSCALL_SEND(p4,0,0,&msg1);
                    break;
                }
        }
    }
}

void rec_rcv()
{
    int x, y;
    for (x=0; x<PATTERN_PER_TASK; x++) {
        for (y=0; y<TOTAL_TASKS; y++) {
            RcvSelect=y+2;

            switch (RcvSelect) {
                case p1:
                    {
                        SYSCALL_RCV(p1,0,0,&msg1);
                        break;
                    }
                case p2:
                    {
                        SYSCALL_RCV(p2,0,0,&msg1);
                        break;
                    }
                case p3:
                    {
                        SYSCALL_RCV(p3,0,0,&msg1);
                        break;
                    }
                case p4:
                    {
                        SYSCALL_RCV(p4,0,0,&msg1);
                        break;
                    }
            }
        }
    }
}

void memcpy( unsigned int *dest, const unsigned int *src, int n )
{
    while (n--) {
        dest[n] = src[n];
    }
}
