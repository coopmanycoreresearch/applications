#include "dtw.h"

// TODO, follow up on why this is the only app that needs a \n at the end of 
// the static strings
static char start_print[] = "DTW B Start";
static char end_print[] =   "DTW B End";

int test[SIZE][SIZE];
int pattern[SIZE][SIZE];
int lastCol[SIZE];
int currCol[SIZE];
int temp[SIZE];

Message msg1;

int main()
{
    int result, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    SYSCALL_RCV(recognizer,0,0,&msg1);

    memcpy(test, msg1.msg, msg1.length);

    for (j=0; j<PATTERN_PER_TASK; j++) {

        memset(msg1.msg, 0, MSG_SIZE);

        SYSCALL_RCV(bank,0,0,&msg1);

        memcpy(pattern, msg1.msg, PATTERN_SIZE);

        result = dynamicTimeWarping(test, pattern);

        msg1.msg[0] = result;

        msg1.length = 1;

        SYSCALL_SEND(recognizer,0,0,&msg1);
    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}

int min( int x, int y )
{
    if (x > y) {
        return y;
    }
    return x;
}

int euclideanDistance( int *x, int *y )
{
    int ed = 0.0f;
    int aux = 0.0f;
    int i;
    for (i = 0; i < SIZE; i++) {
        aux = x[i] - y[i];
        ed += aux * aux;
    }
    return ed;
}

int dynamicTimeWarping( int x[SIZE][SIZE], int y[SIZE][SIZE] )
{
    int maxI = SIZE - 1;
    int maxJ = SIZE - 1;
    int minGlobalCost;
    int i, j;

    currCol[0] = euclideanDistance(x[0], y[0]);
    for (j = 1; j <= maxJ; j++) {
        currCol[j] = currCol[j - 1] + euclideanDistance(x[0], y[j]);
    }

    for (i = 1; i <= maxI; i++) {

        memcpy(temp, lastCol, SIZE);
        memcpy(lastCol, currCol, SIZE);

        currCol[0] = lastCol[0] + euclideanDistance(x[i], y[0]);

        for (j = 1; j <= maxJ; j++) {
            minGlobalCost = min(lastCol[j], min(lastCol[j - 1], currCol[j - 1]));
            currCol[j] = minGlobalCost + euclideanDistance(x[i], y[j]);
        }
    }

    return currCol[maxJ];
}

void memset( int *dst, int c, unsigned int bytes )
{

    while (bytes--) {
        dst[bytes] = c;
    }
}

void memcpy( unsigned int *dest, const unsigned int *src, int n )
{
    while (n--) {
        dest[n] = src[n];
    }
}
