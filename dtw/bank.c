#include "dtw.h"

// TODO, follow up on why this is the only app that needs a \n at the end of 
// the static strings
static char start_print[] = "DTW A Start";
static char end_print[] =   "DTW A End";

int pattern[SIZE][SIZE];
Message msg1;
int SendSelect, seed, lfsr;

int main()
{
    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    msg1.length = PATTERN_SIZE; //SIZE*SIZE nao pode ser maior que 128, senao usar o SendData

    for (j=0; j<PATTERN_PER_TASK; j++) {
        for (i=0; i<TOTAL_TASKS; i++) {
			
			seed=((j*TOTAL_TASKS)+i);
			
            randPattern(pattern, seed); //gera uma matriz de valores aleatorios, poderiam ser coeficientes MFCC

            memcpy(msg1.msg, pattern, PATTERN_SIZE);

            SendSelect=i+2;

            switch (SendSelect) {
                case p1:
                    {
                        SYSCALL_SEND(p1,0,0,&msg1);
                        break;
                    }
                case p2:
                    {
                        SYSCALL_SEND(p2,0,0,&msg1);
                        break;
                    }
                case p3:
                    {
                        SYSCALL_SEND(p3,0,0,&msg1);
                        break;
                    }
                case p4:
                    {
                        SYSCALL_SEND(p4,0,0,&msg1);
                        break;
                    }
            }
        }
    }
    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}

void randPattern( int in[SIZE][SIZE], int seed )
{
    int x,y;

    for (x=0; x<SIZE; x++){
        for (y=0; y<SIZE; y++){
            in[x][y] = abs(irand(seed, 2, 100)%5000);
        }
    }
}

int irand( int seed, int min, int max )
{
    lfsr = seed;

    lfsr = (lfsr >> 1) ^ (-(lfsr & 1u) & 0xB400u);

    return (lfsr % max + min);
}

void memcpy( unsigned int *dest, const unsigned int *src, int n )
{
    while (n--) {
        dest[n] = src[n];
    }
}
