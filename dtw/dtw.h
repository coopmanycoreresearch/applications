#ifndef __DTW_H__
#define __DTW_H__
#include "../applications.h"
/**TASKS**/
#define bank 1
#define p1 2
#define p2 3
#define p3 4
#define p4 5
#define recognizer 6
/**defines**/
#define NUM_PATTERNS 40
#define SIZE    11  //tamanho da matriz
#define PATTERN_SIZE    ( SIZE * SIZE )
#define TOTAL_TASKS 4   //deve ser PAR para dividir igualmente o numero de padroes por task
#define PATTERN_PER_TASK (NUM_PATTERNS/TOTAL_TASKS)

#endif /*__DTW_H__*/
