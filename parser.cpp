#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <fstream>

#define NI_HEADER 2
#define PACKAGE_HEADER 11
#define MAX_TAM_LIN 128
#define MAX_TASKS 20
#define MAX_INIT_TASKS 10
#define PACK_FLITS (NI_HEADER+PACKAGE_HEADER)
#define TASK_HEADER (2*PACK_FLITS)
#define APP_HEADER PACKAGE_HEADER

using namespace std;
typedef struct {
    int task;
    int flits;
} DependencePackage;
/*
   typedef struct {
   char task_code[MAX_TAM_LIN];
   } LineCode;/**/

typedef struct {
    int  dependences_number;
    int address;
    int  size;
    int  bss;                       //BSS section size
    char *task_code[MAX_TAM_LIN];
    DependencePackage dependences[10];
} TaskPackage;

typedef struct {
    int address;
    int size;
    int tasks_number;
    TaskPackage *tasks;
} ApplicationPackage;

int main (int argc, char **argv)
{
    FILE *list_of_archives;
    FILE *fileptrcfg;
    FILE *fileptrIN;
    FILE *fileptrOUT;
    FILE *fileptrTESTCASE;

    char line[MAX_TAM_LIN];
    char temp[MAX_TAM_LIN];
    char temp_read[MAX_TAM_LIN];
    char UpperCode[MAX_TAM_LIN];
    char LowerCode[MAX_TAM_LIN];

    string::iterator it;
    string word;
    std::ostringstream hexa, file, infile,file_to_read,testcase;
    int rom = 0;
    int last_rom = 0;
    int sum_rom = 0;
    int i = 0;
    int j = 0;
    int num_tasks = 0;
    int num_apps = 1;
    int max_tasks = 1 ;
    int max_initial_tasks = 1 ;
    int last_app_task = 1;
    int last_app = 0;
    int number_of_tasks=0;
    int comparison_value[1] = {10};
    int* apptasks;
    int* app_firsts;
    int* appstypes;
    int app=0;
    int app_types=0;
    int task=0;
    int step=0;
    int task_size=0;
    int last_i=0;

    infile << argv[1] << "apps.tsk";
    num_apps=atoi(argv[2]);
    testcase << argv[3];
    apptasks = (int *) malloc(sizeof(int)*num_apps);
    app_firsts = (int *) malloc(sizeof(int)*num_apps);
    appstypes = (int *) malloc(sizeof(int)*num_apps);
    int initial_tasks[num_apps][MAX_INIT_TASKS];

    file << "repository.h";
    fileptrOUT = fopen(file.str().c_str(), "w");
    fprintf(fileptrOUT,"#ifndef __REPOSITORY_H__ \n");
    fprintf(fileptrOUT,"#define __REPOSITORY_H__ \n\n");
    fprintf(fileptrOUT,"#define NUMBER_OF_APPS %d\n",num_apps);
    fileptrTESTCASE = fopen(testcase.str().c_str(), "r");
    std::ostringstream applications[num_apps];
    while (fgets(temp_read,MAX_TAM_LIN,fileptrTESTCASE)!=NULL){
        if (app_types==0){
            appstypes[j]=0;
            string str_tmp(temp_read);
            str_tmp=str_tmp.substr(0,str_tmp.size()-1);
            applications[app_types]<<str_tmp.c_str();
            fprintf(fileptrOUT,"#define %s %d\n",str_tmp.c_str(),app_types);
            app_types++;
        }else{
            int is_equal=0;
            string str_tmp(temp_read);
            str_tmp=str_tmp.substr(0,str_tmp.size()-1);
            for (i=0;i<app_types;i++){
                if ((strstr(str_tmp.c_str(),applications[i].str().c_str()) !=0) && (str_tmp.size() == applications[i].str().size())){
                    is_equal=1;
                    appstypes[j]=i;
                }
            }
            if (is_equal==0){
                string str_tmp(temp_read);
                str_tmp=str_tmp.substr(0,str_tmp.size()-1);
                applications[app_types]<<str_tmp.c_str();
                fprintf(fileptrOUT,"#define %s %d\n",str_tmp.c_str(),app_types);
                app_types++;
            }
        }
        j++;
    }
    fclose(fileptrTESTCASE);

    ApplicationPackage applic_types[app_types];
    for (i=0;i<app_types;i++){
        applic_types[i].tasks=(TaskPackage *) malloc(sizeof(TaskPackage)*MAX_TASKS);
    }



    for (j=0;j<app_types;j++){
        std::ostringstream strcfg_file;
        strcfg_file<<"./"<<applications[j].str().c_str()<<"/"<<applications[j].str().c_str()<<".cfg";
        fileptrcfg = fopen(strcfg_file.str().c_str(), "r");
        int count=0;
        number_of_tasks=0;
        while (fgets(temp,MAX_TAM_LIN,fileptrcfg) != NULL) {
            if (strstr(temp,"<initialTasks>")!=NULL){
                fgets(temp,MAX_TAM_LIN,fileptrcfg);
                count=0;
                while (strstr(temp,"<dependences>")  == NULL) {
                    initial_tasks[j][count]=atoi(temp);
                    count++;
                    fgets(temp,MAX_TAM_LIN,fileptrcfg);
                    if (max_initial_tasks<count){
                        max_initial_tasks=count;
                    }
                }
                app_firsts[j]=count;
            }else{
                if (strstr(temp,"<end application>") == NULL) {
                    if (strstr(temp,"<task>")!=NULL){
                        number_of_tasks++;
                        fgets(temp,MAX_TAM_LIN,fileptrcfg);
                        task=atoi(temp);
                        task--;
                        fgets(temp,MAX_TAM_LIN,fileptrcfg);
                        if (strstr(temp,"<load>") != NULL) {
                            fgets(temp,MAX_TAM_LIN,fileptrcfg);
                            fgets(temp,MAX_TAM_LIN,fileptrcfg);
                        }
                        if (strstr(temp,"<comm>")!=NULL){
                            count=0;
                            fgets(temp,MAX_TAM_LIN,fileptrcfg);
                            while (strstr(temp,"<end task>")  == NULL) {
                                applic_types[j].tasks[task].dependences[count].task=atoi(temp);

                                fgets(temp,MAX_TAM_LIN,fileptrcfg);
                                applic_types[j].tasks[task].dependences[count].flits=atoi(temp);
                                count++;
                                fgets(temp,MAX_TAM_LIN,fileptrcfg);

                            }
                            applic_types[j].tasks[task].dependences_number=count;
                        }
                    }
                }
            }
        }
        apptasks[j]=number_of_tasks;
        applic_types[j].tasks_number=number_of_tasks;
        fclose(fileptrcfg);
    }




    i=0;
    int app_size = 0;
    int count_tasks = 0;
    for (j=0;j<app_types;j++){

        std::ostringstream strlist_files;
        FILE *fileptrlist;
        strlist_files<<"./"<<applications[j].str().c_str()<<"/"<<applications[j].str().c_str()<<".tsk";
        fileptrlist = fopen(strlist_files.str().c_str(), "r");
        count_tasks+=applic_types[j].tasks_number;
        last_app_task=0;
        while (fgets(temp_read,MAX_TAM_LIN,fileptrlist)!=NULL){

            string str_t(temp_read);
            str_t=str_t.substr(0,str_t.size()-1);
            if (str_t.size()>0){
                std::ostringstream strfile_read;
                strfile_read<<"./"<<applications[j].str().c_str()<<"/"<<str_t;
                fileptrIN = fopen(strfile_read.str().c_str(), "r");
                if (fileptrIN == NULL) {
                    cout<<"ERROR! Input file not found"<<endl;
                    return -2;
                }else{
                    while (strstr(temp,".text")  == NULL) {
                        fgets(temp,MAX_TAM_LIN,fileptrIN);
                    }
                    fgets(temp,MAX_TAM_LIN,fileptrIN);
                    fgets(temp,MAX_TAM_LIN,fileptrIN);
                    while (fgets(temp,MAX_TAM_LIN,fileptrIN) != NULL) {
                        if (temp != NULL){

                            if (strstr(temp,".bss")){
                                rom=(i-last_i)/4;
                                cout<<"************* "<<rom<<" ************"<<endl;
                            }
                            //cout<<temp;
                            strcpy(line,temp);
                            strcpy(UpperCode," ");
                            sscanf(line,"%*s %10[0-9a-hA-H ]s",UpperCode);
                            if (UpperCode[4] == ' '){

                                UpperCode[4]= UpperCode[5];
                                UpperCode[5]= UpperCode[6];
                                UpperCode[6]= UpperCode[7];
                                UpperCode[7]= UpperCode[8];
                                UpperCode[8]= 'x';
                            }
                            if (strlen(UpperCode) > 4) {
                                if (UpperCode[4] != ' '){
                                    if (UpperCode[8]== 'x') {
                                        i+=4;
                                    }
                                    else{
                                        i+=4;
                                    }
                                }
                                else{
                                    i+=2;
                                }

                            }
                        }
                    }

                    num_tasks++;
                    if (((i)%4)!=0){
                        i+=(4-(i%4));
                    }

                    cout<<str_t<<" task size "<<(rom);
                    cout<<" bss size "<<(((i-last_i)/4)-rom);
                    cout<<" total size "<<(((i-last_i)/4))<<endl;
                    applic_types[j].tasks[last_app_task].size=rom;
                    applic_types[j].tasks[last_app_task].bss=((i-last_i)/4)-rom;
                    applic_types[j].tasks[last_app_task].address=(sum_rom+(((j+1)*APP_HEADER)+(count_tasks*TASK_HEADER)));

                    last_i=i;
                    sum_rom+=rom;
                    last_rom=rom;
                    last_app_task++;
                }
                fclose(fileptrIN);
            }
        }
        if (j==0){
            applic_types[j].size=((APP_HEADER)+(num_tasks*TASK_HEADER)+(sum_rom));
        }else{
            applic_types[j].size=((APP_HEADER)+(last_app_task*TASK_HEADER)+((sum_rom-app_size)));
        }
        app_size=sum_rom;
        fclose(fileptrlist);
    }

    i=0;
    int repo_size=((num_tasks*TASK_HEADER)+(app_types*APP_HEADER));
    for (j=0;j<app_types;j++){
        if (j==0){
            applic_types[j].address=0x00000000;
        }else{
            applic_types[j].address=(applic_types[j-1].address+applic_types[j-1].size);
        }

        for (i=0;i<applic_types[j].tasks_number;i++){
            repo_size+=applic_types[j].tasks[i].size;
        }
    }/**/

    //fprintf(fileptrOUT,"static int REPO_SIZE = %d ;\n",repo_size);

    fileptrTESTCASE = fopen(testcase.str().c_str(), "r");
    fprintf(fileptrOUT,"static unsigned int appstype[] = {");
    j=0;
    i=0;
    while (fgets(temp_read,MAX_TAM_LIN,fileptrTESTCASE)!=NULL){
        string str_tmp(temp_read);
        str_tmp=str_tmp.substr(0,str_tmp.size()-1);
        if (i+1==num_apps)
            fprintf(fileptrOUT,"%s",str_tmp.c_str());
        else
            fprintf(fileptrOUT,"%s,",str_tmp.c_str());
        i++;
    }
    fprintf(fileptrOUT,"};\n");
    fclose(fileptrTESTCASE);

    i=0;
    fprintf(fileptrOUT,"static unsigned int apps_addresses[] = { ");
    for (j=0;j<app_types;j++){
        if (j+1==app_types)
            fprintf(fileptrOUT,"0x%08X ",applic_types[j].address);
        else
            fprintf(fileptrOUT,"0x%08X, ",applic_types[j].address);
    }/**/
    fprintf(fileptrOUT,"};\n");

    /***********************************
     * START REPOSITORY GENERATION
     * FORMAT IS LIKE A MEMORY PERIPHERAL
     *
     *
     * ********************************/

    i=0;
    num_tasks=0;
    char dep_temp[MAX_TAM_LIN];
    fprintf(fileptrOUT,"static unsigned int repository[] = { \n");
    for (j=0;j<app_types;j++){
        std::ostringstream strlist_files;
        FILE *fileptrlist;
        int yy=0;
        //cout<<"\n\n TASK CODES"<<endl;
        strlist_files<<"./"<<applications[j].str().c_str()<<"/"<<applications[j].str().c_str()<<".tsk";
        fileptrlist = fopen(strlist_files.str().c_str(), "r");
        cout<<applications[j].str().c_str()<<endl;
        /***************************************
         *
         * APP HEADER has PACKAGE_HEADER SIZE
         *
         *
         ****************************************/
        sprintf(dep_temp,"0x%08X,\t//application %s id %d",applic_types[j].tasks_number,applications[j].str().c_str(),j);
        fprintf(fileptrOUT,"\t%s\n",dep_temp);
        strcpy(dep_temp," ");
        yy++;
        sprintf(dep_temp,"0x%08X,\t//application size",applic_types[j].size);
        fprintf(fileptrOUT,"\t%s\n",dep_temp);
        strcpy(dep_temp," ");
        yy++;
        for (int xx=0;xx<app_firsts[j];xx++){
            sprintf(dep_temp,"0x%08X,\t//initial tasks",initial_tasks[j][xx]);
            fprintf(fileptrOUT,"\t%s\n",dep_temp);
            yy++;
        }
        for (int xx=yy;xx<PACKAGE_HEADER;xx++){
            sprintf(dep_temp,"0xffffffff,");
            fprintf(fileptrOUT,"\t%s\n",dep_temp);
        }
        //cout<<"FILE OPEN "<<strlist_files.str().c_str()<<endl;


        last_app_task=0;
        while (fgets(temp_read,MAX_TAM_LIN,fileptrlist)!=NULL){
            string str_t(temp_read);
            str_t=str_t.substr(0,str_t.size()-1);
            if (str_t.size()>0){

                int counter=0;

                strcpy(dep_temp," ");
                /***************************************
                 *
                 * TASK HEADER 2*number of PACK_FLITS SIZE
                 *
                 *
                 ****************************************/

                //INSERT TASK ID
                sprintf(dep_temp,"0x%08X",last_app_task);
                fprintf(fileptrOUT,"\t%s,\t//%s\n",dep_temp,str_t.substr(0,str_t.size()-4).c_str());
                strcpy(dep_temp," ");
                counter+=1;

                //INSERT ROM SIZE
                sprintf(dep_temp,"0x%08X",applic_types[j].tasks[last_app_task].size);
                fprintf(fileptrOUT,"\t%s,\t//task size\n",dep_temp);
                strcpy(dep_temp," ");
                counter+=1;

                //INSERT BSS SIZE
                sprintf(dep_temp,"0x%08X",applic_types[j].tasks[last_app_task].bss);
                fprintf(fileptrOUT,"\t%s,\t//bss size\n",dep_temp);
                strcpy(dep_temp," ");
                counter+=1;

                //INSERT TASK ADDRESS
                sprintf(dep_temp,"0x%08X",applic_types[j].tasks[last_app_task].address);
                fprintf(fileptrOUT,"\t%s,\t//task initial address\n",dep_temp);
                strcpy(dep_temp," ");
                counter+=1;

                //INSERT TASK DEPENDENCES
                for (int yy=0;yy<applic_types[j].tasks[last_app_task].dependences_number;yy++){

                    //TASK
                    sprintf(dep_temp,"0x%08X",applic_types[j].tasks[last_app_task].dependences[yy].task);
                    fprintf(fileptrOUT,"\t%s,\t// dependences\n",dep_temp);
                    strcpy(dep_temp," ");
                    counter+=1;

                    //NUMBER OF FLITS
                    sprintf(dep_temp,"0x%08X",applic_types[j].tasks[last_app_task].dependences[yy].flits);
                    fprintf(fileptrOUT,"\t%s,\n",dep_temp);
                    strcpy(dep_temp," ");
                    counter+=1;
                }


                for (int yy=counter;yy<(PACK_FLITS*2);yy++){
                    fprintf(fileptrOUT,"\t0xffffffff,\n");
                }
                last_app_task++;
            }
        }
        fclose(fileptrlist);


        fileptrlist = fopen(strlist_files.str().c_str(), "r");
        last_app_task=0;
        strcpy(dep_temp," ");
        char code_temp[MAX_TAM_LIN];
        int first=0;
        while (fgets(temp_read,MAX_TAM_LIN,fileptrlist)!=NULL){
            string str_t(temp_read);
            str_t=str_t.substr(0,str_t.size()-1);
            if (str_t.size()>0){
                first=1;
                std::ostringstream strfile_read;
                //cout << str_t.c_str()<<endl;
                //cout << str_t.substr(0,str_t.size()-4)<<endl;
                //cout << applications[j].str().c_str()<<endl;
                strfile_read<<"./"<<applications[j].str().c_str()<<"/"<<str_t;
                //cout << strfile_read.str().c_str()<<endl;
                fileptrIN = fopen(strfile_read.str().c_str(), "r");
                //cout<<strfile_read.str().c_str()<<endl;
                if (fileptrIN == NULL) {
                    printf("ERROR! Input file not found\n");
                    return -2;
                }else{
                    int tess=0;
                    //cout <<"TASKS " <<num_tasks<<endl;
                    //fprintf(fileptrOUT,"extern char array%d[] = { \n",num_tasks);

                    while (strstr(temp,".text")  == NULL) {
                        //printf("nACHOU \n");
                        fgets(temp,MAX_TAM_LIN,fileptrIN);
                        //printf("%s",temp);
                    }
                    //printf("ACHOU \n");
                    fgets(temp,MAX_TAM_LIN,fileptrIN);
                    fgets(temp,MAX_TAM_LIN,fileptrIN);
                    int upper_difference=0;
                    while (fgets(temp,MAX_TAM_LIN,fileptrIN) != NULL && !strstr(temp,".bss")) {

                        //printf("%s",temp);
                        if (temp != NULL){
                            strcpy(line,temp);
                            strcpy(UpperCode," ");
                            //printf("-----%s\n",line);
                            sscanf(line,"%*s %10[0-9a-hA-H ]s",UpperCode);
                            //printf("%d %s\n",(i%4),UpperCode);
                            //printf("%s\n",UpperCode);

                            if (UpperCode[4] == ' '){

                                UpperCode[4]= UpperCode[5];
                                UpperCode[5]= UpperCode[6];
                                UpperCode[6]= UpperCode[7];
                                UpperCode[7]= UpperCode[8];
                                UpperCode[8]= 'x';
                            }

                            //cout<<UpperCode<<endl;
                            string str_upper(UpperCode);
                            if (strlen(UpperCode) > 4) {

                                if (UpperCode[4] != ' '){
                                    tess++;
                                    if (UpperCode[8]== 'x') {
                                        if (upper_difference==4){
                                            strcpy(dep_temp," ");
                                            sprintf(dep_temp,"0x%c%c%c%c%c%c%c%c,",UpperCode[0],UpperCode[1],UpperCode[2],UpperCode[3],code_temp[0],code_temp[1],code_temp[2],code_temp[3]);
                                            UpperCode[0]= UpperCode[4];
                                            UpperCode[1]= UpperCode[5];
                                            UpperCode[2]= UpperCode[6];
                                            UpperCode[3]= UpperCode[7];
                                            strcpy(code_temp,UpperCode);
                                            if (first==1){
                                                strcat(dep_temp,"\t// ");
                                                strcat(dep_temp,str_t.substr(0,str_t.size()-4).c_str());
                                                first=0;
                                            }
                                            //cout<<dep_temp<<endl;
                                            fprintf(fileptrOUT,"\t%s\n",dep_temp);
                                            /*fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[2],UpperCode[3]); i++;
                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[0],UpperCode[1]); i++;

                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[6],UpperCode[7]); i++;
                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[4],UpperCode[5]); i++;/**/
                                        }else{
                                            strcpy(dep_temp," ");
                                            sprintf(dep_temp,"0x%c%c%c%c%c%c%c%c,",UpperCode[4],UpperCode[5],UpperCode[6],UpperCode[7],
                                                    UpperCode[0],UpperCode[1],UpperCode[2],UpperCode[3]);
                                            //cout<<dep_temp<<endl;
                                            fprintf(fileptrOUT,"\t%s\n",dep_temp);
                                            /*fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[2],UpperCode[3]); i++;
                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[0],UpperCode[1]); i++;

                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[6],UpperCode[7]); i++;
                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[4],UpperCode[5]); i++; /**/
                                        }

                                    }
                                    else{
                                        if (upper_difference==4){
                                            strcpy(dep_temp," ");
                                            sprintf(dep_temp,"0x%c%c%c%c%c%c%c%c,",code_temp[0],code_temp[1],code_temp[2],code_temp[3],
                                                    UpperCode[4],UpperCode[5],UpperCode[6],UpperCode[7]);
                                            strcpy(code_temp,UpperCode);
                                            if (first==1){
                                                strcat(dep_temp,"\t// ");
                                                strcat(dep_temp,str_t.substr(0,str_t.size()-4).c_str());
                                                first=0;
                                            }
                                            //cout<<dep_temp<<endl;
                                            fprintf(fileptrOUT,"\t%s\n",dep_temp);
                                            /*fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[6],UpperCode[7]); i++;
                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[4],UpperCode[5]); i++;
                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[2],UpperCode[3]); i++;
                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[0],UpperCode[1]); i++;/**/

                                        }else{
                                            strcpy(dep_temp," ");
                                            sprintf(dep_temp,"0x%c%c%c%c%c%c%c%c,",UpperCode[0],UpperCode[1],UpperCode[2],UpperCode[3],
                                                    UpperCode[4],UpperCode[5],UpperCode[6],UpperCode[7]);
                                            //cout<<dep_temp<<endl;
                                            fprintf(fileptrOUT,"\t%s\n",dep_temp);
                                            /*fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[6],UpperCode[7]); i++;
                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[4],UpperCode[5]); i++;
                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[2],UpperCode[3]); i++;
                                              fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[0],UpperCode[1]); i++;   /**/
                                        }
                                    }
                                }
                                else{
                                    if (upper_difference==4){
                                        tess++;
                                        upper_difference=8;
                                        strcpy(dep_temp," ");
                                        sprintf(dep_temp,"0x%c%c%c%c%c%c%c%c,",UpperCode[0],UpperCode[1],UpperCode[2],UpperCode[3],
                                                code_temp[0],code_temp[1],code_temp[2],code_temp[3]);
                                        strcpy(code_temp,UpperCode);
                                        if (first==1){
                                            //cout<<"\t\t"<<str_t.substr(0,str_t.size()-4).c_str()<<endl;
                                            strcat(dep_temp,"\t// ");
                                            strcat(dep_temp,str_t.substr(0,str_t.size()-4).c_str());
                                            first=0;
                                        }
                                        //cout<<dep_temp<<endl;
                                        fprintf(fileptrOUT,"\t%s\n",dep_temp);
                                        /*fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[2],UpperCode[3]); i++;
                                          fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[0],UpperCode[1]); i++;/**/
                                    }else{
                                        upper_difference=4;
                                        strcpy(code_temp,UpperCode);
                                        /*fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[2],UpperCode[3]); i++;
                                          fprintf(fileptrOUT,"0x%c%c,\n",UpperCode[0],UpperCode[1]); i++;/**/
                                    }

                                }
                                //cout<<dep_temp<<endl;
                                //fprintf(fileptrOUT,"\t%s\n",dep_temp);

                            }

                        }
                    }
                    //cout<<"TESTE "<<tess<<endl;
                    if (upper_difference==4){
                        strcpy(dep_temp," ");
                        sprintf(dep_temp,"0x0000%c%c%c%c,",code_temp[0],code_temp[1],code_temp[2],code_temp[3]);
                        //cout<<dep_temp<<endl;
                        fprintf(fileptrOUT,"%s\n",dep_temp);
                    }
                    strcpy(dep_temp," ");
                    /*if ((i%4)!=0){
                      for (int j=0;j<4-(i%4);j++){
                      fprintf(fileptrOUT,"0x00,\n");
                      }
                      }/**/
                    //fprintf(fileptrOUT,"};\n");
                    num_tasks++;
                    /*******************************************/
                    last_app_task++;
                }
                fclose(fileptrIN);
            }
        }




        fclose(fileptrlist);
    }

    fprintf(fileptrOUT,"\t};\n\n");

    fprintf(fileptrOUT,"#endif  /*__REPOSITORY_H__*/\n");

    fclose(fileptrOUT);
    return 0;
}
