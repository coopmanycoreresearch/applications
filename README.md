This repository contains all applications used in our platform, be that in OVP,
RTL or other.

# TODO
- check why DTW is the only app that need a \n in strings to work properly
- refactor parser.cpp

# Applications Tested and Working (OVP)
- dtw
- dtwd
- mpeg
- dijkstra
- prod cons
- prod cons with slow cons
- prod cons with slow prod
- synthetic

# Application Not Working/Tested (OVP)
- vopd
- mwd
- mpeg4
- fixe base test 16
- dtwd8 pubsub
- mpeg pubsu
