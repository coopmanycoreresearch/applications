#include "dtwd.h"

static char start_print[]=  "DTWD J Start";
static char test_print[]=   "DTWD J Sent";
static char end_print[]=    "DTWD J End";

int test[SIZE][SIZE];

Message msg1;

int main()
{
    int i,j;

    SYSCALL_PRINTF(0,0,0,start_print);
    
    for(j=0; j<SIZE; j++) {
        for(i=0; i<SIZE; i++) {
            if(i<10) {
                test[i][j]=i;
            } else {
                test[i][j]=0;
            }
        }		
    }

    msg1.length = SIZE*SIZE; //SIZE*SIZE nao pode ser maior que 128, senao usar o SendData

    memcpy(msg1.msg, test, sizeof(test));

    rec_send();

    SYSCALL_PRINTF(0,0,0,test_print);

    rec_rcv();

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}

void rec_send()
{
    int i, sw;
    for (i=0; i<TOTAL_TASKS; i++) {
        sw=i+2;

        switch (sw) {
            case p1:
                {
                    SYSCALL_SEND(p1,0,0,&msg1);
                    break;
                }
            case p2:
                {
                    SYSCALL_SEND(p2,0,0,&msg1);
                    break;
                }
            case p3:
                {
                    SYSCALL_SEND(p3,0,0,&msg1);
                    break;
                }
            case p4:
                {
                    SYSCALL_SEND(p4,0,0,&msg1);
                    break;
                }
            case p5:
                {
                    SYSCALL_SEND(p5,0,0,&msg1);
                    break;
                }
            case p6:
                {
                    SYSCALL_SEND(p6,0,0,&msg1);
                    break;
                }
            case p7:
                {
                    SYSCALL_SEND(p7,0,0,&msg1);
                    break;
                }
            case p8:
                {
                    SYSCALL_SEND(p8,0,0,&msg1);
                    break;
                }
        }
    }
}

void rec_rcv()
{
    int i, j, sw;
    for (j=0; j<PATTERN_PER_TASK; j++) {
        for (i=0; i<TOTAL_TASKS; i++) {
            sw=i+2;
            switch (sw) {
                case p1:
                    {
                        SYSCALL_RCV(p1,0,0,&msg1);
                        break;
                    }
                case p2:
                    {
                        SYSCALL_RCV(p2,0,0,&msg1);
                        break;
                    }
                case p3:
                    {
                        SYSCALL_RCV(p3,0,0,&msg1);
                        break;
                    }
                case p4:
                    {
                        SYSCALL_RCV(p4,0,0,&msg1);
                        break;
                    }
                case p5:
                    {
                        SYSCALL_RCV(p5,0,0,&msg1);
                        break;
                    }
                case p6:
                    {
                        SYSCALL_RCV(p6,0,0,&msg1);
                        break;
                    }
                case p7:
                    {
                        SYSCALL_RCV(p7,0,0,&msg1);
                        break;
                    }
                case p8:
                    {
                        SYSCALL_RCV(p8,0,0,&msg1);
                        break;
                    }
            }
        }
    }
}

void memcpy( unsigned int *dest, const unsigned int *src, size_t n )
{
    unsigned int i = n/4;

    while (i--) {
        dest[i] = src[i];
    }
}
