#include "dtwd.h"

static char start_print[]=  "DTWD A Start";
static char end_print[]=    "DTWD A End";

int pattern[SIZE][SIZE];

Message msg1;

int main()
{
    int i, j, sw, seed;
    SYSCALL_PRINTF(0,0,0,start_print);

    msg1.length = SIZE * SIZE; //SIZE*SIZE nao pode ser maior que 128, senao usar o SendData

    for (j=0; j<PATTERN_PER_TASK; j++) {
        for (i=0; i<TOTAL_TASKS; i++) {
			seed=((j*TOTAL_TASKS)+i);
			
            randPattern(pattern, seed); //gera uma matriz de valores aleatorios, poderiam ser coeficientes MFCC

            memcpy(msg1.msg, pattern, sizeof(pattern));

            sw=i+2;

            switch (sw) {
                case p1:
                    {
                        SYSCALL_SEND(p1,0,0,&msg1);
                        break;
                    }
                case p2:
                    {
                        SYSCALL_SEND(p2,0,0,&msg1);
                        break;
                    }
                case p3:
                    {
                        SYSCALL_SEND(p3,0,0,&msg1);
                        break;
                    }
                case p4:
                    {
                        SYSCALL_SEND(p4,0,0,&msg1);
                        break;
                    }
                case p5:
                    {
                        SYSCALL_SEND(p5,0,0,&msg1);
                        break;
                    }
                case p6:
                    {
                        SYSCALL_SEND(p6,0,0,&msg1);
                        break;
                    }
                case p7:
                    {
                        SYSCALL_SEND(p7,0,0,&msg1);
                        break;
                    }
                case p8:
                    {
                        SYSCALL_SEND(p8,0,0,&msg1);
                        break;
                    }
            }
        }
    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}

void randPattern( int in[SIZE][SIZE], int seed )
{
    int i,j;

    for (i=0; i<SIZE; i++){
        for (j=0; j<SIZE; j++){
            in[i][j] = abs(irand(seed, 2, 100)%5000);
        }
    }
}

int irand( int seed, int min, int max )
{
    int lfsr = seed;

    lfsr = (lfsr >> 1) ^ (-(lfsr & 1u) & 0xB400u);

    return (lfsr % max + min);
}

void memcpy( unsigned int *dest, const unsigned int *src, size_t n )
{
    unsigned int i = n/4;

    while (i--) {
        dest[i] = src[i];
    }
}
