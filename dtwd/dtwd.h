#ifndef __DTWD_H__
#define __DTWD_H__
#include <stdio.h>
#include "../applications.h"
/**TASKS**/
#define bank 1
#define p1 2
#define p2 3
#define p3 4
#define p4 5
#define p5 6
#define p6 7
#define p7 8
#define p8 9
#define recognizer 10
/**defines**/
#define NUM_PATTERNS 80
#define SIZE    11  //tamanho da matriz
#define TOTAL_TASKS 8   //deve ser PAR para dividir igualmente o numero de padroes por task
#define PATTERN_PER_TASK (NUM_PATTERNS/TOTAL_TASKS)

#endif /*__DTWD_H__*/
