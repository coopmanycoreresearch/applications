#include "fixe_base_test_16.h"

static char print_start[]="FBT K Start";
static char print_end[]="FBT K End";

int RGB[3];

Message msg1,msg2;

int main()
{
    int i;

    SYSCALL_PRINTF(0,0,0,print_start);

    SYSCALL_RCV(XYZ1,0,0,&msg1);

    rgb(msg1.msg,RGB);

    msg2.length=3;
    for (i=0;i<3;i++) {
        msg2.msg[i]=RGB[i];
    }

    SYSCALL_SEND(DRGB,0,0,&msg2);

    SYSCALL_PRINTF(0,0,0,print_end);

    SYSCALL_DELETE(0,0,0,0);
}

int mult( int a, int b )
{
    int i,res;
    int cpt1=0;  /*détermine l'ordre de grandeur en puissance de 10 de a*/
    int cpt2=0;  /*détermine l'ordre de grandeur en puissance de 10 de b*/
    int cpt =2*FIXE; /*compte le nombre de décimales*/
    int a2,b2;

    a2=a;
    b2=b;

    /*détermine l'ordre de grandeur en puissance de 10 de a*/
    while (a2!=0) {
        a2=a2/10;
        cpt1++;
    }
    cpt1--;

    /*détermine l'ordre de grandeur en puissance de 10 de b*/
    while (b2!=0) {
        b2=b2/10;
        cpt2++;
    }
    cpt2--;

    /*divise a et b en conséquence lorsque ceux-ci sont trop grand*/
    /*on sacrifie en précision pour pouvoir effectuer le calcul*/
    while ((cpt1+cpt2)>=PU-1) {
        a=a/10;
        cpt1--;
        cpt--;
        if ((cpt1+cpt2)>=PU-1) {
            b=b/10;
            cpt2--;
            cpt--;
        }
    }
    res=a*b;

    /*retire les derniers chiffres après la virgule pour n'en garder que FIXE*/
    if (cpt>FIXE) {
        for (i=0;i<(cpt-FIXE);i++) {
            res=res/10;
        }
    } else {
        while (cpt<FIXE) {
            res=res*10;
            cpt++;
        }
    }

    return res;
}

void rgb( int* sum,int* RGB )
{
    RGB[0]=(mult(23706,sum[0])-mult(5138,sum[1]))+mult(53,sum[2]);
    RGB[1]=(mult(14253,sum[1])-mult(9000,sum[0]))-mult(147,sum[2]);
    RGB[2]=(mult(886,sum[1])-mult(4706,sum[0]))+mult(10094,sum[2]);
}




