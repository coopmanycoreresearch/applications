#include "fixe_base_test_16.h"

static char print_start[]="FBT F Start";
static char print_end[]="FBT F End";

int tabrefX[size]={1,2,2,3,5,6,8,11,14,18,23,29,37,47,58,72};
int tabrefY[size]={0,0,0,0,0,0,0,1,1,2,3,3,4,5,6,8};
int tabrefZ[size]={7,9,12,17,22,29,38,50,64,82,105,133,167,210,261,323};

Message msg1,msg2;

int main()
{
    int i;

    SYSCALL_PRINTF(0,0,0,print_start);

    SYSCALL_RCV(P1,0,0,&msg1);

    msg2.length=3;

    msg2.msg[0]=sommeXYZ(msg1.msg,tabrefX);
    msg2.msg[1]=sommeXYZ(msg1.msg,tabrefY);
    msg2.msg[2]=sommeXYZ(msg1.msg,tabrefZ);

    SYSCALL_SEND(LAB1,0,0,&msg2);
    SYSCALL_SEND(DXYZ,0,0,&msg2);
    SYSCALL_SEND(RGB1,0,0,&msg2);

    SYSCALL_PRINTF(0,0,0,print_end);

    SYSCALL_DELETE(0,0,0,0);
}

int mult( int a, int b )
{
    int i,res;
    int cpt1=0;  /*détermine l'ordre de grandeur en puissance de 10 de a*/
    int cpt2=0;  /*détermine l'ordre de grandeur en puissance de 10 de b*/
    int cpt =2*FIXE; /*compte le nombre de décimales*/
    int a2,b2;

    a2=a;
    b2=b;

    /*détermine l'ordre de grandeur en puissance de 10 de a*/
    while (a2!=0) {
        a2=a2/10;
        cpt1++;
    }
    cpt1--;

    /*détermine l'ordre de grandeur en puissance de 10 de b*/
    while (b2!=0) {
        b2=b2/10;
        cpt2++;
    }
    cpt2--;

    /*divise a et b en conséquence lorsque ceux-ci sont trop grand*/
    /*on sacrifie en précision pour pouvoir effectuer le calcul*/
    while ((cpt1+cpt2)>=PU-1) {
        a=a/10;
        cpt1--;
        cpt--;
        if ((cpt1+cpt2)>=PU-1) {
            b=b/10;
            cpt2--;
            cpt--;
        }
    }

    /*calcul de la multiplication*/
    res=a*b;

    /*retire les derniers chiffres après la virgule pour n'en garder que FIXE*/
    if (cpt>FIXE) {
        for (i=0;i<(cpt-FIXE);i++) {
            res=res/10;
        }
    } else {
        while (cpt<FIXE) {
            res=res*10;
            cpt++;
        }
    }
    return res;
}

int sommeXYZ( int* moyenne,int* tabref )
{
    int i;
    int sum=0;
    for (i=0;i<size;i++) {
        sum+=mult(tabref[i],moyenne[i]);
    }
    return sum;
}
