#ifndef __FIXE_BASE_TEST_16_H__
#define __FIXE_BASE_TEST_16_H__
#include "../applications.h"

#define FIXE 4 /*nb de chiffres après la virgule*/
#define PU 9 /*puissance de 10 max supportée ici 2³¹->2 000 000 000 donc 10⁹*/
#define size 16

#define DLAB 1
#define DRGB 2
#define DXYZ 3
#define GFC 4
#define LAB1 5
#define LAB2 6
#define P1 7
#define P2 8
#define RGB1 9
#define RGB2 10
#define RMS 11
#define WRMS 12
#define XYZ1 13
#define XYZ2 14

#endif /*__FIXE_BASE_TEST_16_H__*/
