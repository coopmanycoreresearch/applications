#include "mpeg4.h"

static char start_print[]=	"MPEG I Start";
static char end_print[]=	"MPEG I End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm MCPU 1280*/
    for (j=0;j<10;j++) {
        SYSCALL_RCV(MCPU_0,0,0,&msg);
    }

    /*Comm RAST 1280*/
    msg.length=128;
    for (j=0;j<10;j++) {
        SYSCALL_SEND(RAST_0,0,0,&msg);
    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
