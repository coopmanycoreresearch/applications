#ifndef __MPEG4_H__
#define __MPEG4_H__
#include "../applications.h"

#define ADSP_0 1
#define AU_0 2
#define BAB_0 3
#define IDCT_0 4
#define MCPU_0 5
#define RAST_0 6
#define RISC_0 7
#define SDRAM_0 8
#define SRAM1_0 9
#define SRAM2_0 10
#define UPSAMP_0 11
#define VU_0 12

#endif /*__MPEG4_H__*/
