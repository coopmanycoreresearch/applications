#include "mpeg4.h"

static char start_print[]=	"MPEG C Start";
static char end_print[]=	"MPEG C End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm SRAM2 11310*/
    for (j=0;j<89;j++) {
        SYSCALL_RCV(SRAM2_0,0,0,&msg);
    }
    
    /*Comm SDRAM 15000*/
    for (j=0;j<118;j++) {
        SYSCALL_RCV(SDRAM_0,0,0,&msg);
    }

    /*Comm SDRAM 15000*/
    msg.length=128;
    for (j=0;j<117;j++) {
        SYSCALL_SEND(SDRAM_0,0,0,&msg);
    }
    
    msg.length=24;
    SYSCALL_SEND(SDRAM_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
