#include "mpeg4.h"

static char start_print[]=	"MPEG J Start";
static char end_print[]=	"MPEG J End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm SRAM1 1280*/
    for (j=0;j<10;j++) {
        SYSCALL_RCV(SRAM1_0,0,0,&msg);
    }

    /*Comm SDRAM 10130*/
    for (j=0;j<80;j++) {
        SYSCALL_RCV(SDRAM_0,0,0,&msg);
    }
    
    /*Comm SDRAM 10130*/
    msg.length=128;
    for (j=0;j<79;j++) {
        SYSCALL_SEND(SDRAM_0,0,0,&msg);
    }
    
    msg.length=18;
    SYSCALL_SEND(SDRAM_0,0,0,&msg);
    
    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
