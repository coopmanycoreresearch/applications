#include "mpeg4.h"

static char start_print[]=	"MPEG B Start";
static char end_print[]=	"MPEG B End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    for (j=0;j<128;j++) {
        msg.msg[j]=j;
    }

    /*Comm RISC 8440*/
    msg.length=128;
    for (j=0;j<65;j++) {
        SYSCALL_SEND(RISC_0,0,0,&msg);
    }
    
    msg.length=120;
    SYSCALL_SEND(RISC_0,0,0,&msg);
    
    /*Comm RISC 8440*/
    for (j=0;j<66;j++) {
        SYSCALL_RCV(RISC_0,0,0,&msg);
    }
    
    /*Comm BAB 2930*/
    msg.length=128;
    for (j=0;j<22;j++) {
        SYSCALL_SEND(BAB_0,0,0,&msg);
    }
    
    msg.length=114;
    SYSCALL_SEND(BAB_0,0,0,&msg);
    
    /*Comm IDCT 4220*/
    msg.length=128;
    for (j=0;j<32;j++) {
        SYSCALL_SEND(IDCT_0,0,0,&msg);
    }
    
    msg.length=124;
    SYSCALL_SEND(IDCT_0,0,0,&msg);

    /*Comm IDCT 4220*/
    for (j=0;j<33;j++) {
        SYSCALL_RCV(IDCT_0,0,0,&msg);
    }

    /*Comm UPSAMP 11310*/
    msg.length=128;
    for (j=0;j<88;j++) {
        SYSCALL_SEND(UPSAMP_0,0,0,&msg);
    }

    msg.length=46;
    SYSCALL_SEND(UPSAMP_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
