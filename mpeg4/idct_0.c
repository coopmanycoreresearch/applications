#include "mpeg4.h"

static char start_print[]=	"MPEG E Start";
static char end_print[]=	"MPEG E End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm SRAM2 4220*/
    for (j=0;j<33;j++) {
        SYSCALL_RCV(SRAM2_0,0,0,&msg);
    }

    /*Comm SRAM2 4220*/
    msg.length=128;
    for (j=0;j<32;j++) {
        SYSCALL_SEND(SRAM2_0,0,0,&msg);
    }
    
    msg.length=124;
    SYSCALL_SEND(SRAM2_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
