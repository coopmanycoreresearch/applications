#include "mpeg4.h"

static char start_print[]=	"MPEG A Start";
static char end_print[]=	"MPEG A End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    for (j=0;j<128;j++) {
        msg.msg[j]=j;
    }

    /*Comm BAB 1280*/
    msg.length=128;
    for (j=0;j<10;j++) {
        SYSCALL_SEND(BAB_0,0,0,&msg);
    }

    /*Comm BAB 1280*/
    for (j=0;j<10;j++) {
        SYSCALL_RCV(BAB_0,0,0,&msg);
    }

    /*Comm UPSAMP 15000*/
    msg.length=128;
    for (j=0;j<117;j++) {
        SYSCALL_SEND(UPSAMP_0,0,0,&msg);
    }
    
    msg.length=24;
    SYSCALL_SEND(UPSAMP_0,0,0,&msg);
    
    /*Comm UPSAMP 15000*/
    for (j=0;j<118;j++) {
        SYSCALL_RCV(UPSAMP_0,0,0,&msg);
    }
    
    /*Comm ADSP 1280*/
    msg.length=128;
    for (j=0;j<10;j++) {
        SYSCALL_SEND(ADSP_0,0,0,&msg);
    }
    
    /*Comm MCPU 1280*/
    msg.length=128;
    for (j=0;j<10;j++) {
        SYSCALL_SEND(MCPU_0,0,0,&msg);
    }
    
    /*Comm ADSP 1280*/
    for (j=0;j<10;j++) {
        SYSCALL_RCV(ADSP_0,0,0,&msg);
    }
    
    /*Comm RAST 10130*/
    msg.length=128;
    for (j=0;j<79;j++) {
        SYSCALL_SEND(RAST_0,0,0,&msg);
    }
    
    msg.length=18;
    SYSCALL_SEND(RAST_0,0,0,&msg);
    
    /*Comm RAST 10130*/
    
    for (j=0;j<80;j++) {
        SYSCALL_RCV(RAST_0,0,0,&msg);
    }
    
    /*Comm AU 1280*/
    msg.length=128;
    for (j=0;j<10;j++) {
        SYSCALL_SEND(AU_0,0,0,&msg);
    }

    /*Comm VU 3210*/
    msg.length=128;
    for (j=0;j<25;j++) {
        SYSCALL_SEND(VU_0,0,0,&msg);
    }
    msg.length=10;
    SYSCALL_SEND(VU_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}
