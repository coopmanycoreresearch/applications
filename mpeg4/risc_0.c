#include "mpeg4.h"

static char start_print[]=	"MPEG G Start";
static char end_print[]=	"MPEG G End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm SRAM2 8440*/
    for (j=0;j<66;j++) {
        SYSCALL_RCV(SRAM2_0,0,0,&msg);
    }
    
    /*Comm SRAM2 8440*/
    msg.length=128;
    for (j=0;j<65;j++) {
        SYSCALL_SEND(SRAM2_0,0,0,&msg);
    }
    
    msg.length=120;
    SYSCALL_SEND(SRAM2_0,0,0,&msg);
    
    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
