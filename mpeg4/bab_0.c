#include "mpeg4.h"

static char start_print[]=	"MPEG D Start";
static char end_print[]=	"MPEG D End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);


    /*Comm SRAM2 2930*/
    for (j=0;j<23;j++) {
        SYSCALL_RCV(SRAM2_0,0,0,&msg);
    }

    /*Comm SDRAM 1280*/
    for (j=0;j<10;j++) {
        SYSCALL_RCV(SDRAM_0,0,0,&msg);
    }

    /*Comm SDRAM 1280*/
    msg.length=128;
    for (j=0;j<10;j++) {
        SYSCALL_SEND(SDRAM_0,0,0,&msg);
    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
