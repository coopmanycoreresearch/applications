/*---------------------------------------------------------------------
TITLE: Program Scheduler
AUTHOR: Nicolas Saint-jean
EMAIL : saintjea@lirmm.fr
DATE CREATED: 04/04/06
FILENAME: task3.c
PROJECT: Network Process Unit
COPYRIGHT: Software placed into the public domain by the author.
Software 'as is' without warranty.  Author liable for nothing.
DESCRIPTION: This file contains the task3
---------------------------------------------------------------------*/

//#include <stdlib.h>
#include "mpeg_pubsub.h"

//Message msg1;

messageHandlers_str messageHandlers;

tasks_subscribers_api_struct tasks_subscribers_api;
MQSoCTopic topicName;

void trata_callback(MQSoCMessage* mqMessage);

static char start_print[]=  "MPEG E Start";
static char blocks_print[]= "proc blocks";
static char end_print[]=    "MPEG E End";

int print()
{
    int i;
    SYSCALL_PRINTF(0,0,0,start_print);
    /*
       for(i=0;i<MPEG_FRAMES;i++)
       {
       SYSCALL_RCV(IDCT,0,0,&msg1);
       }*/


    topicName.topic_id = 4;

    MQSoCSubscribe(topicName, trata_callback, 1);

    MQSoCYield(PUBRCV_DELAY, MPEG_FRAMES, SUSPEND_SUBS);

    /*SYSCALL_PRINTF(0,0,0,blocks_print);

      SYSCALL_PRINTF(1,0,0,(msg1.length));

      SYSCALL_PRINTF(0,0,0,end_print);*/

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_END_TASK(PRINT,0,0,0);
}

/*
 * timeout = time between read on buffer
 * c_exit = read attemps to end task
 * suspend = suspend(1) or not(0) the task in case of no message on buffer
 */
void MQSoCYield(int timeout, int c_exit, int suspend){
    int count = 0;
    while (1){
        int k;
        for(k=0;k<timeout;k++); //sleep()
        tasks_subscribers_api.suspend = suspend;
        SYSCALL_MQSOC_PUBRCV(0,0,0,&tasks_subscribers_api); //verifica se chegou pacote e pegar do buffer
        if (tasks_subscribers_api.received == 1){
#ifdef PRINT_APP
            SYSCALL_PRINTF(0,0,0,"Recebeu mensagem");
#endif
            messageHandlers.cb(&tasks_subscribers_api.message);
            count++;
        }
        else{
            //SYSCALL_PRINTF(0,0,0,"Não Recebeu mensagem");
        }
        if (count == c_exit) {
            SYSCALL_PRINTF(0,0,0,blocks_print);
            SYSCALL_PRINTF(1,0,0,(tasks_subscribers_api.message.payloadlen));
            break;
        }
    }
}

void MQSoCSubscribe(MQSoCTopic topicName, Callback_p callBack, int n_publishers)
//void MQSoCSubscribe(MQSoCTopic topicName)
{
    messageHandlers.cb = callBack;
    SYSCALL_MQSOC_SUBSCRIBE(&topicName,0,0,n_publishers);
}


void trata_callback(MQSoCMessage* mqMessage){



}
