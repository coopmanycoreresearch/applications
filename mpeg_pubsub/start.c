/*---------------------------------------------------------------------
TITLE: Program Scheduler
AUTHOR: Nicolas Saint-jean
EMAIL : saintjea@lirmm.fr
DATE CREATED: 04/04/06
FILENAME: task2.c
PROJECT: Network Process Unit
COPYRIGHT: Software placed into the public domain by the author.
Software 'as is' without warranty.  Author liable for nothing.
DESCRIPTION: This file contains the task2
---------------------------------------------------------------------*/
#include "mpeg_pubsub.h"

unsigned int vlc_array[] = {
    0xfa,0xb8,0x20,0x05,0x20,0x20,0x02,0x38,
    0x20,0x7e,0x7f,0xf0,0x10,0x3f,0x54,0x8a,
    0x08,0x1f,0xa8,0x00,0x42,0x00,0xd2,0x80,
    0x3e,0xf6,0xa0,0x0e,0x3e,0x45,0x80,0x3e,
    0xc0,0x07,0xbc,0x79,0x00,0x3f,0xc2,0x28,
    0xb2,0x3f,0x0e,0x78,0xbe,0x88,0x9c,0x82,
    0x17,0xfc,0x11,0xbc,0x85,0x74,0x27,0xa7,
    0xf2,0x24,0x02,0xce,0x5f,0xc7,0xce,0x4e,
    0xa7,0x3c,0x73,0xb6,0x31,0x01,0x01,0x01,
    0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
    0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
    0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
    0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
    0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
    0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
    0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,};

static char start_print[]=  "MPEG A Start";
static char end_print[]=    "MPEG A End";

//Message msg1;
MQSoCMessage message;
MQSoCTopic topicName;


int start(){

    int i;

    SYSCALL_PRINTF(0,0,0,start_print);


    topicName.topic_id = 1;

    MQSoCAdvertise(topicName);

    //msg1.length = 128;
    message.payloadlen = 128;
    message.n_subscribers = 0;

    for(i=0; i<message.payloadlen; i++)
    {
        message.payload[i] = vlc_array[i];
        //msg1.msg[i] =vlc_array[i];
    }
    int k;
    for(i=0;i<MPEG_FRAMES;i++)                          // send 8 times the array to task 2
    {
        //SYSCALL_SEND(IVLC,0,0,&msg1);
        MQSoCPublish(&topicName, &message);
        for(k=0;k<PUBLISH_DELAY;k++); //sleep()
    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_END_TASK(START,0,0,0);
}

void MQSoCAdvertise(MQSoCTopic topicName)
{
    SYSCALL_MQSOC_ADVERTISE(&topicName,0,0,0);
}

void MQSoCPublish(MQSoCTopic* tpName, MQSoCMessage* mqMessage)
{
    mqMessage->pub_ok = 0;
    while (mqMessage->pub_ok == 0) {
        SYSCALL_MQSOC_PUBLISH(tpName,0,0,mqMessage);
        //SYSCALL_MQSOC_PUBLISH(0,0,0,&message);
        //SYSCALL_PRINTF(0,0,0,"mqMessage->pub_ok");
        //SYSCALL_PRINTF(1,0,0,mqMessage->pub_ok);
    }
}


