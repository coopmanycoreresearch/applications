/*---------------------------------------------------------------------
TITLE: Program Scheduler
AUTHOR: Nicolas Saint-jean
EMAIL : saintjea@lirmm.fr
DATE CREATED: 04/04/06
FILENAME: task3.c
PROJECT: Network Process Unit
COPYRIGHT: Software placed into the public domain by the author.
Software 'as is' without warranty.  Author liable for nothing.
DESCRIPTION: This file contains the task3
---------------------------------------------------------------------*/

//#include <stdlib.h>
#include "mpeg.h"

Message msg1;

static char start_print[]=  "MPEG E Start";
static char blocks_print[]= "blocks";
static char end_print[]=    "MPEG E End";

int print()
{
    int i;
    
    SYSCALL_PRINTF(0,0,0,start_print);

    for (i=0;i<MPEG_FRAMES;i++) {
        SYSCALL_RCV(IDCT,0,0,&msg1);
    }

    SYSCALL_PRINTF(0,0,0,blocks_print);

    SYSCALL_PRINTF(1,0,0,(msg1.length));

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(PRINT,0,0,0);
}
