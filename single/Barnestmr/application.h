#ifndef application_H
#define application_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#define ITERATIONS 50

void*  vAppTaskA();
void*  vAppTaskB();
void*  vAppTaskC();

void vTask( void * pvParameters );

#endif
