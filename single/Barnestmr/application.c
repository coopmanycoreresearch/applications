#include "application.h"
#include "barneshut.h"

int finished=0;

void vTask( void * pvParameters ){

xTaskHandle xTaskA,xTaskB,xTaskC;

    xTaskCreate ( &vAppTaskA, "AppTaskA", 256, NULL, PRIORITY+2U , &xTaskA);
    xTaskCreate ( &vAppTaskB, "AppTaskB", 256, NULL, PRIORITY+2U , &xTaskB);
    xTaskCreate ( &vAppTaskC, "AppTaskC", 256, NULL, PRIORITY+2U , &xTaskC);
    
    while(finished<3){ taskYIELD(); }

TMR_voter();

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}


#define SIZER 1e7
#define MASS 6e24
#define _POINT3NT_ 40

const int POINT3NT = _POINT3NT_;

void init();
void deinit();
void draw();
void update();

/* The radius of each body in the system */
int bodysize = SIZER*0.005;
int sumA,sumB,sumC;

float Amass[_POINT3NT_];
float Bmass[_POINT3NT_];
float Cmass[_POINT3NT_];
float Aposition_x[_POINT3NT_];
float Bposition_x[_POINT3NT_];
float Cposition_x[_POINT3NT_];
float Aposition_y[_POINT3NT_];
float Bposition_y[_POINT3NT_];
float Cposition_y[_POINT3NT_];
float Aposition_z[_POINT3NT_];
float Bposition_z[_POINT3NT_];
float Cposition_z[_POINT3NT_];
float Avelocity_x[_POINT3NT_];
float Bvelocity_x[_POINT3NT_];
float Cvelocity_x[_POINT3NT_];
float Avelocity_y[_POINT3NT_];
float Bvelocity_y[_POINT3NT_];
float Cvelocity_y[_POINT3NT_];
float Avelocity_z[_POINT3NT_];
float Bvelocity_z[_POINT3NT_];
float Cvelocity_z[_POINT3NT_];
int Aforce_x[_POINT3NT_];
int Bforce_x[_POINT3NT_];
int Cforce_x[_POINT3NT_];
int Rforce_x[_POINT3NT_];
int Aforce_y[_POINT3NT_];
int Bforce_y[_POINT3NT_];
int Cforce_y[_POINT3NT_];
int Rforce_y[_POINT3NT_];
int Aforce_z[_POINT3NT_];
int Bforce_z[_POINT3NT_];
int Cforce_z[_POINT3NT_];
int Rforce_z[_POINT3NT_];

BarnesHut *bhA;
BarnesHut *bhB;
BarnesHut *bhC;


void* vAppTaskA(void){
  
    updateA();
            
    // the following will never be encountered
    finished++;
    vTaskDelete(NULL);
}   
    
void* vAppTaskB(void){
    
    updateB();
            
    // the following will never be encountered
    finished++;
    vTaskDelete(NULL);
}   
    
void* vAppTaskC(void){
    
    updateC();
            
    // the following will never be encountered
    finished++;
    vTaskDelete(NULL);
}   
    
void init() {
    bhA = bhB = bhC = NULL;
    int i;
    /* Randomize the starting positions and initial velocities of the bodies */
    sumA = sumB = sumC = 0;
    for (i = 0; i < POINT3NT; i++) {	  
        Amass[i] = Bmass[i] = Cmass[i] = MASS;
        Aposition_x[i] = Bposition_x[i] = Cposition_x[i] = (rand()/((float)RAND_MAX))*SIZER*2 - SIZER;
        Aposition_y[i] = Bposition_y[i] = Cposition_y[i] = (rand()/((float)RAND_MAX))*SIZER*2 - SIZER;
        Aposition_z[i] = Bposition_z[i] = Cposition_z[i] = (rand()/((float)RAND_MAX))*SIZER*2 - SIZER;
        Avelocity_x[i] = Bvelocity_x[i] = Cvelocity_x[i] = (rand()/((float)RAND_MAX))*-20000.0f + 10000.0f;
        Avelocity_y[i] = Bvelocity_y[i] = Cvelocity_y[i] = (rand()/((float)RAND_MAX))*-20000.0f + 10000.0f;
        Avelocity_z[i] = Bvelocity_z[i] = Cvelocity_z[i] = (rand()/((float)RAND_MAX))*-20000.0f + 10000.0f;
        Aforce_x[i] = Aforce_y[i] = Aforce_z[i] = 0;
        Bforce_x[i] = Bforce_y[i] = Bforce_z[i] = 0;
        Cforce_x[i] = Cforce_y[i] = Cforce_z[i] = 0;
        Rforce_x[i] = Rforce_y[i] = Rforce_z[i] = 0;
    }

}

/* Stores the minimum and maximum bounds of the system */
float bound_min_x = -SIZER;
float bound_min_y = -SIZER;
float bound_min_z = -SIZER;
float bound_max_x = SIZER;
float bound_max_y = SIZER;
float bound_max_z = SIZER;


void updateA() {
	
    /* Create, fill, and finalize a new Barnes-Hut setup */
    bhA = BarnesHut_malloc(bound_min_x,bound_min_y,bound_min_z,bound_max_x,bound_max_y,bound_max_z);
    int i;
    for (i = 0; i < POINT3NT; i++) {
        BarnesHut_add(bhA, Aposition_x[i], Aposition_y[i], Aposition_z[i], Amass[i]);
    }    
    BarnesHut_finalize(bhA);
    
    for ( i = 0; i < POINT3NT; i++) {
        /* get the force of body[i] */
        BarnesHut_force(bhA, Aposition_x[i], Aposition_y[i], Aposition_z[i], Amass[i],&Aforce_x[i], &Aforce_y[i], &Aforce_z[i]);
        sumA+=Aforce_x[i]+Aforce_y[i]+Aforce_z[i];
    }
    
    BarnesHut_free(bhA);
    bhA = NULL;
  
}

void updateB() {
	
    /* Create, fill, and finalize a new Barnes-Hut setup */
    bhB = BarnesHut_malloc(bound_min_x,bound_min_y,bound_min_z,bound_max_x,bound_max_y,bound_max_z);
    int i;
    for (i = 0; i < POINT3NT; i++) {
        BarnesHut_add(bhB, Bposition_x[i], Bposition_y[i], Bposition_z[i], Bmass[i]);
    }    
    BarnesHut_finalize(bhB);
    
    for ( i = 0; i < POINT3NT; i++) {
        /* get the force of body[i] */
        BarnesHut_force(bhB, Bposition_x[i], Bposition_y[i], Bposition_z[i], Bmass[i],&Bforce_x[i], &Bforce_y[i], &Bforce_z[i]);
        sumB+=Bforce_x[i]+Bforce_y[i]+Bforce_z[i];
    }
    
    BarnesHut_free(bhB);
    bhB = NULL;
  
}

void updateC() {
	
    /* Create, fill, and finalize a new Carnes-Hut setup */
    bhC = BarnesHut_malloc(bound_min_x,bound_min_y,bound_min_z,bound_max_x,bound_max_y,bound_max_z);
    int i;
    for (i = 0; i < POINT3NT; i++) {
        BarnesHut_add(bhC, Cposition_x[i], Cposition_y[i], Cposition_z[i], Cmass[i]);
    }    
    BarnesHut_finalize(bhC);
    
    for ( i = 0; i < POINT3NT; i++) {
        /* get the force of body[i] */
        BarnesHut_force(bhC, Cposition_x[i], Cposition_y[i], Cposition_z[i], Cmass[i],&Cforce_x[i], &Cforce_y[i], &Cforce_z[i]);
        sumC+=Cforce_x[i]+Cforce_y[i]+Cforce_z[i];
    }
    
    BarnesHut_free(bhC);
    bhC = NULL;
  
}

void TMR_voter()
{
    int i,j;
    int ra=0,rb=0,rc=0,r=0;
    //tmr test
    for (i=0; i<POINT3NT; i++)
    {
        if (((Cforce_x[i] == Bforce_x[i]) && (Cforce_x[i] == Aforce_x[i])) 
                && ((Cforce_y[i] == Bforce_y[i]) && (Cforce_y[i] == Aforce_y[i])) 
                && ((Cforce_z[i] == Bforce_z[i]) && (Cforce_z[i] == Aforce_z[i]))) {
                    // no error
                    r++;
                    Rforce_x[i] = Cforce_x[i];
                    Rforce_y[i] = Cforce_y[i];
                    Rforce_z[i] = Cforce_z[i];
            
        } else {
            if ((Cforce_x[i] == Bforce_x[i])
                && (Cforce_y[i] == Bforce_y[i])
                && (Cforce_z[i] == Bforce_z[i])) {
                    // matrix 1 is wrong
                    ra++;
                    Rforce_x[i] = Aforce_x[i] = Cforce_x[i];
                    Rforce_y[i] = Aforce_y[i] = Cforce_y[i];
                    Rforce_z[i] = Aforce_z[i] = Cforce_z[i];
            } else {
                if ((Cforce_x[i] == Aforce_x[i])
                    && (Cforce_y[i] == Aforce_y[i])
                    && (Cforce_z[i] == Aforce_z[i])) {
                        // matrix 2 is wrong
                        rb++;
                        Rforce_x[i] = Bforce_x[i] = Cforce_x[i];
                        Rforce_y[i] = Bforce_y[i] = Cforce_y[i];
                        Rforce_z[i] = Bforce_z[i] = Cforce_z[i];
                } else {
                    if ((Bforce_x[i] == Aforce_x[i])
                        && (Bforce_y[i] == Aforce_y[i]) 
                        && (Bforce_z[i] == Aforce_z[i])) {
                            // matrix 3 is wrong
                            rc++;
                            Rforce_x[i] = Cforce_x[i] = Aforce_x[i];
                            Rforce_y[i] = Cforce_y[i] = Aforce_y[i];
                            Rforce_z[i] = Cforce_z[i] = Aforce_z[i];
                    }
                }
            }
        }
    }
#ifdef DEBUG
    printf("RESULT %d\t T1 %d\t T2 %d\t T3 %d\n",r,ra,rb,rc);
    printf("SUMA = %d\t SUMB = %d\t SUMC = %d\n",sumA,sumB,sumC);
#endif
    
    if(r==POINT3NT){
#ifdef DEBUG
        printf("CORRECT\n");
#endif
        TMRWrite(0);
    }else if (ra+r==POINT3NT) {
#ifdef DEBUG
        printf("T1_INFLUENCED\n");
#endif
        TMRWrite(1);
    }else if (rb+r==POINT3NT) {
#ifdef DEBUG
        printf("T3_INFLUENCED\n");
#endif
        TMRWrite(3);
    }else if (rc+r==POINT3NT) {
#ifdef DEBUG
        printf("T2_INFLUENCED\n");
#endif
        TMRWrite(2);
    } else if (rb+rc+r==POINT3NT) {
#ifdef DEBUG
		printf("T2_T3_INFLUENCED\n");
#endif
        TMRWrite(5);
	} else if (ra+rc+r==POINT3NT) {
#ifdef DEBUG
		printf("T1_T2_INFLUENCED\n");
#endif
        TMRWrite(4);
	} else if (ra+rb+r==POINT3NT) {
#ifdef DEBUG
		printf("T1_T3_INFLUENCED\n");
#endif
        TMRWrite(6);
	} else if (ra+rb+rc+r==POINT3NT) {
#ifdef DEBUG
		printf("T1_T2_T3_INFLUENCED\n");
#endif
        TMRWrite(7);
	} else {
#ifdef DEBUG
		printf("ERROR\n");
#endif
        TMRWrite(8);
	}
    return;
}
