#include "application.h"
#include "snipmath.h"
#include <math.h>

int finished=0;

void vTask( void * pvParameters ){

xTaskHandle xTaskA,xTaskB,xTaskC;

    xTaskCreate ( &vAppTaskA, "AppTaskA", 256, NULL, PRIORITY+2U , &xTaskA);
    xTaskCreate ( &vAppTaskB, "AppTaskB", 256, NULL, PRIORITY+2U , &xTaskB);
    xTaskCreate ( &vAppTaskC, "AppTaskC", 256, NULL, PRIORITY+2U , &xTaskC);
    
    while(finished<3){ taskYIELD(); }

TMR_voter();

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

    int radA[DEG],radB[DEG],radC[DEG],rad[DEG];
    int degA[DEG],degB[DEG],degC[DEG],deg[DEG];
    int xA[3],xB[3],xC[3],x[3];
    int solutionsA,solutionsB,solutionsC;
    struct int_sqrt qA,qB,qC,q;
    long n = 0;

void*  vAppTaskA()
{
    double  a1 = 1.0, b1 = -10.5, c1 = 32.0, d1 = -30.0;
    double  a2 = 1.0, b2 = -4.5, c2 = 17.0, d2 = -30.0;
    double  a3 = 1.0, b3 = -3.5, c3 = 22.0, d3 = -31.0;
    double  a4 = 1.0, b4 = -13.7, c4 = 1.0, d4 = -35.0;
    unsigned long l = 0x3fed0169L;
    int i=0;
    double X=0.0;
    /* solve soem cubic functions */
    
    /* should get 3 solutions: 2, 6 & 2.5   */
    SolveCubic(a1, b1, c1, d1, &solutionsA, xA);  
    // printf("%d) xA %d : %d : %d\n",solutionsA,xA[0],xA[1],xA[2]);
    /* should get 1 solution: 2.5           */
    SolveCubic(a2, b2, c2, d2, &solutionsA, xA);  
    
    SolveCubic(a3, b3, c3, d3, &solutionsA, xA);
    
    SolveCubic(a4, b4, c4, d4, &solutionsA, xA);
    
    /* Now solve some random equations */
    for(a1=1;a1<2;a1++) {
        for(b1=2;b1>0;b1--) {
            for(c1=5;c1<15;c1+=0.5) {
                for(d1=-1;d1>-11;d1--) {
                    SolveCubic(a1, b1, c1, d1, &solutionsA, xA);
                }
            }
        }
    }

    /* perform some integer square roots */
    for (i = 0; i < 1001; ++i) {
        usqrt(i, &qA);
    }

    usqrt(l, &qA);

    /* convert some rads to degrees */
    for (X = 0.0 , i=0; X <= 360.0; X += 1.0) {
        radA[i]=100000*deg2rad(X);
        i++;
    }

    for (X = 0.0 , i = 0; X <= (2 * PI + 1e-6); X += (PI / 180)) {
        degA[i]=rad2deg(X);
        i++;
    }
    finished++;

vTaskDelete(NULL);
}

void*  vAppTaskB()
{
    double  a1 = 1.0, b1 = -10.5, c1 = 32.0, d1 = -30.0;
    double  a2 = 1.0, b2 = -4.5, c2 = 17.0, d2 = -30.0;
    double  a3 = 1.0, b3 = -3.5, c3 = 22.0, d3 = -31.0;
    double  a4 = 1.0, b4 = -13.7, c4 = 1.0, d4 = -35.0;
    unsigned long l = 0x3fed0169L;
    int i=0;
    double X=0.0;
    /* solve soem cubic functions */
    
    /* should get 3 solutions: 2, 6 & 2.5   */
    SolveCubic(a1, b1, c1, d1, &solutionsB, xB);  
    // printf("%d) xB %d : %d : %d\n",solutionsB,xB[0],xB[1],xB[2]);
    /* should get 1 solution: 2.5           */
    SolveCubic(a2, b2, c2, d2, &solutionsB, xB);  
    
    SolveCubic(a3, b3, c3, d3, &solutionsB, xB);
    
    SolveCubic(a4, b4, c4, d4, &solutionsB, xB);
    
    /* Now solve some random equations */
    for(a1=1;a1<2;a1++) {
        for(b1=2;b1>0;b1--) {
            for(c1=5;c1<15;c1+=0.5) {
                for(d1=-1;d1>-11;d1--) {
                    SolveCubic(a1, b1, c1, d1, &solutionsB, xB);
                }
            }
        }
    }

    /* perform some integer square roots */
    for (i = 0; i < 1001; ++i) {
        usqrt(i, &qB);
    }

    usqrt(l, &qB);

    /* convert some rads to degrees */
    for (X = 0.0 , i=0; X <= 360.0; X += 1.0) {
        radB[i]=100000*deg2rad(X);
        i++;
    }

    for (X = 0.0 , i = 0; X <= (2 * PI + 1e-6); X += (PI / 180)) {
        degB[i]=rad2deg(X);
        i++;
    }
    finished++;

vTaskDelete(NULL);
}

void*  vAppTaskC()
{
    double  a1 = 1.0, b1 = -10.5, c1 = 32.0, d1 = -30.0;
    double  a2 = 1.0, b2 = -4.5, c2 = 17.0, d2 = -30.0;
    double  a3 = 1.0, b3 = -3.5, c3 = 22.0, d3 = -31.0;
    double  a4 = 1.0, b4 = -13.7, c4 = 1.0, d4 = -35.0;
    unsigned long l = 0x3fed0169L;
    int i=0;
    double X=0.0;
    /* solve soem cubic functions */
    
    /* should get 3 solutions: 2, 6 & 2.5   */
    SolveCubic(a1, b1, c1, d1, &solutionsC, xC);  
    // printf("%d) xC %d : %d : %d\n",solutionsC,xC[0],xC[1],xC[2]);
    /* should get 1 solution: 2.5           */
    SolveCubic(a2, b2, c2, d2, &solutionsC, xC);  
    
    SolveCubic(a3, b3, c3, d3, &solutionsC, xC);
    
    SolveCubic(a4, b4, c4, d4, &solutionsC, xC);
    
    /* Now solve some random equations */
    for(a1=1;a1<2;a1++) {
        for(b1=2;b1>0;b1--) {
            for(c1=5;c1<15;c1+=0.5) {
                for(d1=-1;d1>-11;d1--) {
                    SolveCubic(a1, b1, c1, d1, &solutionsC, xC);
                }
            }
        }
    }

    /* perform some integer square roots */
    for (i = 0; i < 1001; ++i) {
        usqrt(i, &qC);
    }

    usqrt(l, &qC);

    /* convert some rads to degrees */
    for (X = 0.0 , i=0; X <= 360.0; X += 1.0) {
        radC[i]=100000*deg2rad(X);
        i++;
    }

    for (X = 0.0 , i = 0; X <= (2 * PI + 1e-6); X += (PI / 180)) {
        degC[i]=rad2deg(X);
        i++;
    }
    finished++;

vTaskDelete(NULL);
}

void TMR_voter()
{
    int i,j;
    int ra=0,rb=0,rc=0,r=0;
    //tmr test
    // printf("qA %d : %d\n",qA.frac,qA.sqrt);
    // printf("qB %d : %d\n",qB.frac,qB.sqrt);
    // printf("qC %d : %d\n",qC.frac,qC.sqrt);
    if ( (qA.frac == qB.frac) && (qA.frac == qC.frac) 
            && (qA.sqrt == qB.sqrt) && (qA.sqrt == qC.sqrt) ) {
                r++;
                q.frac = qA.frac;
                q.sqrt = qA.sqrt;
    } else {
        if ( (qB.frac == qC.frac) 
            && (qB.sqrt == qC.sqrt) ) {
                ra++;
                q.frac = qA.frac = qB.frac;
                q.sqrt = qA.sqrt = qB.sqrt;
            } else {
                if ( (qA.frac == qC.frac) 
                    && (qA.sqrt == qC.sqrt) ) {
                        rb++;
                        q.frac = qB.frac = qA.frac;
                        q.sqrt = qB.sqrt = qA.sqrt;
                } else {
                    if ( (qA.frac == qB.frac) 
                        && (qA.sqrt == qB.sqrt) ) {
                            rc++;
                            q.frac = qC.frac = qA.frac;
                            q.sqrt = qC.sqrt = qA.sqrt;
                    }
                }
            }
    }
    
    for (i=0; i<3; i++)
    {
        // printf("%d) xA %d : ",i,xA[i]);
        // printf("xB %d : ",xB[i]);
        // printf("xC %d \n",xC[i]);
        if( (xA[i] == xB[i]) && (xA[i] == xC[i]) ) {
            r++;
            x[i] = xA;
        } else {
            if( xB[i] == xC[i] ) {
                ra++;
                x[i] = xA[i] = xB[i];
            } else {
                if( xA[i] == xC[i] ) {
                    rb++;
                    x[i] = xB[i] = xA[i];
                } else {
                    if( xA[i] == xB[i] ) {
                        rc++;
                        x[i] = xC[i] = xA[i];
                    }
                }
            }
        }
    }
    // printf("RESULT %d\t T1 %d\t T2 %d\t T3 %d\n",r,ra,rb,rc);
    for (i=0; i<361; i++)
    {
        // printf("A x %d y %d z %d\nB x %d y %d z %d \nC x %d y %d z %d \n",Aforce_x[i],Aforce_y[i],Aforce_z[i],Bforce_x[i],Bforce_y[i],Bforce_z[i],Cforce_x[i],Cforce_y[i],Cforce_z[i]);
        if (((radA[i] == radC[i]) && (radB[i] == radC[i])) 
                && ((degA[i] == degC[i]) && (degB[i] == degC[i]))) {
                    // no error
                    r++;
                    rad[i] = radA[i];
                    deg[i] = degA[i];
            
        } else {
            if ((radB[i] == radC[i])
                && (degB[i] == degC[i])) {
                    // matrix 1 is wrong
                    ra++;
                    rad[i] = radA[i] = radC[i];
                    deg[i] = degA[i] = degC[i];
            } else {
                if ((radA[i] == radC[i])
                    && (degA[i] == degC[i])) {
                        // matrix 2 is wrong
                        rb++;
                        rad[i] = radB[i] = radC[i];
                        deg[i] = degB[i] = degC[i];
                } else {
                    if ((radA[i] == radB[i])
                        && (degA[i] == degB[i])) {
                            // matrix 3 is wrong
                            rc++;
                            rad[i] = radC[i] = radA[i];
                            deg[i] = degC[i] = degA[i];
                    }
                }
            }
        }
    }
#ifdef DEBUG
    printf("RESULT %d\t T1 %d\t T2 %d\t T3 %d\n",r,ra,rb,rc);
    // printf("SUMA = %d\t SUMB = %d\t SUMC = %d\n",sum1,sum2,sum3);
#endif

    
    if(r==365){
#ifdef DEBUG
        printf("CORRECT\n");
#endif
        TMRWrite(0);
    }else if (ra+r==365) {
#ifdef DEBUG
        printf("T1_INFLUENCED\n");
#endif
        TMRWrite(1);
    }else if (rb+r==365) {
#ifdef DEBUG
        printf("T3_INFLUENCED\n");
#endif
        TMRWrite(3);
    }else if (rc+r==365) {
#ifdef DEBUG
        printf("T2_INFLUENCED\n");
#endif
        TMRWrite(2);
    } else if (rb+rc+r==365) {
#ifdef DEBUG
		printf("T2_T3_INFLUENCED\n");
#endif
        TMRWrite(5);
	} else if (ra+rc+r==365) {
#ifdef DEBUG
		printf("T1_T2_INFLUENCED\n");
#endif
        TMRWrite(4);
	} else if (ra+rb+r==365) {
#ifdef DEBUG
		printf("T1_T3_INFLUENCED\n");
#endif
        TMRWrite(6);
	} else if (ra+rb+rc+r==365) {
#ifdef DEBUG
		printf("T1_T2_T3_INFLUENCED\n");
#endif
        TMRWrite(7);
	} else {
#ifdef DEBUG
		printf("ERROR\n");
#endif
        TMRWrite(8);
	}
    return;
}
