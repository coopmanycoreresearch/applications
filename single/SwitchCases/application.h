#ifndef APPLICATION_H
#define APPLICATION_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


#define LOOP 100

void vTask( void * pvParameters );
void *vAppTaskA(void* arg);
void *vAppTaskB(void* arg);
void *vAppTaskC(void* arg);
#endif
