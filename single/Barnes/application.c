#include "application.h"
#include "barneshut.h"

void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

#define SIZER 1e7
#define MASS 6e24
#define _POINTCNT_ 40

const int POINTCNT = _POINTCNT_;

void init();
void deinit();
void draw();
void update();

/* The radius of each body in the system */
int bodysize = SIZER*0.005;
int sum;

float mass[_POINTCNT_];
float position_x[_POINTCNT_];
float position_y[_POINTCNT_];
float position_z[_POINTCNT_];
float velocity_x[_POINTCNT_];
float velocity_y[_POINTCNT_];
float velocity_z[_POINTCNT_];
int force_x[_POINTCNT_];
int force_y[_POINTCNT_];
int force_z[_POINTCNT_];

BarnesHut *bh;


void* vAppTask(void){
   
    init();
    update();
#ifdef DEBUG
    printf("SUM = %d\n",sum);
#endif    

vTaskDelete(NULL);
}

void init() {
    bh = NULL;
    int i;
    /* Randomize the starting positions and initial velocities of the bodies */
    sum=0;
    for (i = 0; i < POINTCNT; i++) {	  
        mass[i] = MASS;
        position_x[i] = (rand()/((float)RAND_MAX))*SIZER*2 - SIZER;
        position_y[i] = (rand()/((float)RAND_MAX))*SIZER*2 - SIZER;
        position_z[i] = (rand()/((float)RAND_MAX))*SIZER*2 - SIZER;
        velocity_x[i] = (rand()/((float)RAND_MAX))*-20000.0f + 10000.0f;
        velocity_y[i] = (rand()/((float)RAND_MAX))*-20000.0f + 10000.0f;
        velocity_z[i] = (rand()/((float)RAND_MAX))*-20000.0f + 10000.0f;
        force_x[i] = force_y[i] = force_z[i] = 0.0f;    
    }

}

/* Stores the minimum and maximum bounds of the system */
float bound_min_x = -SIZER;
float bound_min_y = -SIZER;
float bound_min_z = -SIZER;
float bound_max_x = SIZER;
float bound_max_y = SIZER;
float bound_max_z = SIZER;


void update() {
	
    /* Create, fill, and finalize a new Barnes-Hut setup */
    bh = BarnesHut_malloc(bound_min_x,bound_min_y,bound_min_z,bound_max_x,bound_max_y,bound_max_z);
    int i;
    for (i = 0; i < POINTCNT; i++) {
        BarnesHut_add(bh, position_x[i], position_y[i], position_z[i], mass[i]);
    }    
    BarnesHut_finalize(bh);
    
    for ( i = 0; i < POINTCNT; i++) {
        /* get the force of body[i] */
        BarnesHut_force(bh, position_x[i], position_y[i], position_z[i], mass[i],&force_x[i], &force_y[i], &force_z[i]);
        sum+=force_x[i]+force_y[i]+force_z[i];
    }
    
    BarnesHut_free(bh);
    bh = NULL;
  
}
