#ifndef FFT_H
#define FFT_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


#define ROWS 300
#define COLUMNS 300
#define N_TASKS 2
#define N_TMR 3

void* vAppTask (void *arg);
void vTask( void * pvParameters );
#endif /* FREERTOS_CONFIG_H */
