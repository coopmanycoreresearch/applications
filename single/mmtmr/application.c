#include "application.h"

int  a[ROWS][COLUMNS], // INPUT
    b[ROWS][COLUMNS], // INPUT
    c1[ROWS][COLUMNS], // TMR1
    c2[ROWS][COLUMNS], // TMR2
    c3[ROWS][COLUMNS], // TMR3
    cR[ROWS][COLUMNS]; // FINAL
int finished=0;
int sum1=0,
    sum2=0,
    sum3=0;
    
void *mult_task_tmr0(void *t)
{
    /*This function calculates ROWS/THREADS of the matrix*/
    int starting_row;
    int offset = ROWS/N_TASKS;
    starting_row = (int)t;
    starting_row = offset * starting_row;

    int i,j,k;
    for (i = starting_row; i<(starting_row+offset); i++) {
        for (j=0;j<COLUMNS;j++) {
            for (k=0;k<ROWS;k++) {
                c1[i][j] += (a[i][k] * b[k][j]);
            }
            sum1+=c1[i][j];
        }
    }
    finished++;
    vTaskDelete( NULL );
    return;
}

void *mult_task_tmr1(void *t)
{
    /*This function calculates ROWS/THREADS of the matrix*/

    int starting_row;
    int offset = ROWS/N_TASKS;
    starting_row = (int)t;
    starting_row = offset * starting_row;

    int i,j,k;
    for (i = starting_row; i<(starting_row+offset); i++) {
        for (j=0;j<COLUMNS;j++) {
            for (k=0;k<ROWS;k++) {
                c2[i][j] += (a[i][k] * b[k][j]);
            }
            sum2+=c2[i][j];
        }
    }
    finished++;
    vTaskDelete( NULL );
    return;
}

void *mult_task_tmr2(void *t)
{
    /*This function calculates ROWS/THREADS of the matrix*/

    int starting_row;
    int offset = ROWS/N_TASKS;
    starting_row = (int)t;
    starting_row = offset * starting_row;

    int i,j,k;
    for (i = starting_row; i<(starting_row+offset); i++) {
        for (j=0;j<COLUMNS;j++) {
            for (k=0;k<ROWS;k++) {
                c3[i][j] += (a[i][k] * b[k][j]);
            }
            sum3+=c3[i][j];
        }
    }
    finished++;
    
    vTaskDelete( NULL );
    return;
}


void TMR_voter()
{
    int i,j;
    int r0=0,r1=0,r2=0,r=0;
    //tmr test
    for (i=0; i<ROWS; i++)
    {
        for(j=0; j<COLUMNS; j++)
        {
            if ((c3[i][j] == c1[i][j]) && (c1[i][j] == c2[i][j])) {
                // no error
                r++;
                cR[i][j] = c1[i][j];
            } else {
                if((c2[i][j] == c3[i][j]) )
                {
                    // matrix 1 is wrong
                    r0++;
                    cR[i][j] = c3[i][j];
                    c1[i][j] = c3[i][j];
                } else {
                    if((c1[i][j] == c3[i][j]) )
                    {
                        // matrix 2 is wrong
                        r1++;
                        cR[i][j] = c1[i][j];
                        c2[i][j] = c1[i][j];
                    } else {
                        if((c1[i][j] == c2[i][j]) )
                        {
                            // matrix 3 is wrong
                            r2++;
                            cR[i][j] = c1[i][j];
                            c3[i][j] = c1[i][j];
                        }
                    }
                }
             }
            
        }
    }
#ifdef DEBUG
    printf("RESULT %d\t T1 %d\t T2 %d\t T3 %d\n",r,r0,r1,r2);
    printf("SUM1 = %d\t SUM2 = %d\t SUM3 = %d\n",sum1,sum2,sum3);
#endif
    
    if(r==ROWS*COLUMNS){
#ifdef DEBUG
        printf("CORRECT\n");
#endif
        TMRWrite(0);
    }else if (r0+r==ROWS*COLUMNS) {
#ifdef DEBUG
        printf("T1_INFLUENCED\n");
#endif
        TMRWrite(1);
    }else if (r1+r==ROWS*COLUMNS) {
#ifdef DEBUG
        printf("T3_INFLUENCED\n");
#endif
        TMRWrite(3);
    }else if (r2+r==ROWS*COLUMNS) {
#ifdef DEBUG
        printf("T2_INFLUENCED\n");
#endif
        TMRWrite(2);
    } else if (r1+r2+r==ROWS*COLUMNS) {
#ifdef DEBUG
		printf("T2_T3_INFLUENCED\n");
#endif
        TMRWrite(5);
	} else if (r0+r2+r==ROWS*COLUMNS) {
#ifdef DEBUG
		printf("T1_T2_INFLUENCED\n");
#endif
        TMRWrite(4);
	} else if (r0+r1+r==ROWS*COLUMNS) {
#ifdef DEBUG
		printf("T1_T3_INFLUENCED\n");
#endif
        TMRWrite(6);
	} else if (r0+r1+r2+r==ROWS*COLUMNS) {
#ifdef DEBUG
		printf("T1_T2_T3_INFLUENCED\n");
#endif
        TMRWrite(7);
	} else {
#ifdef DEBUG
		printf("ERROR\n");
#endif
        TMRWrite(8);
	}
    return;
}

void vTask( void * pvParameters )
{

int i,j;
xTaskHandle p0[N_TASKS],
            p1[N_TASKS],
            p2[N_TASKS];
    
    /*** Initialize matrixes ***/
    for (i=0; i<ROWS; i++) {
        for (j=0; j<COLUMNS; j++) {
            if(i==j || (i+j) == (ROWS-1)){
                a[i][j]= 1;
            } else {
                a[i][j]= 0;
            }
        }
    }

    for (i=0; i<ROWS; i++) {
        for (j=0; j<COLUMNS; j++) {
            b[i][j]= 1;
        }
    }

    for (i=0; i<ROWS; i++) {
        for (j=0; j<COLUMNS; j++) {
            c1[i][j]= 0;
            c2[i][j]= 0;
            c3[i][j]= 0;
        }
    }

    /*
        In this technique test, whe execute the whole loop two times.
        Another approach could be to put c2[i] += a[i] * b[j]; inside the one and
        only loop, which will be tested further on.
    */
    for (j=0; j<N_TASKS; j++) {
        xTaskCreate ( &mult_task_tmr0, "AppTaskA", 256, (void*)j, PRIORITY+2U , &p0[j]);
        xTaskCreate ( &mult_task_tmr1, "AppTaskB", 256, (void*)j, PRIORITY+2U , &p1[j]);
        xTaskCreate ( &mult_task_tmr2, "AppTaskC", 256, (void*)j, PRIORITY+2U , &p2[j]);
    }

    while(finished<(N_TASKS*3)){ taskYIELD(); }
    
    TMR_voter();
    
#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

