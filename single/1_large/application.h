#ifndef adpcm_H
#define adpcm_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


void* vAppTask (void *arg);

void vTask( void * pvParameters );

#endif
