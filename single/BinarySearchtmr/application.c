#include "application.h"
/*
 
 Total number of instructions 423065
 cycles -+ 665176
 
 * /


/*************************************************************************/
/*                                                                       */
/*   SNU-RT Benchmark Suite for Worst Case Timing Analysis               */
/*   =====================================================               */
/*                              Collected and Modified by S.-S. Lim      */
/*                                           sslim@archi.snu.ac.kr       */
/*                                         Real-Time Research Group      */
/*                                        Seoul National University      */
/*                                                                       */
/*                                                                       */
/*        < Features > - restrictions for our experimental environment   */
/*                                                                       */
/*          1. Completely structured.                                    */
/*               - There are no unconditional jumps.                     */
/*               - There are no exit from loop bodies.                   */
/*                 (There are no 'break' or 'return' in loop bodies)     */
/*          2. No 'switch' statements.                                   */
/*          3. No 'do..while' statements.                                */
/*          4. Expressions are restricted.                               */
/*               - There are no multiple expressions joined by 'or',     */
/*                'and' operations.                                      */
/*          5. No library calls.                                         */
/*               - All the functions needed are implemented in the       */
/*                 source file.                                          */
/*                                                                       */
/*                                                                       */
/*************************************************************************/
/*                                                                       */
/*  FILE: bs.c                                                           */
/*  SOURCE : Public Domain Code                                          */
/*                                                                       */
/*  DESCRIPTION :                                                        */
/*                                                                       */
/*     Binary search for the array of 15 integer elements.               */
/*                                                                       */
/*  REMARK :                                                             */
/*                                                                       */
/*  EXECUTION TIME :                                                     */
/*                                                                       */
/*                                                                       */
/*************************************************************************/

int finished=0;

void vTask( void * pvParameters ){

xTaskHandle xTaskA,xTaskB,xTaskC;

    xTaskCreate ( &vAppTaskA, "AppTaskA", 256, NULL, PRIORITY+2U , &xTaskA);
    xTaskCreate ( &vAppTaskB, "AppTaskB", 256, NULL, PRIORITY+2U , &xTaskB);
    xTaskCreate ( &vAppTaskC, "AppTaskC", 256, NULL, PRIORITY+2U , &xTaskC);
    
    while(finished<3){ taskYIELD(); }

TMR_voter();

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}


int m=0,mA=0,mB=0,mC=0;

struct DAT1 {
  int  key;
  int  value;
}  ;

#ifdef DEBUG
	int cnt1;
#endif 

struct DAT1 data[15] = { {1, 100},
	     {5,200},
	     {6, 300},
	     {7, 700},
	     {8, 900},
	     {9, 250},
	     {10, 400},
	     {11, 600},
	     {12, 800},
	     {13, 1500},
	     {14, 1200},
	     {15, 110},
	     {16, 140},
	     {17, 133},
	     {18, 10} };

void* vAppTaskA (void *arg)
{	
int i;
for(i=0;i<LOOP;i++)	
	 mA += binary_search(8);
	
	//printf ("%d\n",m); //900
finished++;
vTaskDelete(NULL);

}

void* vAppTaskB (void *arg)
{
int i;
for(i=0;i<LOOP;i++)
	 mB += binary_search(8);

	//printf ("%d\n",m); //900
finished++;
vTaskDelete(NULL);

}

void* vAppTaskC (void *arg)
{
int i;
for(i=0;i<LOOP;i++)
	 mC += binary_search(8);

	//printf ("%d\n",m); //900
finished++;
vTaskDelete(NULL);

}

binary_search(x)
{
  int fvalue, mid, up, low ;

  low = 0;
  up = 14;
  fvalue = -1 /* all data are positive */ ;
  while (low <= up) {
    mid = (low + up) >> 1;
    if ( data[mid].key == x ) {  /*  found  */
      up = low - 1;
      fvalue = data[mid].value;
    }
    else  /* not found */
      if ( data[mid].key > x ) 	{
	up = mid - 1;
      }
      else   {
             	low = mid + 1;
      }

  }

  return fvalue;
}
   
void TMR_voter()
{
    int i,j;
    int ra=0,rb=0,rc=0,r=0;

    //tmr test
    if ( (mA == mB) && (mA == mC) ) {
        // no error
        r++;
        m = mA;
    } else {
        if ( mC == mB ) {
            // matrix 1 is wrong
            ra++;
            m = mA = mB;
        } else {
            if ( mA == mC ) {
                // matrix 2 is wrong
                rb++;
                m = mB = mA;
            } else {
                if ( mA == mB ) {
                    // matrix 3 is wrong
                    rc++;
                    m = mC = mA;
                }
            }
        }
    }
#ifdef DEBUG
    printf("RESULT %d\t T1 %d\t T2 %d\t T3 %d\n",r,ra,rb,rc);
    // printf("SUMA = %d\t SUMB = %d\t SUMC = %d\n",sum1,sum2,sum3);
#endif

    if(r){
#ifdef DEBUG
        printf("CORRECT\n");
#endif
        TMRWrite(0);
    }else if (ra) {
#ifdef DEBUG
        printf("T1_INFLUENCED\n");
#endif
        TMRWrite(1);
    }else if (rb) {
#ifdef DEBUG
        printf("T3_INFLUENCED\n");
#endif
        TMRWrite(3);
    }else if (rc) {
#ifdef DEBUG
        printf("T2_INFLUENCED\n");
#endif
        TMRWrite(2);
	} else {
#ifdef DEBUG
		printf("ERROR\n");
#endif
        TMRWrite(8);
	}
    return;
}

