#include "application.h"

void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

/* MDH WCET BENCHMARK SUITE */
/*
 * Changes: CS 2006/05/19: Changed loop bound from constant to variable.
 */

int fac (int n)
{
  if (n == 0)
     return 1;
  else
     return (n * fac (n-1));
}

long int factorial ()
{
  int i ;
  long int s = 0;
  volatile int n;

  n = 25;
  for (i = 0;  i <= n; i++)
      s += fac (i);

  return (s);
}

void* vAppTask (void *arg)
{

long int i,ans = 0;
for( i = 0; i<LOOP; i++)
  ans = factorial(); 

#ifdef DEBUG
printf("\n %ld \n",ans);//712899098
#endif

  vTaskDelete(NULL);
  for(;;);    
}
