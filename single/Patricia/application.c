#include "application.h"

void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

struct MyNode {
	int foo;
	double bar;
};

struct in_addr {
    unsigned long s_addr;  // load with inet_aton()
};


void*  vAppTask()
{
	struct ptree *phead;
	struct ptree *p,*pfind;
	struct ptree_mask *pm;	
	char line[128];
	char addr_str[16];
	struct in_addr addr;
	unsigned long mask=0xffffffff;
	float time;
	
	/*
	 * Initialize the Patricia trie by doing the following:
	 *   1. Assign the head pointer a default route/default node
	 *   2. Give it an address of 0.0.0.0 and a mask of 0x00000000
	 *      (matches everything)
	 *   3. Set the bit position (p_b) to 0.
	 *   4. Set the number of masks to 1 (the default one).
	 *   5. Point the head's 'left' and 'right' pointers to itself.
	 * NOTE: This should go into an intialization function.
	 */
	phead = (struct ptree *)malloc(sizeof(struct ptree));

	bzero(phead, sizeof(*phead));
	phead->p_m = (struct ptree_mask *)malloc(
			sizeof(struct ptree_mask));
	
	bzero(phead->p_m, sizeof(*phead->p_m));
	pm = phead->p_m;
	pm->pm_data = (struct MyNode *)malloc(sizeof(struct MyNode));


	bzero(pm->pm_data, sizeof(*pm->pm_data));
	/*******
	 *
	 * Fill in default route/default node data here.
	 *
	 *******/
	phead->p_mlen = 1;
	phead->p_left = phead->p_right = phead;


	/*
	 * The main loop to insert nodes.
	 */
	int k; 
	for (k=0;k<ITERATIONS;k++)
	{
		/*
		 * Read in each IP address and mask and convert them to
		 * more usable formats.
		 */
		//sscanf(line, "%f %d", &time, (unsigned int *)&addr);
		addr.s_addr = (unsigned long) 1; 
		time = 759794400.242680*k;
		
		//inet_aton(addr_str, &addr);

		/*
		 * Create a Patricia trie node to insert.
		 */
		p = (struct ptree *)malloc(sizeof(struct ptree));

		bzero(p, sizeof(*p));

		/*
		 * Allocate the mask data.
		 */
		p->p_m = (struct ptree_mask *)malloc(
				sizeof(struct ptree_mask));
		
		bzero(p->p_m, sizeof(*p->p_m));

		/*
		 * Allocate the data for this node.
		 * Replace 'struct MyNode' with whatever you'd like.
		 */
		pm = p->p_m;
		pm->pm_data = (struct MyNode *)malloc(sizeof(struct MyNode));

		bzero(pm->pm_data, sizeof(*pm->pm_data));

		/*
		 * Assign a value to the IP address and mask field for this
		 * node.
		 */
		p->p_key = addr.s_addr;		/* Network-byte order */
		p->p_m->pm_mask = htonl(mask);

		pfind=pat_search(addr.s_addr,phead);
		
		if(pfind->p_key==addr.s_addr)
		{
			//printf("%f %08x: ", time, addr.s_addr);
			//printf("Found.\n");
		}
		else
		{
			/*
		 	* Insert the node.
		 	* Returns the node it inserted on success, 0 on failure.
		 	*/
			p = pat_insert(p, phead);
		}

	}//fim whilw

vTaskDelete(NULL);
for(;;);      

}

 

