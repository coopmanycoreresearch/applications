#ifndef FFT_H
#define FFT_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


#define ARRAY_SIZE 100
void* vAppTask (void *arg);
void vTask( void * pvParameters );

#endif
