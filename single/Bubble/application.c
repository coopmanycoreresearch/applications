
#include "application.h"

void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

long array[ARRAY_SIZE];

void* vAppTask (void *arg)
{
  long n, c, d, swap;

  n = ARRAY_SIZE;

  for ( c = 0 ; c < n ; c++ ){
     array[c]=n-c;
  }

// for ( c = 0 ; c < n ; c++ ){
     // printf("%d\n",array[c]);
  // }

  bubble_sort(array, n);

  // for ( c = 0 ; c < n ; c++ ){
     // printf("%d\n",array[c]);
  // }

vTaskDelete(NULL);
}

void bubble_sort(long list[], long n)
{
  long c, d, t;

  for (c = 0 ; c < ( n - 1 ); c++)
  {
    for (d = 0 ; d < n - c - 1; d++)
    {
      if (list[d] > list[d+1])
      {
        /* Swapping */

        t         = list[d];
        list[d]   = list[d+1];
        list[d+1] = t;
      }
    }
  }
}
