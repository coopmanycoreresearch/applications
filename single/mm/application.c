#include "application.h"

int a[ROWS][COLUMNS], // INPUT
    b[ROWS][COLUMNS], // INPUT
    c[ROWS][COLUMNS]; // TMR1

int finished=0;
int sum=0;

void *mult_task_tmr0(void *t)
{
    /*This function calculates 50 ROWS of the matrix*/
    int starting_row;
    int offset = ROWS/N_TASKS;
    starting_row = (int)t;
    starting_row = offset * starting_row;

    int i,j,k;
    for (i = starting_row; i<(starting_row+offset); i++) {
        for (j=0;j<COLUMNS;j++) {
            for (k=0;k<ROWS;k++) {
                c[i][j] += (a[i][k] * b[k][j]);
            }
            sum+=c[i][j];
        }
    }
    finished++;
    vTaskDelete( NULL );
    return;
}

void vTask( void * pvParameters )
{

int i,j;
xTaskHandle   p[N_TASKS];
    
    /*** Initialize matrixes ***/
    for (i=0; i<ROWS; i++) {
        for (j=0; j<COLUMNS; j++) {
            if(i==j || (i+j) == (ROWS-1)){
                a[i][j]= 1;
            } else {
                a[i][j]= 0;
            }
        }
    }

    for (i=0; i<ROWS; i++) {
        for (j=0; j<COLUMNS; j++) {
            b[i][j]= 1;
        }
    }
    
    for (i=0; i<ROWS; i++) {
        for (j=0; j<COLUMNS; j++) {
            c[i][j]= 0;
        }
    }
    
    /*
        In this technique test, whe execute the whole loop two times.
        Another approach could be to put c2[i] += a[i] * b[j]; inside the one and
        only loop, which will be tested further on.
    */
    for (j=0; j<N_TASKS; j++)
        xTaskCreate ( &mult_task_tmr0, "AppTaskA", 256, (void*)j, PRIORITY+2U , &p[j]);

    while(finished<2){ taskYIELD(); }
#ifdef DEBUG
    printf("SUM = %d\n",sum);
#endif

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

