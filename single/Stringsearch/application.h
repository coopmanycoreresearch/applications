
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


#define UCHAR_MAX 255
#define ITERATIONS 50
void vTask( void * pvParameters );
void*  vAppTask();

void  init_search(const char *string);                /* Pbmsrch.C      */
char *strsearch(const char *string);                  /* Pbmsrch.C      */
void  bmh_init(const char *pattern);                  /* Bmhsrch.C      */
char *bmh_search(const char *string,                  /* Bmhsrch.C      */
                 const int stringlen);
void  bmhi_init(const char *pattern);                 /* Bhmisrch.C     */
char *bmhi_search(const char *string,                 /* Bhmisrch.C     */
                  const int stringlen);
void  bmha_init(const char *pattern);                 /* Bmhasrch.C     */
char *bmha_search(const char *string,                 /* Bmhasrch.C     */
                  const int stringlen);
