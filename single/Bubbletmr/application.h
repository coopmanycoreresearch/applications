#ifndef FFT_H
#define FFT_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


#define ARRAY_SIZE 2000
void* vAppTaskA (void *arg);
void* vAppTaskB (void *arg);
void* vAppTaskC (void *arg);
void vTask( void * pvParameters );

#endif
