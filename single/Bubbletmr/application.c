#include "application.h"

int finished=0;

void vTask( void * pvParameters ){

xTaskHandle xTaskA,xTaskB,xTaskC;

    xTaskCreate ( &vAppTaskA, "AppTaskA", 256, NULL, PRIORITY+2U , &xTaskA);
    xTaskCreate ( &vAppTaskB, "AppTaskB", 256, NULL, PRIORITY+2U , &xTaskB);
    xTaskCreate ( &vAppTaskC, "AppTaskC", 256, NULL, PRIORITY+2U , &xTaskC);
    
    while(finished<3){ taskYIELD(); }

TMR_voter();

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

long arrayA[ARRAY_SIZE];
long arrayB[ARRAY_SIZE];
long arrayC[ARRAY_SIZE];
long array[ARRAY_SIZE];

void* vAppTaskA (void *arg)
{
  long n, c, d, swap;

  n = ARRAY_SIZE;

  for ( c = 0 ; c < n ; c++ ){
     arrayA[c]=n-c;
  }

// for ( c = 0 ; c < n ; c++ ){
     // printf("%d\n",array[c]);
  // }

  bubble_sort(arrayA, n);

  // for ( c = 0 ; c < n ; c++ ){
     // printf("%d\n",array[c]);
  // }
finished++;
vTaskDelete(NULL);
}

void* vAppTaskB (void *arg)
{
  long n, c, d, swap;

  n = ARRAY_SIZE;

  for ( c = 0 ; c < n ; c++ ){
     arrayB[c]=n-c;
  }

// for ( c = 0 ; c < n ; c++ ){
     // printf("%d\n",array[c]);
  // }

  bubble_sort(arrayB, n);

  // for ( c = 0 ; c < n ; c++ ){
     // printf("%d\n",array[c]);
  // }
finished++;
vTaskDelete(NULL);
}

void* vAppTaskC (void *arg)
{
  long n, c, d, swap;

  n = ARRAY_SIZE;

  for ( c = 0 ; c < n ; c++ ){
     arrayC[c]=n-c;
  }

// for ( c = 0 ; c < n ; c++ ){
     // printf("%d\n",array[c]);
  // }

  bubble_sort(arrayC, n);

  // for ( c = 0 ; c < n ; c++ ){
     // printf("%d\n",array[c]);
  // }
finished++;
vTaskDelete(NULL);
}

void bubble_sort(long list[], long n)
{
  long c, d, t;

  for (c = 0 ; c < ( n - 1 ); c++)
  {
    for (d = 0 ; d < n - c - 1; d++)
    {
      if (list[d] > list[d+1])
      {
        /* Swapping */

        t         = list[d];
        list[d]   = list[d+1];
        list[d+1] = t;
      }
    }
  }
}

void TMR_voter()
{
    int i,j;
    int ra=0,rb=0,rc=0,r=0;

    //tmr test
    
    for ( i=0; i<ARRAY_SIZE; i++ ){
        if ( (arrayA[i] == arrayB[i]) && (arrayA[i] == arrayC[i]) ) {
            // no error
            r++;
            array[i] = arrayA[i];
        } else {
            if ( arrayC[i] == arrayB[i] ) {
                // matrix 1 is wrong
                ra++;
                array[i] = arrayA[i] = arrayB[i];
            } else {
                if ( arrayA[i] == arrayC[i] ) {
                    // matrix 2 is wrong
                    rb++;
                    array[i] = arrayB[i] = arrayA[i];
                } else {
                    if ( arrayA[i] == arrayB[i] ) {
                        // matrix 3 is wrong
                        rc++;
                        array[i] = arrayC[i] = arrayA[i];
                    }
                }
            }
        }
    }
#ifdef DEBUG
    printf("RESULT %d\t T1 %d\t T2 %d\t T3 %d\n",r,ra,rb,rc);
    // printf("SUMA = %d\t SUMB = %d\t SUMC = %d\n",sum1,sum2,sum3);
#endif

    if(r == ARRAY_SIZE){
#ifdef DEBUG
        printf("CORRECT\n");
#endif
        TMRWrite(0);
    }else if (ra == ARRAY_SIZE) {
#ifdef DEBUG
        printf("T1_INFLUENCED\n");
#endif
        TMRWrite(1);
    }else if (rb == ARRAY_SIZE) {
#ifdef DEBUG
        printf("T3_INFLUENCED\n");
#endif
        TMRWrite(3);
    }else if (rc == ARRAY_SIZE) {
#ifdef DEBUG
        printf("T2_INFLUENCED\n");
#endif
        TMRWrite(2);
    } else if (rb+rc+r == ARRAY_SIZE) {
#ifdef DEBUG
		printf("T2_T3_INFLUENCED\n");
#endif
        TMRWrite(5);
	} else if (ra+rc+r == ARRAY_SIZE) {
#ifdef DEBUG
		printf("T1_T2_INFLUENCED\n");
#endif
        TMRWrite(4);
	} else if (ra+rb+r == ARRAY_SIZE) {
#ifdef DEBUG
		printf("T1_T3_INFLUENCED\n");
#endif
        TMRWrite(6);
	} else if (ra+rb+rc+r == ARRAY_SIZE) {
#ifdef DEBUG
		printf("T1_T2_T3_INFLUENCED\n");
#endif
        TMRWrite(7);
	} else {
#ifdef DEBUG
		printf("ERROR\n");
#endif
        TMRWrite(8);
	}
    return;
}
