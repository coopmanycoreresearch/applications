#include "application.h"

void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

void* vAppTask (void *arg){

int j;
int i;
int OLD1,OLD2,temp;

    for(j=0;j<LOOP;j++)
    {

    OLD1 = 0;
    OLD2 = 1;

        for(i=2; i<Fibonacci+1; i++)
        {

            temp = OLD2;
            OLD2 = OLD1+ OLD2;
            OLD1 = temp; //Valor antigo
        }

    }

vTaskDelete(NULL);
for(;;);
}
