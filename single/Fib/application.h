#ifndef FIB_H
#define FIB_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


#define LOOP 150
#define Fibonacci 38

void* vAppTask (void *arg);
void vTask( void * pvParameters );
#endif
