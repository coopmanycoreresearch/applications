#include "application.h"

void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

int mdc(int a, int b) {
  if (a % b == 0)
    return b;
  return mdc(b, a % b);
}

void * vAppTask(void*arg){

  int i, j;
  int input_A[NB_ELEMENTS], input_B[NB_ELEMENTS], output[NB_ELEMENTS];

  for (i=0; i<NB_ELEMENTS; i++){
    input_A[i] = INIT_A + i;
    input_B[i] = INIT_B + i;
  }

  for (i=0; i<LOOP; i++){
    for (j=0; j<NB_ELEMENTS; j++)
      output[j] = mdc(input_A[j], input_B[j]);
  }


vTaskDelete(NULL);
for(;;);
}
