#ifndef MDC_H
#define MDC_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"

#define LOOP        4
#define NB_ELEMENTS 16*16
#define INIT_A      16
#define INIT_B      524

void* vAppTask (void *arg);
void vTask( void * pvParameters );
#endif /* FREERTOS_CONFIG_H */
