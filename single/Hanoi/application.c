#include "application.h"

void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

void hanoi(int n, char a[], char b[], char c[]) {
  if (n != 1) {
    hanoi(n - 1, a, c, b);
    hanoi(n - 1, c, b, a);
  }
}


void* vAppTask  (void *arg){

  int i, j;
  int input[NB_ELEMENTS];
  char a[] = "src";
  char b[] = "dst";
  char c[] = "tmp";

  for (i=0; i<NB_ELEMENTS; i++)
    input[i] = INIT_VALUE - i;

  for (i=0; i<LOOP; i++){
    for (j=0; j<NB_ELEMENTS; j++)
      hanoi(input[j], a, b, c);
  }

vTaskDelete(NULL);
}
