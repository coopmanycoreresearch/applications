#ifndef application_H
#define application_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"

#define ITERATIONS 50

void*  vAppTask();
void vTask( void * pvParameters );

#endif
