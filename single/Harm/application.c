#include "application.h"

void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

double harm(int n) {
  if (n == 1)
    return 1;
  return 1.0 / (double)n + harm(n-1);
}

void * vAppTask(void*arg){

  int i, j;
  int input[NB_ELEMENTS], output[NB_ELEMENTS];

  for (i=0; i<NB_ELEMENTS; i++)
    input[i] = INIT_VALUE - i;

  for (i=0; i<LOOP; i++){
    for (j=0; j<NB_ELEMENTS; j++)
      output[j] = harm(input[j]);
  }

vTaskDelete(NULL);
for(;;);
}
