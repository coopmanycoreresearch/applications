#ifndef HARM_H
#define HARM_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


#define LOOP        25
#define NB_ELEMENTS 16
#define INIT_VALUE  16 //Number of recursions

void* vAppTask (void *arg);
void vTask( void * pvParameters );
#endif /* FREERTOS_CONFIG_H */
