#ifndef SW_H
#define SW_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


void vTask( void * pvParameters );
void vAppTask(void* arg);
#endif
