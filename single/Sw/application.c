#include "application.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"

#include <pthread.h>
#include "edbseq.h"
#include "eqseq.h"
#include "aa.h"
#include "const.h"
#include "qseq.h"
#include "dbseq.h"

#define initial_a 0
#define initial_b 0
#define final_a   6
#define final_b   2

#define NB_THREAD 4

void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

void * vAppTask(void* arg){
  int DELTA;
  int index, crunch_id, ia, ib;
  int max, Max;
  int i, j;

  short * a, * b;
  short * h;
  short * sim;
  int diag, up, left;

  a = (short *) malloc(Na*sizeof(short));
  b = (short *) malloc(Nb*sizeof(short));
  h = (short *) malloc(Na*Nb*sizeof(short));
  sim = (short *) malloc(AA*AA*sizeof(short));

  DELTA = -1;

//printfc("CRUNCH WORKING\n");

  for (i=0;i<AA;i++){
    for (j=0;j<AA;j++){
      if (i==j)
        sim[i + j*AA]=2;
      else
        sim[i + j*AA]=-1;
    }
  }

  int * pcrunch_id = (int *) arg;
  crunch_id = *pcrunch_id;

  for(index = 0;index < ((final_a-initial_a)*(final_b-initial_b));index++){
    ia = index%(final_a-initial_a);
    ib = index/(final_a-initial_a);
    Max = max = 0;

//  printfc("IA=%d IB=%d\n", ia, ib);

    a[0]=qseq_lengthes[ia];
    for (i=1;i<=a[0];i++)
      a[i]=char2AA(*(qseq_data[ia]+i-1));

    b[0]=dbseq_lengthes[ib];
    for (i=1;i<=b[0];i++)
      b[i]=char2AA(*(dbseq_data[ib]+i-1));

    for (i=0;i<=a[0];i++) h[i]=-i;
    for (j=0;j<=b[0];j++) h[j*Na]=-j;

    for (i=1;i<=a[0];i++){
      for (j=1;j<=b[0];j++){
        diag = h[i-1 + (j-1)*Na] + sim[a[i] + b[j]*AA];
        up   = h[i-1 + j*Na] + DELTA;
        left = h[i   + (j-1)*Na] + DELTA;

        max=MAX3(diag,up,left);
        if (max <= 0)  {
          h[i + j*Na]=0;
        }
        else if (max == diag) {
          h[i + j*Na]=diag;
        }
        else if (max == up) {
          h[i + j*Na]=up;
        }
        else{
          h[i + j*Na]=left;
        }
        if (max >= Max){
          Max=max;
        }
      }
    }
  }


  free((void *)a);
  free((void *)b);
  free((void *)h);
  free((void *)sim);

vTaskDelete(NULL);
}

int maxi(void){
  int maxv, maxia, maxib;
  int histo[12];
  int index;
  int nb_elts = ((final_a-initial_a)*(final_b-initial_b));

//printfc("MAXI  WORKING\n");

  maxv  = 0;
  maxia = 0;
  maxib = 0;

  for (index=0 ; index < nb_elts ; index++)
    histo[index] = 0;

  for (index=0 ; index < nb_elts ; index++){
//    histo[index] = results[index*8];
    if (histo[index]>maxv){
      maxia = index%(final_a-initial_a);
      maxib = index/(final_a-initial_a);
      maxv  = histo[index];
    }
  }
#ifdef DEBUG
  printf("MAXI A=%d B=%d LENGTH=%d\n", maxia, maxib, maxv);

  for (index=0 ; index < nb_elts ; index++)
    printf("histo[%d]=%d\n", index, histo[index]);
#endif
  return 0;
}
