#ifndef DEF_EDBSEQ_H
#define DEF_EDBSEQ_H

#define dbseq_num 2

extern short dbseq_lengthes[2];
extern unsigned char *dbseq_names[2];
extern unsigned char *dbseq_data[2];

#endif
