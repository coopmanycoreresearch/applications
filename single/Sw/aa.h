#ifndef DEF_AA_H
#define DEF_AA_H

short char2AA(char ch){
  switch (ch) {
    case 'C' : return 0;
    case 'G' : return 1;
    case 'P' : return 2;
    case 'S' : return 3;
    case 'A' : return 4;
    case 'T' : return 5;
    case 'D' : return 6;
    case 'E' : return 7;
    case 'N' : return 8;
    case 'Q' : return 9;
    case 'H' : return 10;
    case 'K' : return 11;
    case 'R' : return 12;
    case 'V' : return 13;
    case 'M' : return 14;
    case 'I' : return 15;
    case 'L' : return 16;
    case 'F' : return 17;
    case 'Y' : return 18;
    case 'W' : return 19;
    default  :
  return 0;
  }
}

#endif
