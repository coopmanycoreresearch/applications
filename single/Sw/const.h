#ifndef DEF_CONST_H
#define DEF_CONST_H

#define Na 247        // size of protein arrays (line)
#define Nb 23         // size of protein arrays (column)
#define AA 20         // number of amino acids
#define MAX2(x,y)     ((x)<(y) ? (y) : (x))
#define MAX3(x,y,z)   (MAX2(x,y)<(z) ? (z) : MAX2(x,y))

#endif
