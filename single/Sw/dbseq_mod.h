#ifndef DEF_DBSEQ_MOD_H
#define DEF_DBSEQ_MOD_H

short dbseq_lengthes[2]={
  22,
  22,
};

unsigned char dbseq_names_0[43] = "23KD_BACST 23 kDa basic protein (Fragment).";
unsigned char dbseq_names_1[37] = "48KD_BACCE 48 kDa protein (Fragment).";

unsigned char *dbseq_names[2];

unsigned char dbseq_data_0[22] = "AKESSFDIVSKVDLSEVANAIN";
unsigned char dbseq_data_1[22] = "ATQQEGMDISSSLAKTMIQETG";

unsigned char *dbseq_data[2];

#endif
