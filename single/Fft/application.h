#ifndef FFT_H
#define FFT_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"
#include "pthread.h"
#include "FreeRTOSConfig.h"


#define LOOP 10 //must be a power of 2
void* vAppTask (void *arg);
void vTask( void * pvParameters );
#endif /* FREERTOS_CONFIG_H */
