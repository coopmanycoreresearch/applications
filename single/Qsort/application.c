#include "application.h"
 
int arr[ITERATIONS];
 
void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}
void swap(int *a, int *b)
{
  *a = *a - *b;
  *b = *b + *a;
  *a = *b - *a;
}
 
int partition(int *arr, int start, int end)  //Partition the array
{
  int pivot = start;
  int left = start , right = end;
  int i , pivotVal = arr[pivot];
   
  while(left < right)
  {
    while(arr[left] <= pivotVal && left <=end )
      left++;
    while(arr[right] > pivotVal && right >= 0)
      right--;
    if(left < right)
      swap(&arr[left], &arr[right]);
  }
 
  arr[start] = arr[right];
  arr[right] = pivotVal;
 
  return right;
}
 
void quick(int *arr, int start, int end)
{
  int m;
    if(start < end)
    {
      m = partition(arr,start,end); //Pivot
      quick(arr,start,m-1);
      quick(arr,m+1,end);
    }
}
 
void*  vAppTask()
{
int i;
  
  for(i=0;i<ITERATIONS;i++)
   arr[i] = (i * 3);
    
  quick(arr, 0, ITERATIONS - 1);

vTaskDelete(NULL);
}
