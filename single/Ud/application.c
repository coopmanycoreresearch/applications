/*
 *
 * Copyright (c) 2005-2012 Imperas Software Ltd., www.imperas.com
 *
 * The contents of this file are provided under the Software License
 * Agreement that you accepted before downloading this file.
 *
 * This source forms part of the Software and can be used for educational,
 * training, and demonstration purposes but cannot be used for derivative
 * works except in cases where the derivative works require OVP technology
 * to run.
 *
 * For open source models released under licenses that you can use for
 * derivative works, please visit www.OVPworld.org or www.imperas.com
 * for the location of the open source models.
 *
 */


volatile int	P1_is_marked = 3;
volatile long	P1_marking_member_0[3];
volatile int	P2_is_marked = 5;
volatile long	P2_marking_member_0[5];
volatile int	P3_is_marked = 0;
volatile long	P3_marking_member_0[6];



#include "application.h"


void vTask( void * pvParameters ){

xTaskHandle xTask;

   xTaskCreate ( &vAppTask, "AppTask", 256, NULL, PRIORITY+2U , &xTask);
   while (eTaskGetState(xTask) == 1) { taskYIELD();}

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}
long int a[50][50], b[50], x[50];

int ludcmp(int nmax, int n);

void* vAppTask()
{

int k;
for(k=0;k<LOOP;k++) //LOOP
{
  int      i, j, nmax = 50, n = 5, chkerr;
  long int /* eps, */ w;

  /* eps = 1.0e-6; */

  /* Init loop */
  for(i = 0; i <= n; i++)
    {
      w = 0;              /* data to fill in cells */
      for(j = 0; j <= n; j++)
        {
          a[i][j] = (i + 1) + (j + 1);
          if(i == j)            /* only once per loop pass */
            a[i][j] *= 2;
          w += a[i][j];
        }
      b[i] = w;
    }

  /*  chkerr = ludcmp(nmax, n, eps); */
  chkerr = ludcmp(nmax,n);
}//LOOP

vTaskDelete(NULL);
for(;;);  
}

int ludcmp(int nmax, int n)
{
  int i, j, k;
  long w, y[100];

  /* if(n > 99 || eps <= 0.0) return(999); */
  for(i = 0; i < n; i++)
    {
      /* if(fabs(a[i][i]) <= eps) return(1); */
      for(j = i+1; j <= n; j++) /* triangular loop vs. i */
        {
          w = a[j][i];
          if(i != 0)            /* sub-loop is conditional, done
                                   all iterations except first of the
                                   OUTER loop */
            for(k = 0; k < i; k++)
              w -= a[j][k] * a[k][i];
          a[j][i] = w / a[i][i];
        }
      for(j = i+1; j <= n; j++) /* triangular loop vs. i */
        {
          w = a[i+1][j];
          for(k = 0; k <= i; k++) /* triangular loop vs. i */
            w -= a[i+1][k] * a[k][j];
          a[i+1][j] = w;
        }
    }
  y[0] = b[0];
  for(i = 1; i <= n; i++)       /* iterates n times */
    {
      w = b[i];
      for(j = 0; j < i; j++)    /* triangular sub loop */
        w -= a[i][j] * y[j];
      y[i] = w;
    }
  x[n] = y[n] / a[n][n];
  for(i = n-1; i >= 0; i--)     /* iterates n times */
    {
      w = y[i];
      for(j = i+1; j <= n; j++) /* triangular sub loop */
        w -= a[i][j] * x[j];
      x[i] = w / a[i][i] ;
    }
  return(0);
}
