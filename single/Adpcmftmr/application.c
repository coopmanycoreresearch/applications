#include "application.h"


/* $Id: adpcm.c,v 1.7 2005/06/15 07:27:31 ael01 Exp $ */
/*************************************************************************/
/*                                                                       */
/*   SNU-RT Benchmark Suite for Worst Case Timing Analysis               */
/*   =====================================================               */
/*                              Collected and Modified by S.-S. Lim      */
/*                                           sslim@archi.snu.ac.kr       */
/*                                         Real-Time Research Group      */
/*                                        Seoul National University      */
/*                                                                       */
/*                                                                       */
/*        < Features > - restrictions for our experimental environment   */
/*                                                                       */
/*          1. Completely structured.                                    */
/*               - There are no unconditional jumps.                     */
/*               - There are no exit from loop bodies.                   */
/*                 (There are no 'break' or 'return' in loop bodies)     */
/*          2. No 'switch' statements.                                   */
/*          3. No 'do..while' statements.                                */
/*          4. Expressions are restricted.                               */
/*               - There are no multiple expressions joined by 'or',     */
/*                'and' operations.                                      */
/*          5. No library calls.                                         */
/*               - All the functions needed are implemented in the       */
/*                 source file.                                          */
/*          6. Printouts removed (Jan G)                                 */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/*************************************************************************/
/*                                                                       */
/*  FILE: adpcm.c                                                        */
/*  SOURCE : C Algorithms for Real-Time DSP by P. M. Embree              */
/*                                                                       */
/*  DESCRIPTION :                                                        */
/*                                                                       */
/*     CCITT G.722 ADPCM (Adaptive Differential Pulse Code Modulation)   */
/*     algorithm.                                                        */
/*     16khz sample rate data is stored in the array test_data[SIZE].    */
/*     Results are stored in the array compressed[SIZE] and result[SIZE].*/
/*     Execution time is determined by the constant SIZE (default value  */
/*     is 2000).                                                         */
/*                                                                       */
/*  REMARK :                                                             */
/*                                                                       */
/*  EXECUTION TIME :                                                     */
/*                                                                       */
/*                                                                       */
/*************************************************************************/

int finished=0;

void vTask( void * pvParameters ){

xTaskHandle xTaskA,xTaskB,xTaskC;

    xTaskCreate ( &vAppTaskA, "AppTaskA", 256, NULL, PRIORITY+2U , &xTaskA);
    xTaskCreate ( &vAppTaskB, "AppTaskB", 256, NULL, PRIORITY+2U , &xTaskB);
    xTaskCreate ( &vAppTaskC, "AppTaskC", 256, NULL, PRIORITY+2U , &xTaskC);
    
    while(finished<3){ taskYIELD(); }

TMR_voter();

#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SUCCESS );
#else
    UartEndSimulation();
#endif // OVP

}

/* common sampling rate for sound cards on IBM/PC */
#define SAMPLE_RATE 11025

#define PI 3.14159
#define SIZE 50
#define IN_END 50

/* COMPLEX STRUCTURE */



typedef struct {
    float real, imag;
} COMPLEX;

/* function prototypes for fft and filter functions */
void fft(COMPLEX *,int);
float fir_filter(float input,float *coef,int n,float *history);
float iir_filter(float input,float *coef,int n,float *history);
float gaussian(void);
int my_abs(int n);

void setup_codec(int),key_down(),int_enable(),int_disable();
int flags(int);

float getinput(void);
void sendout(float),flush();

int encodeA(int,int);
int encodeB(int,int);
int encodeC(int,int);
void decodeA(int);
void decodeB(int);
void decodeC(int);
int filtez(int *bpl,int *dlt);
void upzero(int dlt,int *dlti,int *bli);
int filtep(int rlt1,int al1,int rlt2,int al2);
int quantl(int el,int detl);
/* int invqxl(int il,int detl,int *code_table,int mode); */
int logscl(int il,int nbl);
int scalel(int nbl,int shift_constant);
int uppol2(int al1,int al2,int plt,int plt1,int plt2);
int uppol1(int al1,int apl2,int plt,int plt1);
/* int invqah(int ih,int deth); */
int logsch(int ih,int nbh);
void reset();
float my_fabs(float n);
float my_cos(float n);
float my_sin(float n);

/* G722 C code */

/* variables for transimit quadrature mirror filter here */
int tqmfA[24];
int tqmfB[24];
int tqmfC[24];

/* QMF filter coefficients:
scaled by a factor of 4 compared to G722 CCITT recommendation */
int h[24] = {
    12,   -44,   -44,   212,    48,  -624,   128,  1448,
  -840, -3220,  3804, 15504, 15504,  3804, -3220,  -840,
  1448,   128,  -624,    48,   212,   -44,   -44,    12
};

int xlA,xhA;
int xlB,xhB;
int xlC,xhC;

/* variables for receive quadrature mirror filter here */
int accumcA[11],accumdA[11];
int accumcB[11],accumdB[11];
int accumcC[11],accumdC[11];

/* outputs of decode() */
int xout1A,xout2A;
int xout1B,xout2B;
int xout1C,xout2C;

int xsA,xdA;
int xsB,xdB;
int xsC,xdC;

/* variables for encoder (hi and lo) here */

int ilA,szlA,splA,slA,elA;
int ilB,szlB,splB,slB,elB;
int ilC,szlC,splC,slC,elC;

int qq4_code4_table[16] = {
     0,  -20456,  -12896,   -8968,   -6288,   -4240,   -2584,   -1200,
 20456,   12896,    8968,    6288,    4240,    2584,    1200,       0
};

int qq5_code5_table[32] = {
  -280,    -280,  -23352,  -17560,  -14120,  -11664,   -9752,   -8184,
 -6864,   -5712,   -4696,   -3784,   -2960,   -2208,   -1520,    -880,
 23352,   17560,   14120,   11664,    9752,    8184,    6864,    5712,
  4696,    3784,    2960,    2208,    1520,     880,     280,    -280
};

int qq6_code6_table[64] = {
  -136,    -136,    -136,    -136,  -24808,  -21904,  -19008,  -16704,
-14984,  -13512,  -12280,  -11192,  -10232,   -9360,   -8576,   -7856,
 -7192,   -6576,   -6000,   -5456,   -4944,   -4464,   -4008,   -3576,
 -3168,   -2776,   -2400,   -2032,   -1688,   -1360,   -1040,    -728,
 24808,   21904,   19008,   16704,   14984,   13512,   12280,   11192,
 10232,    9360,    8576,    7856,    7192,    6576,    6000,    5456,
  4944,    4464,    4008,    3576,    3168,    2776,    2400,    2032,
  1688,    1360,    1040,     728,     432,     136,    -432,    -136
};

int delay_bplA[6];
int delay_bplB[6];
int delay_bplC[6];

int delay_dltxA[6];
int delay_dltxB[6];
int delay_dltxC[6];

int wl_code_table[16] = {
   -60,  3042,  1198,   538,   334,   172,    58,   -30,
  3042,  1198,   538,   334,   172,    58,   -30,   -60
};

int wl_table[8] = {
   -60,   -30,    58,   172,   334,   538,  1198,  3042
};

int ilb_table[32] = {
  2048,  2093,  2139,  2186,  2233,  2282,  2332,  2383,
  2435,  2489,  2543,  2599,  2656,  2714,  2774,  2834,
  2896,  2960,  3025,  3091,  3158,  3228,  3298,  3371,
  3444,  3520,  3597,  3676,  3756,  3838,  3922,  4008
};

int         nblA;                  /* delay line */
int         nblB;                  /* delay line */
int         nblC;                  /* delay line */
int         al1A,al2A;
int         al1B,al2B;
int         al1C,al2C;
int         pltA,plt1A,plt2A;
int         pltB,plt1B,plt2B;
int         pltC,plt1C,plt2C;
int         rsA;
int         rsB;
int         rsC;
int         dltA;
int         dltB;
int         dltC;
int         rltA,rlt1A,rlt2A;
int         rltB,rlt1B,rlt2B;
int         rltC,rlt1C,rlt2C;

/* decision levels - pre-multiplied by 8, 0 to indicate end */
int decis_levl[30] = {
   280,   576,   880,  1200,  1520,  1864,  2208,  2584,
  2960,  3376,  3784,  4240,  4696,  5200,  5712,  6288,
  6864,  7520,  8184,  8968,  9752, 10712, 11664, 12896,
 14120, 15840, 17560, 20456, 23352, 32767
};

int         detlA;
int         detlB;
int         detlC;

/* quantization table 31 long to make quantl look-up easier,
last entry is for mil=30 case when wd is max */
int quant26bt_pos[31] = {
    61,    60,    59,    58,    57,    56,    55,    54,
    53,    52,    51,    50,    49,    48,    47,    46,
    45,    44,    43,    42,    41,    40,    39,    38,
    37,    36,    35,    34,    33,    32,    32
};

/* quantization table 31 long to make quantl look-up easier,
last entry is for mil=30 case when wd is max */
int quant26bt_neg[31] = {
    63,    62,    31,    30,    29,    28,    27,    26,
    25,    24,    23,    22,    21,    20,    19,    18,
    17,    16,    15,    14,    13,    12,    11,    10,
     9,     8,     7,     6,     5,     4,     4
};


int         dethA;
int         dethB;
int         dethC;
int         shA;         /* this comes from adaptive predictor */
int         shB;         /* this comes from adaptive predictor */
int         shC;         /* this comes from adaptive predictor */
int         ehA;
int         ehB;
int         ehC;

int qq2_code2_table[4] = {
  -7408,   -1616,   7408,  1616
};

int wh_code_table[4] = {
   798,   -214,    798,   -214
};


int         dhA,ihA;
int         dhB,ihB;
int         dhC,ihC;
int         nbhA,szhA;
int         nbhB,szhB;
int         nbhC,szhC;
int         sphA,phA,yhA,rhA;
int         sphB,phB,yhB,rhB;
int         sphC,phC,yhC,rhC;

int         delay_dhxA[6];
int         delay_dhxB[6];
int         delay_dhxC[6];

int         delay_bphA[6];
int         delay_bphB[6];
int         delay_bphC[6];

int         ah1A,ah2A;
int         ah1B,ah2B;
int         ah1C,ah2C;
int         ph1A,ph2A;
int         ph1B,ph2B;
int         ph1C,ph2C;
int         rh1A,rh2A;
int         rh1B,rh2B;
int         rh1C,rh2C;

/* variables for decoder here */
int         ilrA,ylA,rlA;
int         ilrB,ylB,rlB;
int         ilrC,ylC,rlC;
int         dec_dethA,dec_detlA,dec_dltA;
int         dec_dethB,dec_detlB,dec_dltB;
int         dec_dethC,dec_detlC,dec_dltC;

int         dec_del_bplA[6];
int         dec_del_bplB[6];
int         dec_del_bplC[6];

int         dec_del_dltxA[6];
int         dec_del_dltxB[6];
int         dec_del_dltxC[6];

int     dec_pltA,dec_plt1A,dec_plt2A;
int     dec_pltB,dec_plt1B,dec_plt2B;
int     dec_pltC,dec_plt1C,dec_plt2C;
int     dec_szlA,dec_splA,dec_slA;
int     dec_szlB,dec_splB,dec_slB;
int     dec_szlC,dec_splC,dec_slC;
int     dec_rlt1A,dec_rlt2A,dec_rltA;
int     dec_rlt1B,dec_rlt2B,dec_rltB;
int     dec_rlt1C,dec_rlt2C,dec_rltC;
int     dec_al1A,dec_al2A;
int     dec_al1B,dec_al2B;
int     dec_al1C,dec_al2C;
int     dlA;
int     dlB;
int     dlC;
int     dec_nblA,dec_yhA,dec_dhA,dec_nbhA;
int     dec_nblB,dec_yhB,dec_dhB,dec_nbhB;
int     dec_nblC,dec_yhC,dec_dhC,dec_nbhC;

/* variables used in filtez */
int         dec_del_bphA[6];
int         dec_del_bphB[6];
int         dec_del_bphC[6];

int         dec_del_dhxA[6];
int         dec_del_dhxB[6];
int         dec_del_dhxC[6];

int         dec_szhA;
int         dec_szhB;
int         dec_szhC;

/* variables used in filtep */
int         dec_rh1A,dec_rh2A;
int         dec_rh1B,dec_rh2B;
int         dec_rh1C,dec_rh2C;
int         dec_ah1A,dec_ah2A;
int         dec_ah1B,dec_ah2B;
int         dec_ah1C,dec_ah2C;
int         dec_phA,dec_sphA;
int         dec_phB,dec_sphB;
int         dec_phC,dec_sphC;

int     dec_shA,dec_rhA;
int     dec_shB,dec_rhB;
int     dec_shC,dec_rhC;

int     dec_ph1A,dec_ph2A;
int     dec_ph1B,dec_ph2B;
int     dec_ph1C,dec_ph2C;

static int resultA[SIZE],resultB[SIZE],resultC[SIZE],result[SIZE];

/* G722 encode function two ints in, one 8 bit output */

/* put input samples in xin1 = first value, xin2 = second value */
/* returns il and ih stored together */

/* MAX: 1 */
int my_abs(int n)
{
  int m;

  if (n >= 0) m = n;
  else m = -n;
  return m;
}

/* MAX: 1 */
float my_fabs(float n)
{
  float f;

  if (n >= 0) f = n;
  else f = -n;
  return f;
}

float my_sin(float rad)
{
  float diff;
  float app=0;

  int inc = 1;

  /* MAX dependent on rad's value, say 50 */
  while (rad > 2*PI)
      rad -= 2*PI;
  /* MAX dependent on rad's value, say 50 */
  while (rad < -2*PI)
      rad += 2*PI;
   diff = rad;
   app = diff;
   diff = (diff * (-(rad*rad))) /
     ((2 * inc) * (2 * inc + 1));
  app = app + diff;
  inc++;
  /* REALLY: while(my_fabs(diff) >= 0.00001) { */
  /* MAX: 1000 */
  while(my_fabs(diff) >= 1) {
    diff = (diff * (-(rad*rad))) /
	((2 * inc) * (2 * inc + 1));
    app = app + diff;
    inc++;
  }

  return app;
}


float my_cos(float rad)
{
  return (my_sin (PI / 2 - rad));
}


/* MAX: 1 */
int encodeA(int xin1,int xin2)
{
    int i;
    int *h_ptr,*tqmf_ptr,*tqmf_ptr1;
    long int xa,xb;
    int decis;

/* transmit quadrature mirror filters implemented here */
    h_ptr = h;
    tqmf_ptr = tqmfA;
    xa = (long)(*tqmf_ptr++) * (*h_ptr++);
    xb = (long)(*tqmf_ptr++) * (*h_ptr++);
/* main multiply accumulate loop for samples and coefficients */
    /* MAX: 10 */
    for(i = 0 ; i < 10 ; i++) {
        xa += (long)(*tqmf_ptr++) * (*h_ptr++);
        xb += (long)(*tqmf_ptr++) * (*h_ptr++);
    }
/* final mult/accumulate */
    xa += (long)(*tqmf_ptr++) * (*h_ptr++);
    xb += (long)(*tqmf_ptr) * (*h_ptr++);

/* update delay line tqmf */
    tqmf_ptr1 = tqmf_ptr - 2;
    /* MAX: 22 */
    for(i = 0 ; i < 22 ; i++) *tqmf_ptr-- = *tqmf_ptr1--;
    *tqmf_ptr-- = xin1;
    *tqmf_ptr = xin2;

/* scale outputs */
    xlA = (xa + xb) >> 15;
    xhA = (xa - xb) >> 15;

/* end of quadrature mirror filter code */

/* starting with lower sub band encoder */

/* filtez - compute predictor output section - zero section */
    szlA = filtez(delay_bplA,delay_dltxA);

/* filtep - compute predictor output signal (pole section) */
    splA = filtep(rlt1A,al1A,rlt2A,al2A);

/* compute the predictor output value in the lower sub_band encoder */
    slA = szlA + splA;
    elA = xlA - slA;

/* quantl: quantize the difference signal */
    ilA = quantl(elA,detlA);

/* invqxl: computes quantized difference signal */
/* for invqbl, truncate by 2 lsbs, so mode = 3 */
    dltA = ((long)detlA*qq4_code4_table[ilA >> 2]) >> 15;

/* logscl: updates logarithmic quant. scale factor in low sub band */
    nblA = logscl(ilA,nblA);

/* scalel: compute the quantizer scale factor in the lower sub band */
/* calling parameters nbl and 8 (constant such that scalel can be scaleh) */
    detlA = scalel(nblA,8);

/* parrec - simple addition to compute recontructed signal for adaptive pred */
    pltA = dltA + szlA;

/* upzero: update zero section predictor coefficients (sixth order)*/
/* calling parameters: dlt, dlt1, dlt2, ..., dlt6 from dlt */
/*  bpli (linear_buffer in which all six values are delayed */
/* return params:      updated bpli, delayed dltx */
    upzero(dltA,delay_dltxA,delay_bplA);

/* uppol2- update second predictor coefficient apl2 and delay it as al2 */
/* calling parameters: al1, al2, plt, plt1, plt2 */
    al2A = uppol2(al1A,al2A,pltA,plt1A,plt2A);

/* uppol1 :update first predictor coefficient apl1 and delay it as al1 */
/* calling parameters: al1, apl2, plt, plt1 */
    al1A = uppol1(al1A,al2A,pltA,plt1A);

/* recons : compute recontructed signal for adaptive predictor */
    rltA = slA + dltA;

/* done with lower sub_band encoder; now implement delays for next time*/
    rlt2A = rlt1A;
    rlt1A = rltA;
    plt2A = plt1A;
    plt1A = pltA;

/* high band encode */

    szhA = filtez(delay_bphA,delay_dhxA);

    sphA = filtep(rh1A,ah1A,rh2A,ah2A);

/* predic: sh = sph + szh */
    shA = sphA + szhA;
/* subtra: eh = xh - sh */
    ehA = xhA - shA;

/* quanth - quantization of difference signal for higher sub-band */
/* quanth: in-place for speed params: eh, deth (has init. value) */
    if(ehA >= 0) {
        ihA = 3;     /* 2,3 are pos codes */
    }
    else {
        ihA = 1;     /* 0,1 are neg codes */
    }
    decis = (564L*(long)dethA) >> 12L;
    if(my_abs(ehA) > decis) ihA--;     /* mih = 2 case */

/* invqah: compute the quantized difference signal, higher sub-band*/
    dhA = ((long)dethA*qq2_code2_table[ihA]) >> 15L ;

/* logsch: update logarithmic quantizer scale factor in hi sub-band*/
    nbhA = logsch(ihA,nbhA);

/* note : scalel and scaleh use same code, different parameters */
    dethA = scalel(nbhA,10);

/* parrec - add pole predictor output to quantized diff. signal */
    phA = dhA + szhA;

/* upzero: update zero section predictor coefficients (sixth order) */
/* calling parameters: dh, dhi, bphi */
/* return params: updated bphi, delayed dhx */
    upzero(dhA,delay_dhxA,delay_bphA);

/* uppol2: update second predictor coef aph2 and delay as ah2 */
/* calling params: ah1, ah2, ph, ph1, ph2 */
    ah2A = uppol2(ah1A,ah2A,phA,ph1A,ph2A);

/* uppol1:  update first predictor coef. aph2 and delay it as ah1 */
    ah1A = uppol1(ah1A,ah2A,phA,ph1A);

/* recons for higher sub-band */
    yhA = shA + dhA;

/* done with higher sub-band encoder, now Delay for next time */
    rh2A = rh1A;
    rh1A = yhA;
    ph2A = ph1A;
    ph1A = phA;

/* multiplex ih and il to get signals together */
    return(ilA | (ihA << 6));
}

int encodeB(int xin1,int xin2)
{
    int i;
    int *h_ptr,*tqmf_ptr,*tqmf_ptr1;
    long int xa,xb;
    int decis;

/* transmit quadrature mirror filters implemented here */
    h_ptr = h;
    tqmf_ptr = tqmfB;
    xa = (long)(*tqmf_ptr++) * (*h_ptr++);
    xb = (long)(*tqmf_ptr++) * (*h_ptr++);
/* main multiply accumulate loop for samples and coefficients */
    /* MAX: 10 */
    for(i = 0 ; i < 10 ; i++) {
        xa += (long)(*tqmf_ptr++) * (*h_ptr++);
        xb += (long)(*tqmf_ptr++) * (*h_ptr++);
    }
/* final mult/accumulate */
    xa += (long)(*tqmf_ptr++) * (*h_ptr++);
    xb += (long)(*tqmf_ptr) * (*h_ptr++);

/* update delay line tqmf */
    tqmf_ptr1 = tqmf_ptr - 2;
    /* MAX: 22 */
    for(i = 0 ; i < 22 ; i++) *tqmf_ptr-- = *tqmf_ptr1--;
    *tqmf_ptr-- = xin1;
    *tqmf_ptr = xin2;

/* scale outputs */
    xlB = (xa + xb) >> 15;
    xhB = (xa - xb) >> 15;

/* end of quadrature mirror filter code */

/* starting with lower sub band encoder */

/* filtez - compute predictor output section - zero section */
    szlB = filtez(delay_bplB,delay_dltxB);

/* filtep - compute predictor output signal (pole section) */
    splB = filtep(rlt1B,al1B,rlt2B,al2B);

/* compute the predictor output value in the lower sub_band encoder */
    slB = szlB + splB;
    elB = xlB - slB;

/* quantl: quantize the difference signal */
    ilB = quantl(elB,detlB);

/* invqxl: computes quantized difference signal */
/* for invqbl, truncate by 2 lsbs, so mode = 3 */
    dltB = ((long)detlB*qq4_code4_table[ilB >> 2]) >> 15;

/* logscl: updates logarithmic quant. scale factor in low sub band */
    nblB = logscl(ilB,nblB);

/* scalel: compute the quantizer scale factor in the lower sub band */
/* calling parameters nbl and 8 (constant such that scalel can be scaleh) */
    detlB = scalel(nblB,8);

/* parrec - simple addition to compute recontructed signal for adaptive pred */
    pltB = dltB + szlB;

/* upzero: update zero section predictor coefficients (sixth order)*/
/* calling parameters: dlt, dlt1, dlt2, ..., dlt6 from dlt */
/*  bpli (linear_buffer in which all six values are delayed */
/* return params:      updated bpli, delayed dltx */
    upzero(dltB,delay_dltxB,delay_bplB);

/* uppol2- update second predictor coefficient apl2 and delay it as al2 */
/* calling parameters: al1, al2, plt, plt1, plt2 */
    al2B = uppol2(al1B,al2B,pltB,plt1B,plt2B);

/* uppol1 :update first predictor coefficient apl1 and delay it as al1 */
/* calling parameters: al1, apl2, plt, plt1 */
    al1B = uppol1(al1B,al2B,pltB,plt1B);

/* recons : compute recontructed signal for adaptive predictor */
    rltB = slB + dltB;

/* done with lower sub_band encoder; now implement delays for next time*/
    rlt2B = rlt1B;
    rlt1B = rltB;
    plt2B = plt1B;
    plt1B = pltB;

/* high band encode */

    szhB = filtez(delay_bphB,delay_dhxB);

    sphB = filtep(rh1B,ah1B,rh2B,ah2B);

/* predic: sh = sph + szh */
    shB = sphB + szhB;
/* subtra: eh = xh - sh */
    ehB = xhB - shB;

/* quanth - quantization of difference signal for higher sub-band */
/* quanth: in-place for speed params: eh, deth (has init. value) */
    if(ehB >= 0) {
        ihB = 3;     /* 2,3 are pos codes */
    }
    else {
        ihB = 1;     /* 0,1 are neg codes */
    }
    decis = (564L*(long)dethB) >> 12L;
    if(my_abs(ehB) > decis) ihB--;     /* mih = 2 case */

/* invqah: compute the quantized difference signal, higher sub-band*/
    dhB = ((long)dethB*qq2_code2_table[ihB]) >> 15L ;

/* logsch: update logarithmic quantizer scale factor in hi sub-band*/
    nbhB = logsch(ihB,nbhB);

/* note : scalel and scaleh use same code, different parameters */
    dethB = scalel(nbhB,10);

/* parrec - add pole predictor output to quantized diff. signal */
    phB = dhB + szhB;

/* upzero: update zero section predictor coefficients (sixth order) */
/* calling parameters: dh, dhi, bphi */
/* return params: updated bphi, delayed dhx */
    upzero(dhB,delay_dhxB,delay_bphB);

/* uppol2: update second predictor coef aph2 and delay as ah2 */
/* calling params: ah1, ah2, ph, ph1, ph2 */
    ah2B = uppol2(ah1B,ah2B,phB,ph1B,ph2B);

/* uppol1:  update first predictor coef. aph2 and delay it as ah1 */
    ah1B = uppol1(ah1B,ah2B,phB,ph1B);

/* recons for higher sub-band */
    yhB = shB + dhB;

/* done with higher sub-band encoder, now Delay for next time */
    rh2B = rh1B;
    rh1B = yhB;
    ph2B = ph1B;
    ph1B = phB;

/* multiplex ih and il to get signals together */
    return(ilB | (ihB << 6));
}

int encodeC(int xin1,int xin2)
{
    int i;
    int *h_ptr,*tqmf_ptr,*tqmf_ptr1;
    long int xa,xb;
    int decis;

/* transmit quadrature mirror filters implemented here */
    h_ptr = h;
    tqmf_ptr = tqmfC;
    xa = (long)(*tqmf_ptr++) * (*h_ptr++);
    xb = (long)(*tqmf_ptr++) * (*h_ptr++);
/* main multiply accumulate loop for samples and coefficients */
    /* MAX: 10 */
    for(i = 0 ; i < 10 ; i++) {
        xa += (long)(*tqmf_ptr++) * (*h_ptr++);
        xb += (long)(*tqmf_ptr++) * (*h_ptr++);
    }
/* final mult/accumulate */
    xa += (long)(*tqmf_ptr++) * (*h_ptr++);
    xb += (long)(*tqmf_ptr) * (*h_ptr++);

/* update delay line tqmf */
    tqmf_ptr1 = tqmf_ptr - 2;
    /* MAX: 22 */
    for(i = 0 ; i < 22 ; i++) *tqmf_ptr-- = *tqmf_ptr1--;
    *tqmf_ptr-- = xin1;
    *tqmf_ptr = xin2;

/* scale outputs */
    xlC = (xa + xb) >> 15;
    xhC = (xa - xb) >> 15;

/* end of quadrature mirror filter code */

/* starting with lower sub band encoder */

/* filtez - compute predictor output section - zero section */
    szlC = filtez(delay_bplC,delay_dltxC);

/* filtep - compute predictor output signal (pole section) */
    splC = filtep(rlt1C,al1C,rlt2C,al2C);

/* compute the predictor output value in the lower sub_band encoder */
    slC = szlC + splC;
    elC = xlC - slC;

/* quantl: quantize the difference signal */
    ilC = quantl(elC,detlC);

/* invqxl: computes quantized difference signal */
/* for invqbl, truncate by 2 lsbs, so mode = 3 */
    dltC = ((long)detlC*qq4_code4_table[ilC >> 2]) >> 15;

/* logscl: updates logarithmic quant. scale factor in low sub band */
    nblC = logscl(ilC,nblC);

/* scalel: compute the quantizer scale factor in the lower sub band */
/* calling parameters nbl and 8 (constant such that scalel can be scaleh) */
    detlC = scalel(nblC,8);

/* parrec - simple addition to compute recontructed signal for adaptive pred */
    pltC = dltC + szlC;

/* upzero: update zero section predictor coefficients (sixth order)*/
/* calling parameters: dlt, dlt1, dlt2, ..., dlt6 from dlt */
/*  bpli (linear_buffer in which all six values are delayed */
/* return params:      updated bpli, delayed dltx */
    upzero(dltC,delay_dltxC,delay_bplC);

/* uppol2- update second predictor coefficient apl2 and delay it as al2 */
/* calling parameters: al1, al2, plt, plt1, plt2 */
    al2C = uppol2(al1C,al2C,pltC,plt1C,plt2C);

/* uppol1 :update first predictor coefficient apl1 and delay it as al1 */
/* calling parameters: al1, apl2, plt, plt1 */
    al1C = uppol1(al1C,al2C,pltC,plt1C);

/* recons : compute recontructed signal for adaptive predictor */
    rltC = slC + dltC;

/* done with lower sub_band encoder; now implement delays for next time*/
    rlt2C = rlt1C;
    rlt1C = rltC;
    plt2C = plt1C;
    plt1C = pltC;

/* high band encode */

    szhC = filtez(delay_bphC,delay_dhxC);

    sphC = filtep(rh1C,ah1C,rh2C,ah2C);

/* predic: sh = sph + szh */
    shC = sphC + szhC;
/* subtra: eh = xh - sh */
    ehC = xhC - shC;

/* quanth - quantization of difference signal for higher sub-band */
/* quanth: in-place for speed params: eh, deth (has init. value) */
    if(ehC >= 0) {
        ihC = 3;     /* 2,3 are pos codes */
    }
    else {
        ihC = 1;     /* 0,1 are neg codes */
    }
    decis = (564L*(long)dethC) >> 12L;
    if(my_abs(ehC) > decis) ihC--;     /* mih = 2 case */

/* invqah: compute the quantized difference signal, higher sub-band*/
    dhC = ((long)dethC*qq2_code2_table[ihC]) >> 15L ;

/* logsch: update logarithmic quantizer scale factor in hi sub-band*/
    nbhC = logsch(ihC,nbhC);

/* note : scalel and scaleh use same code, different parameters */
    dethC = scalel(nbhC,10);

/* parrec - add pole predictor output to quantized diff. signal */
    phC = dhC + szhC;

/* upzero: update zero section predictor coefficients (sixth order) */
/* calling parameters: dh, dhi, bphi */
/* return params: updated bphi, delayed dhx */
    upzero(dhC,delay_dhxC,delay_bphC);

/* uppol2: update second predictor coef aph2 and delay as ah2 */
/* calling params: ah1, ah2, ph, ph1, ph2 */
    ah2C = uppol2(ah1C,ah2C,phC,ph1C,ph2C);

/* uppol1:  update first predictor coef. aph2 and delay it as ah1 */
    ah1C = uppol1(ah1C,ah2C,phC,ph1C);

/* recons for higher sub-band */
    yhC = shC + dhC;

/* done with higher sub-band encoder, now Delay for next time */
    rh2C = rh1C;
    rh1C = yhC;
    ph2C = ph1C;
    ph1C = phC;

/* multiplex ih and il to get signals together */
    return(ilC | (ihC << 6));
}

/* decode function, result in xout1 and xout2 */

void decodeB(int input)
{
    int i;
    long int xa1,xa2;    /* qmf accumulators */
    int *h_ptr,*ac_ptr,*ac_ptr1,*ad_ptr,*ad_ptr1;

/* split transmitted word from input into ilr and ih */
    ilrB = input & 0x3f;
    ihB = input >> 6;

/* LOWER SUB_BAND DECODER */

/* filtez: compute predictor output for zero section */
    dec_szlB = filtez(dec_del_bplB,dec_del_dltxB);

/* filtep: compute predictor output signal for pole section */
    dec_splB = filtep(dec_rlt1B,dec_al1B,dec_rlt2B,dec_al2B);

    dec_slB = dec_splB + dec_szlB;

/* invqxl: compute quantized difference signal for adaptive predic */
    dec_dltB = ((long)dec_detlB*qq4_code4_table[ilrB >> 2]) >> 15;

/* invqxl: compute quantized difference signal for decoder output */
    dlB = ((long)dec_detlB*qq6_code6_table[ilB]) >> 15;

    rlB = dlB + dec_slB;

/* logscl: quantizer scale factor adaptation in the lower sub-band */
    dec_nblB = logscl(ilrB,dec_nblB);

/* scalel: computes quantizer scale factor in the lower sub band */
    dec_detlB = scalel(dec_nblB,8);

/* parrec - add pole predictor output to quantized diff. signal */
/* for partially reconstructed signal */
    dec_pltB = dec_dltB + dec_szlB;

/* upzero: update zero section predictor coefficients */
    upzero(dec_dltB,dec_del_dltxB,dec_del_bplB);

/* uppol2: update second predictor coefficient apl2 and delay it as al2 */
    dec_al2B = uppol2(dec_al1B,dec_al2B,dec_pltB,dec_plt1B,dec_plt2B);

/* uppol1: update first predictor coef. (pole setion) */
    dec_al1B = uppol1(dec_al1B,dec_al2B,dec_pltB,dec_plt1B);

/* recons : compute recontructed signal for adaptive predictor */
    dec_rltB = dec_slB + dec_dltB;

/* done with lower sub band decoder, implement delays for next time */
    dec_rlt2B = dec_rlt1B;
    dec_rlt1B = dec_rltB;
    dec_plt2B = dec_plt1B;
    dec_plt1B = dec_pltB;

/* HIGH SUB-BAND DECODER */

/* filtez: compute predictor output for zero section */
    dec_szhB = filtez(dec_del_bphB,dec_del_dhxB);

/* filtep: compute predictor output signal for pole section */
    dec_sphB = filtep(dec_rh1B,dec_ah1B,dec_rh2B,dec_ah2B);

/* predic:compute the predictor output value in the higher sub_band decoder */
    dec_shB = dec_sphB + dec_szhB;

/* invqah: in-place compute the quantized difference signal */
    dec_dhB = ((long)dec_dethB*qq2_code2_table[ihB]) >> 15L ;

/* logsch: update logarithmic quantizer scale factor in hi sub band */
    dec_nbhB = logsch(ihB,dec_nbhB);

/* scalel: compute the quantizer scale factor in the higher sub band */
    dec_dethB = scalel(dec_nbhB,10);

/* parrec: compute partially recontructed signal */
    dec_phB = dec_dhB + dec_szhB;

/* upzero: update zero section predictor coefficients */
    upzero(dec_dhB,dec_del_dhxB,dec_del_bphB);

/* uppol2: update second predictor coefficient aph2 and delay it as ah2 */
    dec_ah2B = uppol2(dec_ah1B,dec_ah2B,dec_phB,dec_ph1B,dec_ph2B);

/* uppol1: update first predictor coef. (pole setion) */
    dec_ah1B = uppol1(dec_ah1B,dec_ah2B,dec_phB,dec_ph1B);

/* recons : compute recontructed signal for adaptive predictor */
    rhB = dec_shB + dec_dhB;

/* done with high band decode, implementing delays for next time here */
    dec_rh2B = dec_rh1B;
    dec_rh1B = rhB;
    dec_ph2B = dec_ph1B;
    dec_ph1B = dec_phB;

/* end of higher sub_band decoder */

/* end with receive quadrature mirror filters */
    xdB = rlB - rhB;
    xsB = rlB + rhB;

/* receive quadrature mirror filters implemented here */
    h_ptr = h;
    ac_ptr = accumcB;
    ad_ptr = accumdB;
    xa1 = (long)xdB * (*h_ptr++);
    xa2 = (long)xsB * (*h_ptr++);
/* main multiply accumulate loop for samples and coefficients */
    for(i = 0 ; i < 10 ; i++) {
        xa1 += (long)(*ac_ptr++) * (*h_ptr++);
        xa2 += (long)(*ad_ptr++) * (*h_ptr++);
    }
/* final mult/accumulate */
    xa1 += (long)(*ac_ptr) * (*h_ptr++);
    xa2 += (long)(*ad_ptr) * (*h_ptr++);

/* scale by 2^14 */
    xout1B = xa1 >> 14;
    xout2B = xa2 >> 14;

/* update delay lines */
    ac_ptr1 = ac_ptr - 1;
    ad_ptr1 = ad_ptr - 1;
    for(i = 0 ; i < 10 ; i++) {
        *ac_ptr-- = *ac_ptr1--;
        *ad_ptr-- = *ad_ptr1--;
    }
    *ac_ptr = xdB;
    *ad_ptr = xsB;

    return;
}

void decodeC(int input)
{
    int i;
    long int xa1,xa2;    /* qmf accumulators */
    int *h_ptr,*ac_ptr,*ac_ptr1,*ad_ptr,*ad_ptr1;

/* split transmitted word from input into ilr and ih */
    ilrC = input & 0x3f;
    ihC = input >> 6;

/* LOWER SUC_CAND DECODER */

/* filtez: compute predictor output for zero section */
    dec_szlC = filtez(dec_del_bplC,dec_del_dltxC);

/* filtep: compute predictor output signal for pole section */
    dec_splC = filtep(dec_rlt1C,dec_al1C,dec_rlt2C,dec_al2C);

    dec_slC = dec_splC + dec_szlC;

/* invqxl: compute quantized difference signal for adaptive predic */
    dec_dltC = ((long)dec_detlC*qq4_code4_table[ilrC >> 2]) >> 15;

/* invqxl: compute quantized difference signal for decoder output */
    dlC = ((long)dec_detlC*qq6_code6_table[ilC]) >> 15;

    rlC = dlC + dec_slC;

/* logscl: quantizer scale factor adaptation in the lower sub-band */
    dec_nblC = logscl(ilrC,dec_nblC);

/* scalel: computes quantizer scale factor in the lower sub band */
    dec_detlC = scalel(dec_nblC,8);

/* parrec - add pole predictor output to quantized diff. signal */
/* for partially reconstructed signal */
    dec_pltC = dec_dltC + dec_szlC;

/* upzero: update zero section predictor coefficients */
    upzero(dec_dltC,dec_del_dltxC,dec_del_bplC);

/* uppol2: update second predictor coefficient apl2 and delay it as al2 */
    dec_al2C = uppol2(dec_al1C,dec_al2C,dec_pltC,dec_plt1C,dec_plt2C);

/* uppol1: update first predictor coef. (pole setion) */
    dec_al1C = uppol1(dec_al1C,dec_al2C,dec_pltC,dec_plt1C);

/* recons : compute recontructed signal for adaptive predictor */
    dec_rltC = dec_slC + dec_dltC;

/* done with lower sub band decoder, implement delays for next time */
    dec_rlt2C = dec_rlt1C;
    dec_rlt1C = dec_rltC;
    dec_plt2C = dec_plt1C;
    dec_plt1C = dec_pltC;

/* HIGH SUC-CAND DECODER */

/* filtez: compute predictor output for zero section */
    dec_szhC = filtez(dec_del_bphC,dec_del_dhxC);

/* filtep: compute predictor output signal for pole section */
    dec_sphC = filtep(dec_rh1C,dec_ah1C,dec_rh2C,dec_ah2C);

/* predic:compute the predictor output value in the higher sub_band decoder */
    dec_shC = dec_sphC + dec_szhC;

/* invqah: in-place compute the quantized difference signal */
    dec_dhC = ((long)dec_dethC*qq2_code2_table[ihC]) >> 15L ;

/* logsch: update logarithmic quantizer scale factor in hi sub band */
    dec_nbhC = logsch(ihC,dec_nbhC);

/* scalel: compute the quantizer scale factor in the higher sub band */
    dec_dethC = scalel(dec_nbhC,10);

/* parrec: compute partially recontructed signal */
    dec_phC = dec_dhC + dec_szhC;

/* upzero: update zero section predictor coefficients */
    upzero(dec_dhC,dec_del_dhxC,dec_del_bphC);

/* uppol2: update second predictor coefficient aph2 and delay it as ah2 */
    dec_ah2C = uppol2(dec_ah1C,dec_ah2C,dec_phC,dec_ph1C,dec_ph2C);

/* uppol1: update first predictor coef. (pole setion) */
    dec_ah1C = uppol1(dec_ah1C,dec_ah2C,dec_phC,dec_ph1C);

/* recons : compute recontructed signal for adaptive predictor */
    rhC = dec_shC + dec_dhC;

/* done with high band decode, implementing delays for next time here */
    dec_rh2C = dec_rh1C;
    dec_rh1C = rhC;
    dec_ph2C = dec_ph1C;
    dec_ph1C = dec_phC;

/* end of higher sub_band decoder */

/* end with receive quadrature mirror filters */
    xdC = rlC - rhC;
    xsC = rlC + rhC;

/* receive quadrature mirror filters implemented here */
    h_ptr = h;
    ac_ptr = accumcC;
    ad_ptr = accumdC;
    xa1 = (long)xdC * (*h_ptr++);
    xa2 = (long)xsC * (*h_ptr++);
/* main multiply accumulate loop for samples and coefficients */
    for(i = 0 ; i < 10 ; i++) {
        xa1 += (long)(*ac_ptr++) * (*h_ptr++);
        xa2 += (long)(*ad_ptr++) * (*h_ptr++);
    }
/* final mult/accumulate */
    xa1 += (long)(*ac_ptr) * (*h_ptr++);
    xa2 += (long)(*ad_ptr) * (*h_ptr++);

/* scale by 2^14 */
    xout1C = xa1 >> 14;
    xout2C = xa2 >> 14;

/* update delay lines */
    ac_ptr1 = ac_ptr - 1;
    ad_ptr1 = ad_ptr - 1;
    for(i = 0 ; i < 10 ; i++) {
        *ac_ptr-- = *ac_ptr1--;
        *ad_ptr-- = *ad_ptr1--;
    }
    *ac_ptr = xdC;
    *ad_ptr = xsC;

    return;
}

void decodeA(int input)
{
    int i;
    long int xa1,xa2;    /* qmf accumulators */
    int *h_ptr,*ac_ptr,*ac_ptr1,*ad_ptr,*ad_ptr1;

/* split transmitted word from input into ilr and ih */
    ilrA = input & 0x3f;
    ihA = input >> 6;

/* LOWER SUA_AAND DECODER */

/* filtez: compute predictor output for zero section */
    dec_szlA = filtez(dec_del_bplA,dec_del_dltxA);

/* filtep: compute predictor output signal for pole section */
    dec_splA = filtep(dec_rlt1A,dec_al1A,dec_rlt2A,dec_al2A);

    dec_slA = dec_splA + dec_szlA;

/* invqxl: compute quantized difference signal for adaptive predic */
    dec_dltA = ((long)dec_detlA*qq4_code4_table[ilrA >> 2]) >> 15;

/* invqxl: compute quantized difference signal for decoder output */
    dlA = ((long)dec_detlA*qq6_code6_table[ilA]) >> 15;

    rlA = dlA + dec_slA;

/* logscl: quantizer scale factor adaptation in the lower sub-band */
    dec_nblA = logscl(ilrA,dec_nblA);

/* scalel: computes quantizer scale factor in the lower sub band */
    dec_detlA = scalel(dec_nblA,8);

/* parrec - add pole predictor output to quantized diff. signal */
/* for partially reconstructed signal */
    dec_pltA = dec_dltA + dec_szlA;

/* upzero: update zero section predictor coefficients */
    upzero(dec_dltA,dec_del_dltxA,dec_del_bplA);

/* uppol2: update second predictor coefficient apl2 and delay it as al2 */
    dec_al2A = uppol2(dec_al1A,dec_al2A,dec_pltA,dec_plt1A,dec_plt2A);

/* uppol1: update first predictor coef. (pole setion) */
    dec_al1A = uppol1(dec_al1A,dec_al2A,dec_pltA,dec_plt1A);

/* recons : compute recontructed signal for adaptive predictor */
    dec_rltA = dec_slA + dec_dltA;

/* done with lower sub band decoder, implement delays for next time */
    dec_rlt2A = dec_rlt1A;
    dec_rlt1A = dec_rltA;
    dec_plt2A = dec_plt1A;
    dec_plt1A = dec_pltA;

/* HIGH SUA-AAND DECODER */

/* filtez: compute predictor output for zero section */
    dec_szhA = filtez(dec_del_bphA,dec_del_dhxA);

/* filtep: compute predictor output signal for pole section */
    dec_sphA = filtep(dec_rh1A,dec_ah1A,dec_rh2A,dec_ah2A);

/* predic:compute the predictor output value in the higher sub_band decoder */
    dec_shA = dec_sphA + dec_szhA;

/* invqah: in-place compute the quantized difference signal */
    dec_dhA = ((long)dec_dethA*qq2_code2_table[ihA]) >> 15L ;

/* logsch: update logarithmic quantizer scale factor in hi sub band */
    dec_nbhA = logsch(ihA,dec_nbhA);

/* scalel: compute the quantizer scale factor in the higher sub band */
    dec_dethA = scalel(dec_nbhA,10);

/* parrec: compute partially recontructed signal */
    dec_phA = dec_dhA + dec_szhA;

/* upzero: update zero section predictor coefficients */
    upzero(dec_dhA,dec_del_dhxA,dec_del_bphA);

/* uppol2: update second predictor coefficient aph2 and delay it as ah2 */
    dec_ah2A = uppol2(dec_ah1A,dec_ah2A,dec_phA,dec_ph1A,dec_ph2A);

/* uppol1: update first predictor coef. (pole setion) */
    dec_ah1A = uppol1(dec_ah1A,dec_ah2A,dec_phA,dec_ph1A);

/* recons : compute recontructed signal for adaptive predictor */
    rhA = dec_shA + dec_dhA;

/* done with high band decode, implementing delays for next time here */
    dec_rh2A = dec_rh1A;
    dec_rh1A = rhA;
    dec_ph2A = dec_ph1A;
    dec_ph1A = dec_phA;

/* end of higher sub_band decoder */

/* end with receive quadrature mirror filters */
    xdA = rlA - rhA;
    xsA = rlA + rhA;

/* receive quadrature mirror filters implemented here */
    h_ptr = h;
    ac_ptr = accumcA;
    ad_ptr = accumdA;
    xa1 = (long)xdA * (*h_ptr++);
    xa2 = (long)xsA * (*h_ptr++);
/* main multiply accumulate loop for samples and coefficients */
    for(i = 0 ; i < 10 ; i++) {
        xa1 += (long)(*ac_ptr++) * (*h_ptr++);
        xa2 += (long)(*ad_ptr++) * (*h_ptr++);
    }
/* final mult/accumulate */
    xa1 += (long)(*ac_ptr) * (*h_ptr++);
    xa2 += (long)(*ad_ptr) * (*h_ptr++);

/* scale by 2^14 */
    xout1A = xa1 >> 14;
    xout2A = xa2 >> 14;

/* update delay lines */
    ac_ptr1 = ac_ptr - 1;
    ad_ptr1 = ad_ptr - 1;
    for(i = 0 ; i < 10 ; i++) {
        *ac_ptr-- = *ac_ptr1--;
        *ad_ptr-- = *ad_ptr1--;
    }
    *ac_ptr = xdA;
    *ad_ptr = xsA;

    return;
}

/* clear all storage locations */

void reset()
{
    int i;

    detlA = dec_detlA = 32;   /* reset to min scale factor */
    detlB = dec_detlB = 32;   /* reset to min scale factor */
    detlC = dec_detlC = 32;   /* reset to min scale factor */
    dethA = dec_dethA = 8;
    dethB = dec_dethB = 8;
    dethC = dec_dethC = 8;
    nblA = al1A = al2A = plt1A = plt2A = rlt1A = rlt2A = 0;
    nblB = al1B = al2B = plt1B = plt2B = rlt1B = rlt2B = 0;
    nblC = al1C = al2C = plt1C = plt2C = rlt1C = rlt2C = 0;
    nbhA = ah1A = ah2A = ph1A = ph2A = rh1A = rh2A = 0;
    nbhB = ah1B = ah2B = ph1B = ph2B = rh1B = rh2B = 0;
    nbhC = ah1C = ah2C = ph1C = ph2C = rh1C = rh2C = 0;
    dec_nblA = dec_al1A = dec_al2A = dec_plt1A = dec_plt2A = dec_rlt1A = dec_rlt2A = 0;
    dec_nblB = dec_al1B = dec_al2B = dec_plt1B = dec_plt2B = dec_rlt1B = dec_rlt2B = 0;
    dec_nblC = dec_al1C = dec_al2C = dec_plt1C = dec_plt2C = dec_rlt1C = dec_rlt2C = 0;
    dec_nbhA = dec_ah1A = dec_ah2A = dec_ph1A = dec_ph2A = dec_rh1A = dec_rh2A = 0;
    dec_nbhB = dec_ah1B = dec_ah2B = dec_ph1B = dec_ph2B = dec_rh1B = dec_rh2B = 0;
    dec_nbhC = dec_ah1C = dec_ah2C = dec_ph1C = dec_ph2C = dec_rh1C = dec_rh2C = 0;

    for(i = 0 ; i < 6 ; i++) {
        delay_dltxA[i] = 0;
        delay_dltxB[i] = 0;
        delay_dltxC[i] = 0;
        delay_dhxA[i] = 0;
        delay_dhxB[i] = 0;
        delay_dhxC[i] = 0;
        dec_del_dltxA[i] = 0;
        dec_del_dltxB[i] = 0;
        dec_del_dltxC[i] = 0;
        dec_del_dhxA[i] = 0;
        dec_del_dhxB[i] = 0;
        dec_del_dhxC[i] = 0;
    }

    for(i = 0 ; i < 6 ; i++) {
        delay_bplA[i] = 0;
        delay_bplB[i] = 0;
        delay_bplC[i] = 0;
        delay_bphA[i] = 0;
        delay_bphB[i] = 0;
        delay_bphC[i] = 0;
        dec_del_bplA[i] = 0;
        dec_del_bplB[i] = 0;
        dec_del_bplC[i] = 0;
        dec_del_bphA[i] = 0;
        dec_del_bphB[i] = 0;
        dec_del_bphC[i] = 0;
    }

    for(i = 0 ; i < 23 ; i++) {
        tqmfA[i] = 0;
        tqmfB[i] = 0;
        tqmfC[i] = 0;
    }

    for(i = 0 ; i < 11 ; i++) {
        accumcA[i] = 0;
        accumcB[i] = 0;
        accumcC[i] = 0;
        accumdA[i] = 0;
        accumdB[i] = 0;
        accumdC[i] = 0;
    }
    return;
}

/* filtez - compute predictor output signal (zero section) */
/* input: bpl1-6 and dlt1-6, output: szl */

int filtez(int *bpl,int *dlt)
{
    int i;
    long int zl;
    zl = (long)(*bpl++) * (*dlt++);
    /* MAX: 6 */
    for(i = 1 ; i < 6 ; i++)
        zl += (long)(*bpl++) * (*dlt++);

    return((int)(zl >> 14));   /* x2 here */
}

/* filtep - compute predictor output signal (pole section) */
/* input rlt1-2 and al1-2, output spl */

int filtep(int rlt1,int al1,int rlt2,int al2)
{
    long int pl,pl2;
    pl = 2*rlt1;
    pl = (long)al1*pl;
    pl2 = 2*rlt2;
    pl += (long)al2*pl2;
    return((int)(pl >> 15));
}

/* quantl - quantize the difference signal in the lower sub-band */
int quantl(int el,int detl)
{
    int ril,mil;
    long int wd,decis;

/* abs of difference signal */
    wd = my_abs(el);
/* determine mil based on decision levels and detl gain */
    /* MAX: 30 */
    for(mil = 0 ; mil < 30 ; mil++) {
        decis = (decis_levl[mil]*(long)detl) >> 15L;
        if(wd <= decis) break;
    }
/* if mil=30 then wd is less than all decision levels */
    if(el >= 0) ril = quant26bt_pos[mil];
    else ril = quant26bt_neg[mil];
    return(ril);
}

/* invqxl is either invqbl or invqal depending on parameters passed */
/* returns dlt, code table is pre-multiplied by 8 */

/*    int invqxl(int il,int detl,int *code_table,int mode) */
/*    { */
/*        long int dlt; */
/*       dlt = (long)detl*code_table[il >> (mode-1)]; */
/*        return((int)(dlt >> 15)); */
/*    } */

/* logscl - update log quantizer scale factor in lower sub-band */
/* note that nbl is passed and returned */

int logscl(int il,int nbl)
{
    long int wd;
    wd = ((long)nbl * 127L) >> 7L;   /* leak factor 127/128 */
    nbl = (int)wd + wl_code_table[il >> 2];
    if(nbl < 0) nbl = 0;
    if(nbl > 18432) nbl = 18432;
    return(nbl);
}

/* scalel: compute quantizer scale factor in lower or upper sub-band*/

int scalel(int nbl,int shift_constant)
{
    int wd1,wd2,wd3;
    wd1 = (nbl >> 6) & 31;
    wd2 = nbl >> 11;
    wd3 = ilb_table[wd1] >> (shift_constant + 1 - wd2);
    return(wd3 << 3);
}

/* upzero - inputs: dlt, dlti[0-5], bli[0-5], outputs: updated bli[0-5] */
/* also implements delay of bli and update of dlti from dlt */

void upzero(int dlt,int *dlti,int *bli)
{
    int i,wd2,wd3;
/*if dlt is zero, then no sum into bli */
    if(dlt == 0) {
      for(i = 0 ; i < 6 ; i++) {
        bli[i] = (int)((255L*bli[i]) >> 8L); /* leak factor of 255/256 */
      }
    }
    else {
      for(i = 0 ; i < 6 ; i++) {
        if((long)dlt*dlti[i] >= 0) wd2 = 128; else wd2 = -128;
        wd3 = (int)((255L*bli[i]) >> 8L);    /* leak factor of 255/256 */
        bli[i] = wd2 + wd3;
      }
    }
/* implement delay line for dlt */
    dlti[5] = dlti[4];
    dlti[4] = dlti[3];
    dlti[3] = dlti[2];
    dlti[1] = dlti[0];
    dlti[0] = dlt;
    return;
}

/* uppol2 - update second predictor coefficient (pole section) */
/* inputs: al1, al2, plt, plt1, plt2. outputs: apl2 */

int uppol2(int al1,int al2,int plt,int plt1,int plt2)
{
    long int wd2,wd4;
    int apl2;
    wd2 = 4L*(long)al1;
    if((long)plt*plt1 >= 0L) wd2 = -wd2;    /* check same sign */
    wd2 = wd2 >> 7;                  /* gain of 1/128 */
    if((long)plt*plt2 >= 0L) {
        wd4 = wd2 + 128;             /* same sign case */
    }
    else {
        wd4 = wd2 - 128;
    }
    apl2 = wd4 + (127L*(long)al2 >> 7L);  /* leak factor of 127/128 */

/* apl2 is limited to +-.75 */
    if(apl2 > 12288) apl2 = 12288;
    if(apl2 < -12288) apl2 = -12288;
    return(apl2);
}

/* uppol1 - update first predictor coefficient (pole section) */
/* inputs: al1, apl2, plt, plt1. outputs: apl1 */

int uppol1(int al1,int apl2,int plt,int plt1)
{
    long int wd2;
    int wd3,apl1;
    wd2 = ((long)al1*255L) >> 8L;   /* leak factor of 255/256 */
    if((long)plt*plt1 >= 0L) {
        apl1 = (int)wd2 + 192;      /* same sign case */
    }
    else {
        apl1 = (int)wd2 - 192;
    }
/* note: wd3= .9375-.75 is always positive */
    wd3 = 15360 - apl2;             /* limit value */
    if(apl1 > wd3) apl1 = wd3;
    if(apl1 < -wd3) apl1 = -wd3;
    return(apl1);
}

/* logsch - update log quantizer scale factor in higher sub-band */
/* note that nbh is passed and returned */

int logsch(int ih,int nbh)
{
    int wd;
    wd = ((long)nbh * 127L) >> 7L;       /* leak factor 127/128 */
    nbh = wd + wh_code_table[ih];
    if(nbh < 0) nbh = 0;
    if(nbh > 22528) nbh = 22528;
    return(nbh);
}



void* vAppTaskA (void *arg)
{
    int i,j,f;
    static int test_data[SIZE],compressed[SIZE];

/* read in amplitude and frequency for test data */
    /*  scanf("%d",&j);
  scanf("%d",&f); */
     j = 10; f = 2000;

/* 16 KHz sample rate */
    for(i = 0 ; i < SIZE ; i++) {
              test_data[i] = (int)j*my_cos(f*PI*i/8000.0); 
    }

    for(i = 0 ; i < IN_END ; i += 2)
        compressed[i/2] = encodeA(test_data[i],test_data[i+1]);
    /* MAX: 2 */
    for(i = 0 ; i < IN_END ; i += 2) {
        decodeA(compressed[i/2]);
        resultA[i] = xout1A;
        resultA[i+1] = xout2A;
    }

#ifdef DEBUG
    long int k=0,m=0;
    for(k=0;k<SIZE;k++) {
        m+=resultA[k];
    }
    printf("%d\n",m);//-1515866833
#endif

finished++;
vTaskDelete(NULL);
}

void* vAppTaskB (void *arg)
{
    int i,j,f;
    static int test_data[SIZE],compressed[SIZE];

/* read in amplitude and frequency for test data */
    /*  scanf("%d",&j);
  scanf("%d",&f); */
     j = 10; f = 2000;

/* 16 KHz sample rate */
    for(i = 0 ; i < SIZE ; i++) {
              test_data[i] = (int)j*my_cos(f*PI*i/8000.0); 
    }

    for(i = 0 ; i < IN_END ; i += 2)
        compressed[i/2] = encodeB(test_data[i],test_data[i+1]);
    /* MAX: 2 */
    for(i = 0 ; i < IN_END ; i += 2) {
        decodeB(compressed[i/2]);
        resultB[i] = xout1B;
        resultB[i+1] = xout2B;
    }

#ifdef DEBUG
    long int k=0,m=0;
    for(k=0;k<SIZE;k++) {
        m+=resultB[k];
    }
    printf("%d\n",m);//-1515866833
#endif

finished++;

vTaskDelete(NULL);

}

void* vAppTaskC (void *arg)
{
    int i,j,f;
    static int test_data[SIZE],compressed[SIZE];

/* read in amplitude and frequency for test data */
    /*  scanf("%d",&j);
  scanf("%d",&f); */
     j = 10; f = 2000;

/* 16 KHz sample rate */
    for(i = 0 ; i < SIZE ; i++) {
        test_data[i] = (int)j*my_cos(f*PI*i/8000.0); 
    }

    for(i = 0 ; i < IN_END ; i += 2)
        compressed[i/2] = encodeC(test_data[i],test_data[i+1]);
    /* MAX: 2 */
    for(i = 0 ; i < IN_END ; i += 2) {
        decodeC(compressed[i/2]);
        resultC[i] = xout1C;
        resultC[i+1] = xout2C;
    }

#ifdef DEBUG
    long int k=0,m=0;
    for(k=0;k<IN_END;k++) {
        m+=resultC[k];
    }
    printf("%d\n",m);//-1515866833
#endif

finished++;

vTaskDelete(NULL);
}


void TMR_voter()
{
    int i,j;
    int ra=0,rb=0,rc=0,r=0;
    //tmr test
    for (i=0; i<SIZE; i++)
    {
        if ((resultA[i]== resultB[i]) && (resultA[i] == resultC[i])) {
            // no error
            r++;
            result[i] = resultA[i];
        } else {
            if((resultB[i] == resultC[i]) )
            {
                // matrix 1 is wrong
                ra++;
                result[i] = resultC[i];
                resultA[i] = resultC[i];
            } else {
                if((resultA[i] == resultC[i]) )
                {
                    // matrix 2 is wrong
                    rb++;
                    result[i] = resultA[i];
                    resultB[i] = resultA[i];
                } else {
                    if((resultA[i] == resultB[i]) )
                    {
                        // matrix 3 is wrong
                        rc++;
                        result[i] = resultA[i];
                        resultC[i] = resultA[i];
                    }
                }
            }
        }
    }
    
#ifdef DEBUG
    printf("RESULT %d\t T1 %d\t T2 %d\t T3 %d\n",r,ra,rb,rc);
#endif
    
    if(r==SIZE){
#ifdef DEBUG
        printf("CORRECT%c","\n");
#endif
        TMRWrite(0);
    }else if (ra+r==SIZE) {
#ifdef DEBUG
        printf("T1_INFLUENCED%c","\n");
#endif
        TMRWrite(1);
    }else if (rb+r==SIZE) {
#ifdef DEBUG
        printf("T3_INFLUENCED%c","\n");
#endif
        TMRWrite(3);
    }else if (rc+r==SIZE) {
#ifdef DEBUG
        printf("T2_INFLUENCED%c","\n");
#endif
        TMRWrite(2);
    } else if (rb+rc+r==SIZE) {
#ifdef DEBUG
		printf("T2_T3_INFLUENCED%c","\n");
#endif
        TMRWrite(5);
	} else if (ra+rc+r==SIZE) {
#ifdef DEBUG
		printf("T1_T2_INFLUENCED%c","\n");
#endif
        TMRWrite(4);
	} else if (ra+rb+r==SIZE) {
#ifdef DEBUG
		printf("T1_T3_INFLUENCED%c","\n");
#endif
        TMRWrite(6);
	} else if (ra+rb+rc+r==SIZE) {
#ifdef DEBUG
		printf("T1_T2_T3_INFLUENCED%c","\n");
#endif
        TMRWrite(7);
	} else {
#ifdef DEBUG
		printf("ERROR%c","\n");
#endif
        TMRWrite(8);
	}
    return;
}

