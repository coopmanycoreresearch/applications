#include "mwd.h"

static char start_print[] =	"MWD K Start";
static char end_print[]	=	"MWD K End";

Message msg;

int main()
{

    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm MEM3 640*/
    for (j=0;j<5;j++) {
        SYSCALL_RCV(MEM3,0,0,&msg);
    }

    /*Comm BLEND 640*/
    msg.length=128;
    for (j=0;j<5;j++) {
        SYSCALL_SEND(BLEND,0,0,&msg);
    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
