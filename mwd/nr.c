#include "mwd.h"

static char start_print[] =	"MWD C Start";
static char end_print[]	=	"MWD C End";

Message msg;

int main()
{

    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm IN 640*/
    msg.length=128;
    for (j=0;j<5;j++) {
        SYSCALL_RCV(IN,0,0,&msg);
    }

    /*Comm MEM1 640*/
    msg.length=128;
    for (j=0;j<5;j++) {
        SYSCALL_SEND(MEM1,0,0,&msg);
    }
    
    /*Comm MEM1 640*/
    for (j=0;j<5;j++) {
        SYSCALL_RCV(MEM1,0,0,&msg);
    }

    /*Comm MEM2 960*/
    msg.length=128;
    for (j=0;j<7;j++) {
        SYSCALL_SEND(MEM2,0,0,&msg);
    }

    msg.length=64;
    SYSCALL_SEND(MEM2,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,start_print);

    SYSCALL_DELETE(0,0,0,0);

}
