#include "mwd.h"

static char start_print[] =	"MWD E Start";
static char end_print[]	=	"MWD E End";

Message msg;

int main()
{

    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm NR 640*/
    for (j=0;j<5;j++) {
        SYSCALL_RCV(NR,0,0,&msg);
    }

    /*Comm NR 640*/
    msg.length=128;
    for (j=0;j<5;j++) {
        SYSCALL_SEND(NR,0,0,&msg);
    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
