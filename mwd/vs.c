#include "mwd.h"

static char start_print[] =	"MWD D Start";
static char end_print[]	=	"MWD D End";

Message msg;

int main()
{

    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    for (j=0;j<128;j++) {
        msg.msg[j]=i;
    }

    /*Comm HS 960*/
    for (j=0;j<8;j++) {
        SYSCALL_RCV(HS,0,0,&msg);
    }
    
    /*Comm JUG1 960*/
    msg.length=128;
    for (j=0;j<7;j++) {
        SYSCALL_SEND(JUG1,0,0,&msg);
    }
    
    msg.length=64;
    SYSCALL_SEND(JUG1,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
