#include "mwd.h"

static char start_print[] =	"MWD I Start";
static char end_print[]	=	"MWD I End";

Message msg;

int main()
{

    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm MEM2 960*/
    for (j=0;j<8;j++) {
        SYSCALL_RCV(MEM2,0,0,&msg);
    }

    /*Comm JUG2 960*/
    msg.length=128;
    for (j=0;j<7;j++) {
        SYSCALL_SEND(JUG2,0,0,&msg);
    }

    msg.length=64;
    SYSCALL_SEND(JUG2,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
