#include "mwd.h"

static char start_print[] =	"MWD H Start";
static char end_print[]	=	"MWD H End";

Message msg;

int main()
{

    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm JUG1 960*/
    for (j=0;j<8;j++) {
        SYSCALL_RCV(JUG1,0,0,&msg);
    }
    
    /*Comm JUG2 960*/
    for (j=0;j<8;j++) {
        SYSCALL_RCV(JUG2,0,0,&msg);
    }
    
    /*Comm SE 640*/
    msg.length=128;
    for (j=0;j<5;j++) {
        SYSCALL_SEND(SE,0,0,&msg);
    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
