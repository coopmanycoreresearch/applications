#include "mwd.h"

static char start_print[] =	"MWD L Start";
static char end_print[]	=	"MWD L End";

Message msg;

int main()
{

    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm SE 640*/
    for (j=0;j<5;j++) {
        SYSCALL_RCV(SE,0,0,&msg);
    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
