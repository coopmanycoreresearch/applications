#include "mwd.h"

static char start_print[] =	"MWD A Start";
static char end_print[]	=	"MWD A End";

Message msg;

int main()
{

    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    for (j=0;j<128;j++) {
        msg.msg[j]=i;
    }

    /*Comm HS 1280*/
    msg.length=128;
    for (j=0;j<10;j++) {
        SYSCALL_SEND(HS,0,0,&msg);
    }

    /*Comm NR 640*/
    msg.length=128;
    for (j=0;j<5;j++) {
        SYSCALL_SEND(NR,0,0,&msg);
    }

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
