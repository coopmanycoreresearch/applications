#include "mwd.h"

static char start_print[] =	"MWD B Start";
static char end_print[]	=	"MWD B End";

Message msg;

int main()
{

    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    /*Comm IN 1280*/
    for (j=0;j<10;j++) {
        SYSCALL_RCV(IN,0,0,&msg);
    }

    /*Comm VS 960*/
    msg.length=128;
    for (j=0;j<7;j++) {
        SYSCALL_SEND(VS,0,0,&msg);
    }

    msg.length=64;
    SYSCALL_SEND(VS,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
