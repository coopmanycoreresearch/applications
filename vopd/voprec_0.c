#include "vopd.h"

static char start_print[]=	"VOPD J Start";
static char end_print[]=	"VOPD J End";


Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    for (j=0;j<8;j++) {
        SYSCALL_RCV(UPSAMP_0,0,0,&msg);
    }
    
    msg.length=128;
    for (j=0;j<7;j++) {
        SYSCALL_SEND(PAD_0,0,0,&msg);
    }

    msg.length=66;
    SYSCALL_SEND(PAD_0,0,0,&msg);

    msg.length=128;
    for (j=0;j<11;j++) {
        SYSCALL_RCV(VOPME_0,0,0,&msg);
    }

    msg.length=92;
    SYSCALL_RCV(VOPME_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
