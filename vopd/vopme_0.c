#include "vopd.h"

static char start_print[]=	"VOPD L Start";
static char end_print[]=	"VOPD L End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    for (j=0;j<8;j++) {
        SYSCALL_RCV(PAD_0,0,0,&msg);
    }

    msg.length=128;
    for (j=0;j<2;j++) {
        SYSCALL_SEND(PAD_0,0,0,&msg);
    }

    msg.length=33;
    SYSCALL_SEND(PAD_0,0,0,&msg);

    msg.length=128;
    for (j=0;j<11;j++) {
        SYSCALL_SEND(VOPREC_0,0,0,&msg);
    }

    msg.length=92;
    SYSCALL_SEND(VOPREC_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
