#include "vopd.h"

static char start_print[]=	"VOPD I Start";
static char end_print[]=	"VOPD I End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    for (j=0;j<9;j++) {
        SYSCALL_RCV(IDCT2_0,0,0,&msg);
    }

    msg.length=128;
    for (j=0;j<7;j++) {
        SYSCALL_SEND(VOPREC_0,0,0,&msg);
    }
    
    msg.length=26;
    SYSCALL_SEND(VOPREC_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
