#include "vopd.h"

static char start_print[]=	"VOPD C Start";
static char end_print[]=	"VOPD C End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    SYSCALL_RCV(ARM_0,0,0,&msg);
    
    for (j=0;j<9;j++) {
        SYSCALL_RCV(IQUANT_0,0,0,&msg);
    }
    
    msg.length=128;
    for (j=0;j<8;j++) {
        SYSCALL_SEND(UPSAMP_0,0,0,&msg);
    }
    
    msg.length=61;
    SYSCALL_SEND(UPSAMP_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);
    
    SYSCALL_DELETE(0,0,0,0);

}
