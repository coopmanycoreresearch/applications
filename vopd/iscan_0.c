#include "vopd.h"

static char start_print[]=	"VOPD E Start";
static char end_print[]=	"VOPD E End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    for (j=0;j<9;j++) {
        SYSCALL_RCV(RUN_0,0,0,&msg);
    }

    msg.length=128;
    for (j=0;j<8;j++) {
        SYSCALL_SEND(ACDC_0,0,0,&msg);
    }

    msg.length=88;
    SYSCALL_SEND(ACDC_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);

}
