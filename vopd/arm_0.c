#include "vopd.h"

static char start_print[]=	"VOPD A Start";
static char end_print[]=	"VOPD A End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    for (j=0;j<128;j++) {
        msg.msg[j]=j;
    }

    msg.length=128;
    SYSCALL_SEND(IDCT2_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);	

    SYSCALL_DELETE(0,0,0,0);

}
