#include "vopd.h"

static char start_print[]=	"VOPD G Start";
static char end_print[]=	"VOPD G End";

Message msg;

int main()
{

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);

    SYSCALL_RCV(ACDC_0,0,0,&msg);
    
    SYSCALL_RCV(ACDC_0,0,0,&msg);
    
    msg.length=128;
    SYSCALL_SEND(IQUANT_0,0,0,&msg);

    SYSCALL_PRINTF(0,0,0,end_print);
    
    SYSCALL_DELETE(0,0,0,0);

}
