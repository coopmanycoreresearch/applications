#ifndef __VOPD_H__
#define __VOPD_H__
#include "../applications.h"

#define ACDC_0 1
#define ARM_0 2
#define IDCT2_0 3
#define IQUANT_0 4
#define ISCAN_0 5
#define PAD_0 6
#define RUN_0 7
#define STRIPEM_0 8
#define UPSAMP_0 9
#define VLD_0 10
#define VOPME_0 11
#define VOPREC_0 12

#endif /*__VOPD_H__*/
