#ifndef __DIJKSTRA_H__
#define __DIJKSTRA_H__
#include "../applications.h"

#define NUM_NODES                  16       //16 for small input; 160 for large input; 30 for medium input;
#define MAXPROCESSORS              64       //The amount of processor
#define MSGLEN                     (NUM_NODES*(NUM_NODES-1)/2)

#define dijkstra_0 1
#define dijkstra_1 2
#define dijkstra_2 3
#define dijkstra_3 4
#define dijkstra_4 5
#define divider 6

#endif /*__DIJKSTRA_H__*/
