#include "dijkstra.h"

#define NONE                       9999     //Maximum
#define rank                        2

static char start_print[] = "DJK D Start";
static char end_print[] =   "DJK D End";

struct _NODE{
    int iDist;
    int iPrev;
    int iCatched;
};
typedef struct _NODE NODE;

struct _UVERTEX{
    int iPID;
    int iNID;
    int iDist;
};
typedef struct _UVERTEX UVERTEX;

UVERTEX uVertex[MAXPROCESSORS];
NODE rgnNodes[MAXPROCESSORS][NUM_NODES];
int g_qCount[MAXPROCESSORS];
int resultSend[33];
int tasks[MAXPROCESSORS][2];
int nodes_tasks[MSGLEN][2];
int AdjMatrix[NUM_NODES][NUM_NODES];
int globalMiniCost[MAXPROCESSORS];
int paths;
int qtdEnvios;
Message msg;

int main()
{
    int i, j;

    SYSCALL_PRINTF(0,0,0,start_print);

    qtdEnvios = 0;

    msg.length = MSGLEN;
    for (j=0; j<2; j++) {
        SYSCALL_RCV(divider,0,0,&msg);
        for (i=0; i<MSGLEN; i++) {
            nodes_tasks[i][j] = msg.msg[i];
		}
	}

    msg.length = MAXPROCESSORS;
    for (j=0; j<2; j++) {
        SYSCALL_RCV(divider,0,0,&msg);
        for (i=0; i<MAXPROCESSORS; i++) {
            tasks[i][j] = msg.msg[i];
        }
	}

    msg.length = NUM_NODES;
    for (i=0; i<NUM_NODES; i++) {
        SYSCALL_RCV(divider,0,0,&msg);
        for (j=0; j<NUM_NODES; j++) {
            AdjMatrix[j][i] = msg.msg[j];
        }
    }

    dijkstra(rank);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}

void sendPath( NODE *rgnNodes, int chNode )
{
    if ((rgnNodes+chNode)->iPrev != NONE) {
        sendPath(rgnNodes, (rgnNodes+chNode)->iPrev);
    }
    resultSend[paths] = chNode+1;
    paths++;
}

void sendResult( int myID,int chStart, int chEnd )
{
    int k;
    paths = 3;
    for (k=0; k<33; k++) {
        resultSend[k] = 0;
    }
    resultSend[0] = chStart;
    resultSend[1] = chEnd;
    resultSend[2] = rgnNodes[myID][chEnd].iDist;
    sendPath(rgnNodes[myID], chEnd);

    msg.length = 33;
    for (k=0; k<33; k++) {
        msg.msg[k] = resultSend[k];
    }
    SYSCALL_SEND(divider,0,0,&msg);
}

void dijkstra( int myID ) {
    int x,i,v;
    int chStart, chEnd;
    int u =-1;

    for (x=tasks[myID][0]; x<tasks[myID][1]; x++) {
        chStart = nodes_tasks[x][0];    //Start node
        chEnd = nodes_tasks[x][1];      //End node
        u=-1;

        //Initialize and clear
        uVertex[myID].iDist=NONE;
        uVertex[myID].iPID=myID;
        uVertex[myID].iNID=NONE;
        g_qCount[myID] = 0;
        u=-1;

        for (v=0; v<NUM_NODES; v++) {
            rgnNodes[myID][v].iDist =  AdjMatrix[chStart][v];
            rgnNodes[myID][v].iPrev = NONE;
            rgnNodes[myID][v].iCatched = 0;
        }

        //Start working
        while (g_qCount[myID] < NUM_NODES-1) {
            for (i=0; i<NUM_NODES; i++) {
                if (rgnNodes[myID][i].iCatched==0 &&
                    rgnNodes[myID][i].iDist<uVertex[myID].iDist &&
                    rgnNodes[myID][i].iDist!=0) {
                    uVertex[myID].iDist=rgnNodes[myID][i].iDist;
                    uVertex[myID].iNID=i;
                }
            }

            globalMiniCost[myID]=NONE;
            if (globalMiniCost[myID]>uVertex[myID].iDist) {
                globalMiniCost[myID] = uVertex[myID].iDist;
                u=uVertex[myID].iNID;
                g_qCount[myID]++;
            }

            for (v=0; v<NUM_NODES; v++) {
                if (v==u) {
                    rgnNodes[myID][v].iCatched = 1;
                    continue;
                }
                if ((rgnNodes[myID][v].iCatched==0 &&
                    rgnNodes[myID][v].iDist>(rgnNodes[myID][u].iDist+AdjMatrix[u][v]))) {
                    rgnNodes[myID][v].iDist=rgnNodes[myID][u].iDist+AdjMatrix[u][v];
                    rgnNodes[myID][v].iPrev = u;
                }
            }
            uVertex[myID].iDist = NONE; //Reset
        }

        sendResult(myID,chStart,chEnd);
        qtdEnvios++;
    }

    msg.length = 33;
    msg.msg[0] = -1;
    SYSCALL_SEND(divider,0,0,&msg);
}
