#include "dijkstra.h"

#define NPROC                       5

static char start_print[] = "DJK A Start";
static char start_tasks[] = "DJK A Tasks";
static char end_print[] =   "DJK A End";

int fpTrix[NUM_NODES*NUM_NODES] = {     1,    6,    3,    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999,
    6,    1,    2,    5,    9999, 9999, 1,    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999,
    3,    2,    1,    3,    4,    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999,
    9999, 5,    3,    1,    2,    3,    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999,
    9999, 9999, 4,    2,    1,    5,    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999,
    9999, 9999, 9999, 3,    5,    1,    3,    2,    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999,
    9999, 1,    9999, 9999, 9999, 3,    1,    4,    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999,
    9999, 9999, 9999, 9999, 9999, 2,    4,    1,    7,    9999, 9999, 9999, 9999, 9999, 9999, 9999,
    9999, 9999, 9999, 9999, 9999, 9999, 9999, 7,    1,    5,    1,    9999, 9999, 9999, 9999, 9999,
    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 5,    1,    9999, 3,    9999, 9999, 9999, 9999,
    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 1,    9999, 1,    9999, 4,    9999, 9999, 8,
    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 3,    9999, 1,    9999, 2,    9999, 9999,
    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 4,    9999, 1,    1,    9999, 2,
    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 2,    1,    1,    6,    9999,
    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 6,    1,    3,
    9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 9999, 8,    9999, 2,    9999, 3,    1 };

int tasks[MAXPROCESSORS][2];
int nodes_tasks[MSGLEN][2];
int AdjMatrix[NUM_NODES][NUM_NODES];
int result[33];
int end_task[5];
int ended;

Message msg;

int main(int argc, char *argv[])
{

    SYSCALL_PRINTF(0,0,0,start_print);

    ended = 0;

    execute();

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}

void sendTOtasks()
{
    SYSCALL_SEND(dijkstra_0,0,0,&msg);
    SYSCALL_SEND(dijkstra_1,0,0,&msg);
    SYSCALL_SEND(dijkstra_2,0,0,&msg);
    SYSCALL_SEND(dijkstra_3,0,0,&msg);
    SYSCALL_SEND(dijkstra_4,0,0,&msg);
}

void startThreads(void)
{
    int i, j;

    SYSCALL_PRINTF(0,0,0,start_tasks);

    /* SEND nodes_tasks[NUM_NODES*(NUM_NODES-1)/2][2] */
    msg.length = MSGLEN;
    // Send X of nodes_tasks
    for (j=0; j<2; j++) {
        for (i=0; i<MSGLEN; i++) {
            msg.msg[i] = nodes_tasks[i][j];
        }
        sendTOtasks();
    }
    
    /* SEND tasks[MAXPROCESSORS][2] */
    msg.length = MAXPROCESSORS;
    // Send X and Y of tasks
    for (j=0; j<2; j++) {
        for (i=0; i<MAXPROCESSORS; i++) {
            msg.msg[i] = tasks[i][j];
		}
		sendTOtasks();
	}

    /* SEND AdjMatrix[NUM_NODES][NUM_NODES] */
    msg.length = NUM_NODES;
    for (i=0; i<NUM_NODES; i++) {
        for (j=0; j<NUM_NODES; j++) {
            msg.msg[j] = AdjMatrix[j][i];
        }
        sendTOtasks();
    }

}

void divide_task_group(int task)
{
    int i;
    for (i=0;i<NPROC;i++) {
        tasks[i][0] = task/NPROC * (i);
        tasks[i][1] = task/NPROC * (i+1) + (i+1==NPROC&task%NPROC!=0?task%NPROC:0);

    }
}

void ProcessMessage()
{

    if (result[0] == -1) {
        ended++;
    }

}

void execute()
{

    int i,j,k;

    k = 0;
    /* Step 1: geting the working vertexs and assigning values */
    for (i=0;i<NUM_NODES;i++) {
        for (j=0;j<NUM_NODES;j++) {
            AdjMatrix[i][j]= fpTrix[k];
            k++;
        }

    }

    int tasks = MSGLEN;

    int x=0;
    for (i=0;i<NUM_NODES-1;i++) { //small:15; large:159
        for (j=i+1;j<NUM_NODES;j++) {    //small:16; large:160
            nodes_tasks[x][0] = i;
            nodes_tasks[x][1] = j;
            x++;
        }
    }

    divide_task_group(tasks);
    startThreads();

    msg.length = 33;
    while (ended < (NPROC)) {

        if (end_task[0] != -1) {

            SYSCALL_RCV(dijkstra_0,0,0,&msg);
            for (k=0; k<33; k++)
                result[k] = msg.msg[k];
            if (result[0] == -1) end_task[0] = -1;
            ProcessMessage();
        }

        if (end_task[1] != -1) {

            SYSCALL_RCV(dijkstra_1,0,0,&msg);
            for (k=0; k<33; k++) {
                result[k] = msg.msg[k];
            }

            if (result[0] == -1) {
                end_task[1] = -1;
            }

            ProcessMessage();
        }

        if (end_task[2] != -1) {

            SYSCALL_RCV(dijkstra_2,0,0,&msg);
            for (k=0; k<33; k++) {
                result[k] = msg.msg[k];
            }
            if (result[0] == -1) {
                end_task[2] = -1;
            }
            ProcessMessage();
        }

        if (end_task[3] != -1) {

            SYSCALL_RCV(dijkstra_3,0,0,&msg);
            for (k=0; k<33; k++) {
                result[k] = msg.msg[k];
            }
            if (result[0] == -1) {
                end_task[3] = -1;
            }
            ProcessMessage();
        }

        if (end_task[4] != -1) {

            SYSCALL_RCV(dijkstra_4,0,0,&msg);
            for (k=0; k<33; k++) {
                result[k] = msg.msg[k];
            }

            if (result[0] == -1) {
                end_task[4] = -1;
            }
            ProcessMessage();
        }
    }

}
