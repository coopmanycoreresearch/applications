#include "dtw8_pubsub.h"


int pattern[SIZE][SIZE] = {
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0}
};

static char start_print[]=  "DTWD A Start";
static char end_print[]=    "DTWD A End";
int P[TOTAL_TASKS] = {p1,p2,p3,p4,p5,p6,p7,p8};
//int pattern[SIZE][SIZE];
//Message msg1;
MQSoCMessage message;
MQSoCTopic topicName;

int main(){

    int i, j, sw;
    SYSCALL_PRINTF(0,0,0,start_print);

    ///////////////////////////////////////////////
    topicName.topic_id = 1;

    MQSoCAdvertise(topicName);

    //msg1.length = SIZE * SIZE; //SIZE*SIZE nao pode ser maior que 128, senao usar o SendData
    message.payloadlen = SIZE * SIZE;

    randPattern(pattern); //gera uma matriz de valores aleatorios, poderiam ser coeficientes MFCC
    memcpy(message.payload, pattern, sizeof(pattern));
    message.n_subscribers = TOTAL_TASKS;

    for(j=0; j<PATTERN_PER_TASK; j++)
        MQSoCPublish(&topicName, &message);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}

void randPattern(int in[SIZE][SIZE]){
    int i,j;

    for(i=0; i<SIZE; i++){
        for(j=0; j<SIZE; j++){
            in[i][j] = abs(irand(23, 2, 100)%5000);
        }
    }
}

int irand(int seed, int min, int max)
{
    int lfsr = seed;

    lfsr = (lfsr >> 1) ^ (-(lfsr & 1u) & 0xB400u);

    return (lfsr % max + min);
}

void MQSoCAdvertise(MQSoCTopic topicName)
{
    SYSCALL_MQSOC_ADVERTISE(&topicName,0,0,0);
}

void MQSoCPublish(MQSoCTopic* tpName, MQSoCMessage* mqMessage)
{
    mqMessage->pub_ok = 0;
    while (mqMessage->pub_ok == 0) {
        SYSCALL_MQSOC_PUBLISH(tpName,0,0,mqMessage);
        //SYSCALL_MQSOC_PUBLISH(0,0,0,&message);
        //SYSCALL_PRINTF(0,0,0,"mqMessage->pub_ok");
        //SYSCALL_PRINTF(1,0,0,mqMessage->pub_ok);
    }
}

void memcpy(unsigned int *dest, const unsigned int *src, size_t n) {
    unsigned int i = n/4;
    while(i--)
        dest[i] = src[i];
}
