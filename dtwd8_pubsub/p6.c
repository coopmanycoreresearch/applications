#include "dtw8_pubsub.h"

static char start_print[]=  "DTWD G Start";
static char end_print[]=    "DTWD G End";

int test[SIZE][SIZE];
int pattern[SIZE][SIZE];
int lastCol[SIZE];
int currCol[SIZE];
int temp[SIZE];

//Message msg1;
MQSoCMessage message;
MQSoCTopic topicName;
tasks_subscribers_api_struct tasks_subscribers_api;
void trata_callback1(MQSoCMessage* mqMessage);
void trata_callback2(MQSoCMessage* mqMessage);
messageHandlers_str messageHandlers1;
messageHandlers_str messageHandlers2;

int main(){

    SYSCALL_PRINTF(0,0,0,start_print);

    //SYSCALL_RCV(recognizer,0,0,&msg1);

    topicName.topic_id = 2; //recognizer from

    MQSoCSubscribe(topicName, trata_callback2, 1); //recognizer from

    topicName.topic_id = 1; //bank from

    MQSoCSubscribe(topicName, trata_callback1, 1); //bank from

    topicName.topic_id = 3;

    MQSoCAdvertise(topicName); //recognizer to

    MQSoCYield(PUBRCV_DELAY, PATTERN_PER_TASK+1, SUSPEND_SUBS); //recognizer and bank from

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}

int min(int x, int y) {
    if (x > y)
        return y;
    return x;
}

int euclideanDistance(int *x, int *y) {
    int ed = 0.0f;
    int aux = 0.0f;
    int i;
    for (i = 0; i < SIZE; i++) {
        aux = x[i] - y[i];
        ed += aux * aux;
    }
    return ed;
}

int dynamicTimeWarping(int x[SIZE][SIZE], int y[SIZE][SIZE]) {

    int maxI = SIZE - 1;
    int maxJ = SIZE - 1;
    int minGlobalCost;
    int i, j;

    currCol[0] = euclideanDistance(x[0], y[0]);
    for (j = 1; j <= maxJ; j++) {
        currCol[j] = currCol[j - 1] + euclideanDistance(x[0], y[j]);
    }

    for (i = 1; i <= maxI; i++) {

        memcpy(temp, lastCol, SIZE);
        memcpy(lastCol,currCol, SIZE);
        memcpy(currCol,currCol, SIZE);

        currCol[0] = lastCol[0] + euclideanDistance(x[i], y[0]);

        for (j = 1; j <= maxJ; j++) {
            minGlobalCost = min(lastCol[j], min(lastCol[j - 1], currCol[j - 1]));
            currCol[j] = minGlobalCost + euclideanDistance(x[i], y[j]);
        }
    }

    return currCol[maxJ];
}

void *memset(void *dst, int c, unsigned long bytes) {

    unsigned char *Dst = (unsigned char*)dst;

    while((int)bytes-- > 0)
        *Dst++ = (unsigned char)c;

    return dst;
}

/*
 * timeout = time between read on buffer
 * c_exit = read attemps to end task
 * suspend = suspend(1) or not(0) the task in case of no message on buffer
 */
void MQSoCYield(int timeout, int c_exit, int suspend){
    int count = 0;
    while (1){
        int k;
        for(k=0;k<timeout;k++); //sleep()
        tasks_subscribers_api.suspend = suspend;
        SYSCALL_MQSOC_PUBRCV(0,0,0,&tasks_subscribers_api); //verifica se chegou pacote e pegar do buffer
        if (tasks_subscribers_api.received == 1){
            //SYSCALL_PRINTF(0,0,0,"ivlc");
            if (tasks_subscribers_api.message.topic_id == 1) {
#ifdef PRINTDEBUGBUFFER
                SYSCALL_PRINTF(0,0,0,"Recebeu mensagem - topic_id = 1");
#endif
                messageHandlers1.cb(&tasks_subscribers_api.message);
            }

            if (tasks_subscribers_api.message.topic_id == 2) {
#ifdef PRINTDEBUGBUFFER
                SYSCALL_PRINTF(0,0,0,"Recebeu mensagem - topic_id = 2");
#endif
                messageHandlers2.cb(&tasks_subscribers_api.message);
            }
            count++;
        }
        else{
            //SYSCALL_PRINTF(0,0,0,"Não Recebeu mensagem");
        }

        if (count == c_exit)
            break;
    }
}

void MQSoCSubscribe(MQSoCTopic tpName, Callback_p callBack, int n_publishers)
//void MQSoCSubscribe(MQSoCTopic topicName)
{
    if (tpName.topic_id == 1)
        messageHandlers1.cb = callBack;
    if (tpName.topic_id == 2)
        messageHandlers2.cb = callBack;
    SYSCALL_MQSOC_SUBSCRIBE(&tpName,0,0,n_publishers);
}


void trata_callback2(MQSoCMessage* mqMessage){
    memcpy(test, mqMessage->payload, mqMessage->payloadlen);

}

void trata_callback1(MQSoCMessage* mqMessage){

    int result,j;

    result = 0;

    memcpy(pattern, mqMessage->payload, mqMessage->payloadlen);

    result = dynamicTimeWarping(test, pattern);

    topicName.topic_id = 3;


    //msg1.msg[0] = result;
    message.payload[0] = result;

    //msg1.length = 1;
    message.payloadlen = 1;

    message.n_subscribers = 0;

    //SYSCALL_SEND(recognizer,0,0,&msg1);
    MQSoCPublish(&topicName, &message);



}

void MQSoCAdvertise(MQSoCTopic tpName)
{
    SYSCALL_MQSOC_ADVERTISE(&tpName,0,0,0);
}

void MQSoCPublish(MQSoCTopic* tpName, MQSoCMessage* mqMessage)
{
    mqMessage->pub_ok = 0;
    while (mqMessage->pub_ok == 0) {
        SYSCALL_MQSOC_PUBLISH(tpName,0,0,mqMessage);
        //SYSCALL_MQSOC_PUBLISH(0,0,0,&message);
        //SYSCALL_PRINTF(0,0,0,"mqMessage->pub_ok");
        //SYSCALL_PRINTF(1,0,0,mqMessage->pub_ok);
    }
}

void memcpy(unsigned int *dest, const unsigned int *src, size_t n) {
    unsigned int i = n/4;
    while(i--)
        dest[i] = src[i];
}
