#include "dtw8_pubsub.h"

static char start_print[]=  "DTWD J Start";
static char test_print[]=   "Test Sent";
static char end_print[]=    "DTWD J End";

//Message msg1;
MQSoCMessage message;
MQSoCTopic topicName;
tasks_subscribers_api_struct tasks_subscribers_api;
void trata_callback(MQSoCMessage* mqMessage);
messageHandlers_str messageHandlers;

/*int test[SIZE][SIZE] = {
  {7200, 4600},
  {1900, 5800}
  };*/

int test[SIZE][SIZE] = {
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0},
    {0,1,2,3,4,5,6,7,8,9,0}
};

/* int P[TOTAL_TASKS] = {p1,p2,p3,p4,p5,p6,p7,p8}; */

int main(){

    int j;

    SYSCALL_PRINTF(0,0,0,start_print);


    topicName.topic_id = 2;
    MQSoCAdvertise(topicName); //pn to

    topicName.topic_id = 3;
    MQSoCSubscribe(topicName, trata_callback, TOTAL_TASKS); //pn from

    //msg1.length = SIZE*SIZE; //SIZE*SIZE nao pode ser maior que 128, senao usar o SendData
    message.payloadlen = SIZE*SIZE; //SIZE*SIZE nao pode ser maior que 128, senao usar o SendData

    memcpy(message.payload, test, sizeof(test));

    topicName.topic_id = 2;
    message.n_subscribers = TOTAL_TASKS;
    MQSoCPublish(&topicName, &message);

    SYSCALL_PRINTF(0,0,0,test_print);

    MQSoCYield(PUBRCV_DELAY, NUM_PATTERNS, SUSPEND_SUBS);

    SYSCALL_PRINTF(0,0,0,end_print);

    SYSCALL_DELETE(0,0,0,0);
}



/*
 * timeout = time between read on buffer
 * c_exit = read attemps to end task
 * suspend = suspend(1) or not(0) the task in case of no message on buffer
 */
void MQSoCYield(int timeout, int c_exit, int suspend){
    int count = 0;
    while (1){
        int k;
        for(k=0;k<timeout;k++); //sleep()
        tasks_subscribers_api.suspend = suspend;
        SYSCALL_MQSOC_PUBRCV(0,0,0,&tasks_subscribers_api); //verifica se chegou pacote e pegar do buffer
        if (tasks_subscribers_api.received == 1){
#ifdef PRINTDEBUGBUFFER
            SYSCALL_PRINTF(0,0,0,"Recebeu mensagem");
#endif
            //SYSCALL_PRINTF(0,0,0,"ivlc");
            messageHandlers.cb(&tasks_subscribers_api.message);
            count++;
        }
        else{
            //SYSCALL_PRINTF(0,0,0,"Não Recebeu mensagem");
        }

        if (count == c_exit)
            break;
    }
}

void MQSoCSubscribe(MQSoCTopic tpName, Callback_p callBack, int n_publishers)
//void MQSoCSubscribe(MQSoCTopic topicName)
{
    messageHandlers.cb = callBack;
    SYSCALL_MQSOC_SUBSCRIBE(&tpName,0,0,n_publishers);
}


void trata_callback(MQSoCMessage* mqMessage){


}

void MQSoCAdvertise(MQSoCTopic tpName)
{
    SYSCALL_MQSOC_ADVERTISE(&tpName,0,0,0);
}

void MQSoCPublish(MQSoCTopic* tpName, MQSoCMessage* mqMessage)
{
    mqMessage->pub_ok = 0;
    while (mqMessage->pub_ok == 0) {
        SYSCALL_MQSOC_PUBLISH(tpName,0,0,mqMessage);
        //SYSCALL_MQSOC_PUBLISH(0,0,0,&message);
        //SYSCALL_PRINTF(0,0,0,"mqMessage->pub_ok");
        //SYSCALL_PRINTF(1,0,0,mqMessage->pub_ok);
    }
}

void memcpy(unsigned int *dest, const unsigned int *src, size_t n) {
    unsigned int i = n/4;
    while(i--)
        dest[i] = src[i];
}
